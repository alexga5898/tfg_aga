# Estructura de directorios del proyecto

A continuación se muestran los directorios más relevantes del proyecto:

```bash
.
├── backend
│    └── src
│        ├── main
│        └── ...backend
│            ├── model
│            │   ├── amazon
│            │   ├── entities
│            │   ├── exceptions
│            │   └── model
│            └── rest
│            │   ├── common
│            │   │   ├── amazon
│            │   │   ├── jwt
│            │   │   └── stripe
│            │   ├── controllers
│            │   └── dtos
│            ├── sql
│            └── test
│
└── frontend
     ├── public
     └── src
         ├── backend
         ├── i18n
         │   ├── es
         │   └── en
         ├── modules
         │   ├── app
         │   │   ├── components
         │   │   └── css
         │   ├── catalog
         │   │    ├── components
         │   │    └── css
         │   ├── common
         │   │    ├── components
         │   │    └── css
         │   ├── shopping
         │   │    ├── components
         │   │    └── css
         │   └── users
         │        ├── components
         │        └── css
         └── store

```

# Requisitos de software

### Backend

- Java SE Development Kit 11
- MySQL Community Server 8
- Apache Maven 3.6.1+

### Frontend

- Node.js 12.19.0+
- NPM 6.14.8+

# Instalación

### Creación de las bases de datos

- Iniciar MySQL server si no está iniciado (ej. *mysqld*).

- Crear dos bases de datos (una para el proyecto y otra para los tests).
```sh
mysqladmin -u root create tfgproject -p
mysqladmin -u root create tfgprojecttest -p
```

- Iniciar sesión en MYSQL y crear un usuario, dándole permiso sobre las dos bases da datos.
```sh
mysql -u root
    CREATE USER 'tfg'@'localhost' IDENTIFIED BY 'tfg';
    GRANT ALL PRIVILEGES ON tfgproject.* to 'tfg'@'localhost' WITH GRANT OPTION;
    GRANT ALL PRIVILEGES ON tfgprojecttest.* to 'tfg'@'localhost' WITH GRANT OPTION;
    exit
```

### Backend

- Crear las tablas para las entidades en la base de datos.
```sh
cd backend
mvn sql:execute
```

### Frontend

- Instalar todas las dependencias.
```sh
cd frontend
npm install
```

### Stripe

Para poder añadir y comprar productos a la aplicación mediante la interfaz, es necesario realizar las siguientes configuraciones:

1. Crear una cuenta o iniciar sesión en [Stripe](https://dashboard.stripe.com/register), y activar la cuenta (no es necesario cubrir la información de empresa, sólo activar la cuenta utilizando el enlace que se envía al correo introducido).
2. Desde el dashboard, dirigirse al apartado de configuración arriba a la derecha.
3. Entrar en Payments > Checkout y Payment Links.
4. Desactivar las opciones de Apple Pay y Google Pay.
5. En información pública, entrar en el enlace de "información de la cuenta", escribir un nombre para la misma y guardarlo.
6. Desde el dashboard, dirigirse al apartado de Connect.
7. En el apartado de facilitar pagos a múltiples destinatarios con Connect, seleccionar el botón "empezar" y marcar la opción de "plataforma o marketplace" que aparece en la ventana abierta.
8. Volver al apartado de ajustes.
9. Ahora aparece un apartado de configuración para Connect. Entramos en la opción "configuración" que está dentro de este.
10. En tipos de cuenta Express, entrar en gestionar y seleccionar España y Francia y guardar.
11. De vuelta en la configuración de Connect, escribir un nombre de empresa y añadir un icono en el apartado de imagen de marca y guardar los cambios.
12. Descargar Stripe CLI e iniciar sesión siguiendo los pasos indicados en la [documentación](https://stripe.com/docs/stripe-cli).
13. Iniciar Stripe CLI con el siguiente comando:
```sh
stripe listen
```
14. Copiar la clave secreta que aparece en la consola de Stripe en el atributo "my.stripe.webhook.secret" del fichero application.properties, que se encuentra en backend > src > main > resources.
15. Desde el dashboard, dirigirse al apartado de desarrolladores y consultar las claves de prueba.
16. Copiar la clave publicable en el fichero config.json, que se encuentra en frontend > src.
17. Copiar la clave secreta en el atributo "my.strype.key" del fichero application.properties, que se encuentra en backend > src > main > resources.

# Ejecución

- Iniciar el backend.
```sh
cd backend
mvn spring-boot:run
```

- Iniciar el frontend.
```sh
cd frontend
npm start
```

- Iniciar Stripe CLI.
```sh
stripe listen --events payment_intent.created,checkout.session.completed,account.updated --forward-to localhost:8080/stripe/webhook
```
