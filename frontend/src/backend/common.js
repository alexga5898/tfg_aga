const SERVICETOKEN_NAME = "serviceToken";
let reauthenticationCallback;
let networkErrorCallback;

export const handleErrorResponse = (error, onErrors) => {
     if (error.response && error.response.status >= 400 && error.response.status < 500) {
        if (error.response.status === 401 && reauthenticationCallback != null) {
            reauthenticationCallback();
        }
        onErrors(error.response.data);
    } else {
        handleNetworkError(error);
    }
}

export const handleNetworkError = (error) => {
    if (error.toString().split(": ")[1] === 'Network Error') {
        networkErrorCallback();
    }
}

export const setServiceToken = (serviceToken) => {
    localStorage.setItem(SERVICETOKEN_NAME, serviceToken);
}

export const getServiceToken = () => {
    return localStorage.getItem(SERVICETOKEN_NAME);
}

export const removeServiceToken = () => {
    localStorage.removeItem(SERVICETOKEN_NAME);
}

export const setReauthenticationCallback = callback => {
    reauthenticationCallback = callback;
}

export const getConfig = () => {
    if (!localStorage.getItem(SERVICETOKEN_NAME)) {
        return null;
    }
    let config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem(SERVICETOKEN_NAME)
        }
    }
    return config;
}

export const init = callback => networkErrorCallback = callback;


