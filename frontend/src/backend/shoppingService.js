import axios from "axios";
import { handleErrorResponse, getConfig, handleNetworkError } from "./common";

const URL = "http://localhost:8080/shopping/";

export const sellProduct = (product, onSuccess, onErrors) => {
    axios.post(URL + 'products/newProduct', product, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors)
        });
}

export const updateProduct = (productId, product, onSuccess, onErrors) => {
    axios.post(URL + `products/myProducts/${productId}`, product, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const removeProduct = (productId, onSuccess, onErrors) => {
    axios.delete(URL + `products/myProducts/${productId}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const addItemCart = (shoppingCartId, addItemsToCartParams, onSuccess, onErrors) => {
    axios.post(URL + `shoppingCart/${shoppingCartId}/add`, addItemsToCartParams, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const updateItemCart = (shoppingCartId, updateItemsCartParams, onSuccess, onErrors) => {
    axios.post(URL + `shoppingCart/${shoppingCartId}/update`, updateItemsCartParams, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const deleteItemCart = (shoppingCartId, deleteItemsCartParams, onSuccess, onErrors) => {
    axios.post(URL + `shoppingCart/${shoppingCartId}/delete`, deleteItemsCartParams, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const buySuccess = (sessionId, onSuccess, onErrors) => {
    axios.get(URL + `order/success?session_id=${sessionId}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getOrders = (page, month, year, asc, buyer, onSuccess) => {
    axios.get(URL + `orders?page=${page}&month=${month}&year=${year}&asc=${asc}&buyer=${buyer}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleNetworkError(error);
        });
}

export const getOrder = (orderId, onSuccess, onErrors) => {
    axios.get(URL + `orders/${orderId}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const addRating = (rate, onSuccess, onErrors) => {
    axios.post(URL + `ratings/addRating`, rate, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getUserRatings = (page, onSuccess) => {
    axios.get(URL + `ratings?page=${page}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleNetworkError(error);
        });
}

export const getRatings = (username, page, onSuccess) => {
    axios.get(URL + `ratings/${username}?page=${page}`)
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleNetworkError(error);
        });
}