import axios from "axios";
import {
    handleErrorResponse, setServiceToken, getServiceToken, removeServiceToken,
    setReauthenticationCallback, getConfig, handleNetworkError
} from "./common";

const URL = "http://localhost:8080/users/";

export const register = (user, onSuccess, onErrors, callback) => {
    axios.post(URL + 'sign', user)
        .then(response => {
            setServiceToken(response.data.serviceToken);
            setReauthenticationCallback(callback);
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors)
        });
}

export const logIn = (logInParams, onSuccess, onErrors, callback) => {
    axios.post(URL + 'logIn', logInParams)
        .then(response => {
            setServiceToken(response.data.serviceToken);
            setReauthenticationCallback(callback);
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const logInFromServiceToken = (onSuccess, callback) => {
    if (!getServiceToken()) {
        onSuccess();
        return;
    }
    axios.post(URL + 'logInFromServiceToken', {}, getConfig())
        .then(response => {
            setReauthenticationCallback(callback);
            onSuccess(response);
        }).catch(() => {
            removeServiceToken();
        });
}

export const logOut = () => {
    removeServiceToken();
}

export const updateProfile = (address, onSuccess, onErrors) => {
    axios.post(URL + 'profile', address, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        })
}

export const checkNewMessages = (chatId, onSuccess, onErrors) => {
    axios.get(URL + `chats/${chatId}/newMessages`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const checkNewMessagesGlobal = (onSuccess, onErrors) => {
    axios.get(URL + `chats/newMessages`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const markAsRead = (chatId, onSuccess, onErrors) => {
    axios.post(URL + `chats/${chatId}/changeStatus`, {}, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getChats = (page, onSuccess) => {
    axios.get(URL + `chats?page=${page}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleNetworkError(error);
        });
}

export const getChat = (chatId, onSuccess, onErrors) => {
    axios.get(URL + `chats/${chatId}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getChatIfExistent = (sellerId, productId, onSuccess, onErrors) => {
    axios.get(URL + `chats/find/${sellerId}/${productId}`, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getMessages = (chatId, page, onSuccess, onErrors) => {
    axios.get(URL + `chats/${chatId}/messages?page=${page}`, getConfig())
    .then(response => {
        onSuccess(response);
    }).catch(error => {
        handleErrorResponse(error, onErrors);
    });
}

export const connectStripe = (onSuccess, onErrors) => {
    axios.post(URL + "stripe/connect", {}, getConfig())
        .then(response => {
            onSuccess(response);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

