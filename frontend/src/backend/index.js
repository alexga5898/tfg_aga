import {init} from './common';
import * as userService from './userService';
import * as shoppingService from './shoppingService';
import * as catalogService from './catalogService';

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {init, userService, shoppingService, catalogService};