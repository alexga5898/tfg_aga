import axios from "axios";
import { getConfig, handleErrorResponse, handleNetworkError } from "./common";

const URL = "http://localhost:8080/products";

export const getUserProducts = (page, keywords, onSuccess) => {
    axios.get(URL + `/myProducts?page=${page}&keywords=${keywords}`, getConfig())
        .then(response => {
            onSuccess(response.data);
        }).catch(error => {
            handleNetworkError(error);
        });
}

export const getProduct = (productId, onSuccess, onErrors) => {
    axios.get(URL + `/${productId}`)
        .then(response => {
            onSuccess(response.data);
        }).catch(error => {
            handleErrorResponse(error, onErrors);
        });
}

export const getProducts = (page, keywords, category, condition, lPrice, hPrice, order, onSuccess, onErrors) => {
    axios.get(URL + `?page=${page}&keywords=${keywords}&category=${category}&condition=${condition}`
        + `&lPrice=${lPrice}&hPrice=${hPrice}&order=${order}`)
        .then(response => {
            onSuccess(response.data);
        }).catch(error => {
            handleErrorResponse(error, onErrors)
        });
}

export const getProductsByUsername = (page, username, onSuccess) => {
    axios.get(URL + `/user/${username}?page=${page}`)
        .then(response => {
            onSuccess(response.data);
        }).catch(error => {
            handleNetworkError(error);
        });
}