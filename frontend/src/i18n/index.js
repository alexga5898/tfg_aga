import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import detector from 'i18next-browser-languagedetector';
import moment from 'moment';

import translationEN from './locales/en/translation.json';
import translationES from './locales/es/translation.json';


const resources = {
    en: {
        translation: translationEN
    },
    es: {
        translation: translationES
    }
};

i18n
    .use(detector)
    .use(initReactI18next)
    .init({
        resources,
        interpolation: {
            escapeValue: false,
            format: function (value, format, lng) {
                if (value instanceof Date) return moment(value).locale(lng).format(format);
                return value;
            }
        }
    });

export default i18n;