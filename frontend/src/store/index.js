import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducer from './rootReducer'

const configStore = () => {

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || 
    compose;

    return createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
}

export default configStore;