import {combineReducers} from 'redux';
import app from '../modules/app'
import users from '../modules/users';
import shopping from '../modules/shopping';
import catalog from '../modules/catalog';

const reducer = combineReducers({
    app: app.reducer,
    users: users.reducer,
    shopping: shopping.reducer,
    catalog: catalog.reducer
});

export default reducer;