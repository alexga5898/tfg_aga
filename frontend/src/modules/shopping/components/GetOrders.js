import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";
import * as selectors from "../selectors";
import * as actions from "../actions";
import queryString from 'query-string';
import { useEffect, useState } from "react";
import Container from "react-bootstrap/esm/Container";
import Form from "react-bootstrap/esm/Form";
import { useTranslation } from "react-i18next/";
import Col from "react-bootstrap/esm/Col";
import Order from "./Order";
import Row from "react-bootstrap/esm/Row";
import { Paginate } from "../../common";
import PropTypes from 'prop-types';
import Spinner from "react-bootstrap/esm/Spinner";

const GetOrders = ({ buyer }) => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const orders = useSelector(selectors.getOrdersList);
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const [month, setMonth] = useState(queryString.parse(location.search).month ? queryString.parse(location.search).month : '');
    const [year, setYear] = useState(queryString.parse(location.search).year ? queryString.parse(location.search).year : '');
    const [asc, setAsc] = useState(queryString.parse(location.search).asc ? queryString.parse(location.search).asc : false);
    const hasNext = useSelector(selectors.getHasNextOrders);
    const totalPages = useSelector(selectors.getTotalPagesNumberOrders);
    const dispatch = useDispatch();
    const domain = buyer ? 'orders' : 'sells';
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        dispatch(actions.getOrders(
            page,
            month,
            year,
            asc,
            buyer,
            () => setLoading(false)));
    }, [dispatch, page, month, year, asc, buyer]);

    const filterOrder = (value) => {
        history.push(domain + `?page=0&month=${month}&year=${year}&asc=${value}&buyer=${buyer}`);
        setAsc(value);
    }

    const filterMonth = (value) => {
        history.push(domain + `?page=0&month=${value}&year=${year}&asc=${asc}&buyer=${buyer}`);
        setMonth(value);
    }

    const filterYear = (value) => {
        history.push(domain + `?page=0&month=${month}&year=${value}&asc=${asc}&buyer=${buyer}`);
        setYear(value);
    }

    const nextPage = () => {
        history.push(domain + `?page=${page + 1}&month=${month}&year=${year}&asc=${asc}&buyer=${buyer}`);
    }

    const previousPage = () => {
        history.push(domain + `?page=${page - 1}&month=${month}&year=${year}&asc=${asc}&buyer=${buyer}`);
    }

    const goPage = (page) => {
        history.push(domain + `?page=${page}&month=${month}&year=${year}&asc=${asc}&buyer=${buyer}`);
    }

    return (
        <Container className={"rounded"}>
            {buyer ?
                <h2>{t('Header.Orders')}</h2>
                :
                <h2>{t('Header.Sells')}</h2>
            }
            <br />
            <div>
                <Form>
                    <Form.Row>
                        <Col>
                            <Form.Check type="radio" label={t('Field.RecentOlderDate')} name="order"
                                value={false} onChange={e => filterOrder(e.target.value)}
                                defaultChecked={"false" === asc || false === asc} />
                        </Col>
                        <Col>
                            <Form.Check type="radio" label={t('Field.OlderRecentDate')} name="order"
                                value={true} onChange={e => filterOrder(e.target.value)}
                                defaultChecked={"true" === asc || true === asc} />
                        </Col>
                        <Col>
                            <Row>
                                <Col xs={3} lg={2}>
                                    <Form.Label>{t('Field.Month')}</Form.Label>
                                </Col>
                                <Col xs={9} lg={7}>
                                    <Form.Control as="select" onChange={e => filterMonth(e.target.value)} defaultValue={month}>
                                        <option value=''> - </option>
                                        <option value={1}>{t('Month.January')}</option>
                                        <option value={2}>{t('Month.Febrary')}</option>
                                        <option value={3}>{t('Month.March')}</option>
                                        <option value={4}>{t('Month.April')}</option>
                                        <option value={5}>{t('Month.May')}</option>
                                        <option value={6}>{t('Month.June')}</option>
                                        <option value={7}>{t('Month.July')}</option>
                                        <option value={8}>{t('Month.August')}</option>
                                        <option value={9}>{t('Month.September')}</option>
                                        <option value={10}>{t('Month.October')}</option>
                                        <option value={11}>{t('Month.November')}</option>
                                        <option value={12}>{t('Month.December')}</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <Col xs={3} lg={2}>
                                    <Form.Label>{t('Field.Year')}</Form.Label>
                                </Col>
                                <Col xs={9} lg={7}>
                                    <Form.Control as="select" onChange={e => filterYear(e.target.value)} defaultValue={year}>
                                        <option value=''> - </option>
                                        <option value={2019}>2019</option>
                                        <option value={2020}>2020</option>
                                        <option value={2021}>2021</option>
                                    </Form.Control>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Row>
                </Form>
            </div>
            <hr />
            {!loading ?
                <>
                    {orders && orders.length > 0 ?
                        <>
                            {orders.map(order => (
                                <Order key={order.id} order={order} buyer={buyer} />
                            ))}
                            <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()} next={() => nextPage()} go={(page) => goPage(page)} />
                        </>
                        : orders && orders.length === 0 ?
                            <div className="text-center">
                                <span className="text-muted">{t('Empty.Orders')}</span>
                            </div>
                            : ''}
                </>
                :
                <div className="d-flex justify-content-center mt-4 mb-3">
                    <Spinner animation="border" />
                </div>
            }
        </Container>
    )
}

GetOrders.propTypes = {
    buyer: PropTypes.bool
};

export default GetOrders;