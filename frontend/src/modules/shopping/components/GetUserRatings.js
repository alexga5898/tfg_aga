import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";
import * as selectors from "../selectors";
import * as actions from "../actions";
import queryString from 'query-string';
import { useEffect, useState } from "react";
import Container from "react-bootstrap/esm/Container";
import { Paginate } from "../../common";
import Rating from "./Rating";
import ListGroup from "react-bootstrap/esm/ListGroup";
import { useTranslation } from "react-i18next";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { Rating as Rate } from "@material-ui/lab";
import PropTypes from 'prop-types';
import Spinner from "react-bootstrap/esm/Spinner";

const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});

const GetUserRatings = ({ username, hideAvg }) => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const ratings = useSelector(selectors.getRatingsList);
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const hasNext = useSelector(selectors.getHasNextRatings);
    const totalPages = useSelector(selectors.getTotalPagesNumberRatings);
    const avg = useSelector(selectors.getRatingsAvg);
    const dispatch = useDispatch();
    const classes = useStyles();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (username) {
            dispatch(actions.getRatings(username, page, () => setLoading(false)));
        }
        else {
            dispatch(actions.getUserRatings(page, () => setLoading(false)));
        }
    }, [dispatch, page, username]);


    const nextPage = () => {
        history.push(`rating/?page=${page + 1}`);
    }

    const previousPage = () => {
        history.push(`rating/page=${page - 1}`);
    }

    const goPage = (page) => {
        history.push(`rating/page=${page}`);
    }

    return (
        <>
            {!loading ?
                <>
                    {!username ?
                        <Container className={"rounded"}>
                            <h2>{t('Header.Ratings')}</h2>
                            <p>{t('Rating.Text')}</p>
                            <div className={classes.root}>
                                <Box component="span" mr={1}>
                                    <Typography component="span" className="font-weight-bold">{t('Rating.Average')}: </Typography>
                                </Box>
                                <Rate
                                    readOnly
                                    name="half-rating"
                                    precision={0.5}
                                    value={avg || 0}
                                />
                            </div>
                            <hr />
                            {ratings && ratings.length > 0 ?
                                <>
                                    <ListGroup>
                                        {ratings.map(rating => (
                                            <Rating key={rating.id} rating={rating} />
                                        ))}
                                    </ListGroup>
                                    <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()} next={() => nextPage()} go={(page) => goPage(page)} />
                                </>
                                : ratings && ratings.length === 0 ?
                                    <div className="text-center">
                                        <span className="text-muted">{t('Empty.MyRatings')}</span>
                                    </div>
                                    : ''}
                        </Container>
                        :
                        <>
                            {!hideAvg ?
                                <div className={classes.root}>
                                    <Box component="span" mr={1}>
                                        <Typography component="span" className="font-weight-bold">{t('Rating.Average')}: </Typography>
                                    </Box>
                                    <Rate
                                        readOnly
                                        name="half-rating"
                                        precision={0.5}
                                        value={avg || 0}
                                    />
                                </div>
                                : ''}
                            <br />
                            {ratings && ratings.length > 0 ?
                                <>
                                    <ListGroup>
                                        {ratings.map(rating => (
                                            <Rating key={rating.id} rating={rating} />
                                        ))}
                                    </ListGroup>
                                    <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()} next={() => nextPage()} go={(page) => goPage(page)} />
                                </>
                                : ratings && ratings.length === 0 ?
                                    <div className="text-center">
                                        <span className="text-muted">{t('Empty.Ratings')}</span>
                                    </div>
                                    : ''}
                        </>
                    }
                </>
                :
                <div className="d-flex justify-content-center mt-3 mb-3">
                    <Spinner animation="border" />
                </div>
            }
        </>
    )
}

GetUserRatings.propTypes = {
    username: PropTypes.string,
    hideAvg: PropTypes.bool
};

export default GetUserRatings;