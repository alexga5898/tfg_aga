import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from 'react-router-dom';
import { Errors, Success } from '../../common';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import { useDispatch, useSelector } from "react-redux";
import * as actions from '../actions';
import configData from "../../../i18n/locales/en/translation.json"
import ImagesDropzone from "./ImagesDropzone";
import { hasStripeAccId } from "../../users/selectors"
import Modal from "react-bootstrap/esm/Modal";

const SellProduct = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const history = useHistory();
    const stripeAcc = useSelector(hasStripeAccId)
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const [category, setCategory] = useState('');
    const [condition, setCondition] = useState('');
    const [description, setDescription] = useState('');
    const [image1, setImage1] = useState(null);
    const [image2, setImage2] = useState(null);
    const [image3, setImage3] = useState(null);
    const [image4, setImage4] = useState(null);
    const [image5, setImage5] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);
    const [backendSuccess, setBackendSuccess] = useState(null);
    const [loading, setLoading] = useState(false);
    const [invalidFile, setInvalidFile] = useState(false);
    const validImages = "image/jpeg,image/png,image/jpg"

    useEffect(() => {
        if (loading) {
            if (backendSuccess || backendErrors) {
                setLoading(false);
            }
        }
        if (invalidFile) {
            if ((!image1 || validImages.includes(image1.type)) &&
                (!image2 || validImages.includes(image2.type)) &&
                (!image3 || validImages.includes(image3.type)) &&
                (!image4 || validImages.includes(image4.type)) &&
                (!image5 || validImages.includes(image5.type))) {
                setInvalidFile(false)
            }
        }
    }, [loading, backendSuccess, backendErrors, invalidFile, image1, image2, image3, image4, image5])

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setSubmitted(true);
        if (form.checkValidity()) {
            setLoading(true);
            let bodyFormData = new FormData();
            bodyFormData.append("name", name);
            bodyFormData.append("price", price);
            bodyFormData.append("quantity", quantity);
            bodyFormData.append("category", category);
            bodyFormData.append("condition", condition);
            bodyFormData.append("description", description);
            bodyFormData.append("images", image1);
            bodyFormData.append("images", image2);
            bodyFormData.append("images", image3);
            bodyFormData.append("images", image4);
            bodyFormData.append("images", image5);

            dispatch(actions.sellProduct(bodyFormData,
                () => {
                    setBackendSuccess(t('Products.AddProductSuccess'));
                    history.push('/shopping/products/myProducts');
                },
                errors => setBackendErrors(errors)
            ));
        }
    }

    let categories = [];
    let conditions = [];

    for (let categoryValue in configData.Categories) {
        categories.push(
            <option key={categoryValue} value={configData.Categories[categoryValue]}>{t(`Categories.${categoryValue}`)}</option>
        );
    }

    for (let conditionValue in configData.Conditions) {
        conditions.push(
            <option key={conditionValue} value={configData.Conditions[conditionValue]}>{t(`Conditions.${conditionValue}`)}</option>
        );
    }

    return (
        <div>
            <Modal show={!stripeAcc} centered onHide={() => console.log()}>
                <Modal.Header>
                    <Modal.Title>{t('Users.StripeTextHeader')}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>{t('Users.StripeText')}</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="success" onClick={() => history.push("/users/profile")}>{t('Users.GoButton')}</Button>
                    <Button variant="danger" onClick={() => history.goBack()}>{t('Users.ReturnButton')}</Button>
                </Modal.Footer>
            </Modal>
            <Container className={"rounded"}>
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <Success success={backendSuccess} onClose={() => setBackendSuccess(null)} />
                <h1>{t('Products.NewProductHeader')}</h1>
                <Form noValidate validated={submitted} onSubmit={e => submitHandler(e)}>
                    <Form.Group controlId="name">
                        <Form.Label>{t('Field.Name')}:</Form.Label>
                        <Form.Control required
                            onChange={e => setName(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="price">
                        <Form.Label>{t('Field.Price')}:</Form.Label>
                        <Form.Control required
                            type="number"
                            step=".01"
                            onChange={e => setPrice(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="quantity">
                        <Form.Label>{t('Field.Quantity')}:</Form.Label>
                        <Form.Control required
                            type="number"
                            onChange={e => setQuantity(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="category">
                        <Form.Label>{t('Field.Category')}:</Form.Label>
                        <Form.Control as="select"
                            required
                            onChange={e => setCategory(e.target.value)} >
                            <option></option>
                            {categories}
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="condition">
                        <Form.Label>{t('Field.Condition')}:</Form.Label>
                        <Form.Control as="select"
                            required
                            onChange={e => setCondition(e.target.value)} >
                            <option></option>
                            {conditions}
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>{t('Field.Description')}:</Form.Label>
                        <Form.Control required
                            as="textarea" rows={6}
                            onChange={e => setDescription(e.target.value)} />
                    </Form.Group>

                    <ImagesDropzone setImage1={setImage1} setImage2={setImage2} setImage3={setImage3}
                        setImage4={setImage4} setImage5={setImage5} validImages={validImages}
                        setInvalidFile={setInvalidFile} />

                    <Button variant="success" type="submit" disabled={loading || invalidFile} >
                        {!loading ? t('Products.SellButton') : t('App.Loading')}
                    </Button>
                </Form>
            </Container>
        </div >
    );
}

export default SellProduct;