import { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../actions";
import * as selectors from "../selectors";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types';
import { Errors } from "../../common";

const RemoveItemShoppingCart = ({ productId }) => {
    const dispatch = useDispatch();
    const shoppingCartId = useSelector(selectors.getShoppingCartId);
    const [backendErrors, setBackendErrors] = useState(null);

    const removeItem = () => {
        dispatch(actions.deleteItemCart(shoppingCartId,
            {
                productId: productId,
            },
            () => {
               setBackendErrors(null);
            },
            errors => setBackendErrors(errors)));
    }

    return (
        <>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Button variant="danger" onClick={() => removeItem()} size="sm">
                <FontAwesomeIcon icon={faTrash} size="sm" />
            </Button>
        </>
    )
}

RemoveItemShoppingCart.propTypes = {
    productId: PropTypes.number
};

export default RemoveItemShoppingCart;