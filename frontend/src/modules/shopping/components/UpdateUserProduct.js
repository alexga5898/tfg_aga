import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { getProduct } from '../../catalog/actions';
import * as selectors from '../../shopping/selectors';
import { useParams } from "react-router-dom";
import UpdateUserProductForm from "./UpdateUserProductForm";
import users from "../../users";
import { Errors } from "../../common";

const UpdateUserProduct = () => {
    const { productId } = useParams();
    const product = useSelector(selectors.getProduct);
    const dispatch = useDispatch();
    const userId = useSelector(users.selectors.getUserId);
    const userProductId = useSelector(selectors.getUserProductId);
    const isOwner = userId && userProductId && userId === userProductId;
    const [backendErrors, setBackendErrors] = useState(null);

    useEffect(() => {
        if (!Number.isNaN(productId)) {
            dispatch(getProduct(productId, 
                error => setBackendErrors(error)));
        }
    }, [productId, dispatch]);

    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)}/>
            {product && isOwner ?
                <UpdateUserProductForm product={product} />
                : ''}
        </div>
    )
}

export default UpdateUserProduct;