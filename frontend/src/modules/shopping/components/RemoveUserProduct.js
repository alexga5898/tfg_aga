import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import { useDispatch } from "react-redux";
import * as actions from '../actions';
import { Confirmation } from "../../common";
import PropTypes from 'prop-types';

const RemoveUserProduct = ({ productId }) => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const history = useHistory();
    const [showConfirmDialog, setShowConfirmDialog] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);

    const removeProduct = () => {
        if (productId) {
            dispatch(actions.removeProduct(
                productId,
                () => history.push("/shopping/products/myProducts"),
                errors => setBackendErrors(errors)
            ));
        }
    }

    return (
        <>
            <Button variant="danger" className="float-right" onClick={() => setShowConfirmDialog(true)}>
                {t("Products.DeleteButton")}
            </Button>
            <Confirmation show={showConfirmDialog} onClose={() => setShowConfirmDialog(false)}
                action={() => removeProduct()} errors={backendErrors} closeErrors={() => setBackendErrors(null)}/>
        </>
    );
}

RemoveUserProduct.propTypes = {
    productId: PropTypes.string
};


export default RemoveUserProduct;