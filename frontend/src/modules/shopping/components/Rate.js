import { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import Modal from "react-bootstrap/esm/Modal";
import Form from "react-bootstrap/esm/Form";
import { useTranslation } from "react-i18next/";
import { useDispatch } from "react-redux";
import { Errors, Success } from "../../common";
import * as actions from "../actions";
import Rating from '@material-ui/lab/Rating';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

const Rate = ({ orderId, onClose }) => {
    const [t] = useTranslation();
    const [show, setShow] = useState(false);
    const dispatch = useDispatch();
    const [rate, setRate] = useState(5);
    const [comment, setComment] = useState('');
    const [backendSuccess, setBackendSuccess] = useState(null);
    const [backendErrors, setBackendErrors] = useState(null);
    const [submitted, setSubmitted] = useState(false);

    const submitHandler = event => {
        const form = event.currentTarget;
        event.preventDefault();
        setSubmitted(true);
        if (validateForm(form)) {
            dispatch(actions.addRating(
                {
                    orderId: orderId,
                    rate: rate,
                    comment: comment
                },
                () => {
                    setShow(false);
                    setBackendErrors(null);
                    setSubmitted(false);
                    setRate(5);
                    setComment('');
                    setBackendSuccess(t('Rating.RatingSent'));
                    setTimeout(() => {
                        setBackendSuccess(null);
                        onClose();
                    }, 3000);
                },
                errors => setBackendErrors(errors)
            ))
        }
    }

    const validateForm = (form) => {
        if (!rate) {
            return false;
        }
        if (form.comment.value.length < 3 || form.comment.value.length > 300) {
            return false;
        }
        return true;
    }

    return (
        <div>
            <Success success={backendSuccess} onClose={() => setBackendSuccess(null)} />
            <Button variant="link" onClick={() => setShow(true)} className="button-as-link float-right mt-2">{t('Rating.RateLink')}</Button>
            <Modal show={show} onHide={() => setShow(false)} centered size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>
                        {t('Rating.RateHeader')}
                    </Modal.Title>
                </Modal.Header>
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <Form noValidate onSubmit={e => submitHandler(e)}>
                    <Modal.Body>
                        <Box component="fieldset" mb={1}>
                            <Typography component="legend">{t('Field.Rate')}: </Typography>
                            <Rating
                                name="simple-controlled"
                                value={rate}
                                color="red"
                                onChange={(event, newValue) => {
                                    setRate(newValue);
                                }}
                            />
                        </Box>
                        <Form.Group controlId="comment">
                            <Form.Label> {t('Field.Comment')}: </Form.Label>
                            <Form.Control required onChange={(e) => setComment(e.target.value)}
                                as="textarea" rows={5}
                                isInvalid={submitted && (comment.length < 3 || comment.length > 300)}
                                isValid={submitted && comment.length >= 3 && comment.length <= 300} />
                            <Form.Text id="ratingCommentHelp" muted>
                                {t('Field.RatingCommentHelp')}
                            </Form.Text>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="success" type="submit"> {t('Products.SendButton')} </Button>
                        <Button variant="danger" onClick={() => setShow(false)}> {t('Products.CancelButton')} </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    )
}

Rate.propTypes = {
    orderId: PropTypes.number,
    onClose: PropTypes.func
};


export default Rate;