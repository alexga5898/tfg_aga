import { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import Form from "react-bootstrap/esm/Form";
import InputGroup from "react-bootstrap/esm/InputGroup";
import Tooltip from "react-bootstrap/esm/Tooltip";
import OverlayTrigger from "react-bootstrap/esm/OverlayTrigger";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../actions";
import * as selectors from "../selectors";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Col from "react-bootstrap/esm/Col";
import PropTypes from 'prop-types';
import { Errors } from "../../common";
import RemoveItemShoppingCart from "./RemoveItemShoppingCart";
import { useTranslation } from "react-i18next/";

const UpdateItemShoppingCart = ({ shoppingCartItem }) => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const shoppingCartId = useSelector(selectors.getShoppingCartId);
    const [quantity, setQuantity] = useState(shoppingCartItem.quantity);
    const stock = shoppingCartItem.stock;
    const [backendErrors, setBackendErrors] = useState(null);

    const submitHandler = (value) => {
        setQuantity(value);
        if (value >= 1 && value <= stock) {
            dispatch(actions.updateItemCart(shoppingCartId,
                {
                    productId: shoppingCartItem.productId,
                    quantity: value
                },
                () => {
                    setBackendErrors(null);
                },
                errors => {
                    setBackendErrors(errors);
                }));
        }
        if (value > stock) {
            submitHandler(stock)
        }
    }

    const renderTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {shoppingCartItem.stock === 0 ?
                t('ShoppingCart.WarningNoStock')
                : shoppingCartItem.quantity > shoppingCartItem.stock ?
                    t('ShoppingCart.WarningQuantityUnavailable')
                    : ''
            }
        </Tooltip>
    );

    return (
        <>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Form noValidate inline>
                <Form.Row style={{ width: "100%" }} className="row justify-content-end">
                    {shoppingCartItem.quantity > shoppingCartItem.stock ?
                        <OverlayTrigger
                            placement="bottom"
                            delay={{ show: 100, hide: 250 }}
                            overlay={renderTooltip}>
                            <Button variant="light" size="sm" className="cart-warning-triangle">
                                <FontAwesomeIcon icon={faExclamationTriangle} color="#CCCC00" size="lg" />
                            </Button>
                        </OverlayTrigger>
                        : ''
                    }
                    <Col xs={7} lg={3} className="float-right">
                        <InputGroup size="sm">
                            <InputGroup.Prepend>
                                <Button onClick={() => submitHandler(quantity - 1)} disabled={quantity <= 1}>
                                    <FontAwesomeIcon icon={faMinus} size="sm" />
                                </Button>
                            </InputGroup.Prepend>
                            <Form.Control id="product-quantity-form-control" className="text-center" required type="number"
                                value={quantity} onChange={(e) => submitHandler(e.target.value)} />
                            <InputGroup.Append>
                                <Button onClick={() => submitHandler(Number(quantity) + 1)} disabled={quantity >= stock}>
                                    <FontAwesomeIcon icon={faPlus} size="sm" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                    <Col xs="auto">
                        <RemoveItemShoppingCart productId={shoppingCartItem.productId} />
                    </Col>
                </Form.Row>
            </Form>
        </>
    )
}

UpdateItemShoppingCart.propTypes = {
    shoppingCartItem: PropTypes.object
};

export default UpdateItemShoppingCart;