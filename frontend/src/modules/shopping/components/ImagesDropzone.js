import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Dropzone } from '../../common';
import PropTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ImagesDropzone = ({ product, setImage1, setImage2, setImage3, setImage4, 
        setImage5, setInvalidFile, setChanged, validImages }) => {

    const [t] = useTranslation();
    const images = product ? product.images : null;
    const [imagesN, setImagesN] = useState(null);
    const [imagesNCopy, setImagesNCopy] = useState(null);

    useEffect(() => {
        if (images) {
            setImagesN(images.length);
            setImagesNCopy(images.length);
            return;
        }
        if (imagesN !== imagesNCopy && setChanged) {
            setChanged(true);
        }
    }, [images, imagesN, imagesNCopy, setChanged])

    const addHandler = (setImage, image) => {
        setImage(image);
        if (image.size !== 0 && validImages.includes(image.type) && setChanged) {
            setChanged(true);
        }
    }

    const removeHandler = (setImage, image) => {
        setImage(image);
        if (image && setChanged) {
            setChanged(true);
        }
    }

    return (
        <div>
            <h2>{t('Products.ImagesHeader')}</h2>
            <br />
            <Row className="justify-content-md-center">
                <Col md="auto">
                    <Dropzone imageName={images && images[0] ? images[0].name : null}
                        imageUrl={images && images[0] ? `http://localhost:8080/products/images/${images[0].id}` : null}
                        addImage={(image) => addHandler(setImage1, image)}
                        removeImage={(image) => removeHandler(setImage1, image)}
                        invalidFile={() => setInvalidFile(true)} />
                </Col>
                <Col md="auto">
                    <Dropzone imageName={images && images[1] ? images[1].name : null}
                        imageUrl={images && images[1] ? `http://localhost:8080/products/images/${images[1].id}` : null}
                        addImage={(image) => addHandler(setImage2, image)}
                        removeImage={(image) => removeHandler(setImage2, image)}
                        invalidFile={() => setInvalidFile(true)} />
                </Col>
                <Col md="auto">
                    <Dropzone imageName={images && images[2] ? images[2].name : null}
                        imageUrl={images && images[2] ? `http://localhost:8080/products/images/${images[2].id}` : null}
                        addImage={(image) => addHandler(setImage3, image)}
                        removeImage={(image) => removeHandler(setImage3, image)}
                        invalidFile={() => setInvalidFile(true)} />
                </Col>
                <Col md="auto">
                    <Dropzone imageName={images && images[3] ? images[3].name : null}
                        imageUrl={images && images[3] ? `http://localhost:8080/products/images/${images[3].id}` : null}
                        addImage={(image) => addHandler(setImage4, image)}
                        removeImage={(image) => removeHandler(setImage4, image)}
                        invalidFile={() => setInvalidFile(true)} />
                </Col>
                <Col md="auto">
                    <Dropzone imageName={images && images[4] ? images[4].name : null}
                        imageUrl={images && images[4] ? `http://localhost:8080/products/images/${images[4].id}` : null}
                        addImage={(image) => addHandler(setImage5, image)}
                        removeImage={(image) => removeHandler(setImage5, image)}
                        invalidFile={() => setInvalidFile(true)} />
                </Col>
            </Row>
            <br />

        </div>)
}

ImagesDropzone.propTypes = {
    product: PropTypes.object,
    setImage1: PropTypes.func.isRequired,
    setImage2: PropTypes.func.isRequired,
    setImage3: PropTypes.func.isRequired,
    setImage4: PropTypes.func.isRequired,
    setImage5: PropTypes.func.isRequired,
    setInvalidFile: PropTypes.func,
    setChanged: PropTypes.func,
    validImages: PropTypes.string.isRequired
};

export default ImagesDropzone;
