import { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import Form from "react-bootstrap/esm/Form";
import InputGroup from "react-bootstrap/esm/InputGroup";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../actions";
import * as selectors from "../selectors";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Col from "react-bootstrap/esm/Col";
import PropTypes from 'prop-types';
import { Errors, Success } from "../../common";
import { useTranslation } from "react-i18next/";
import { isLoggedIn } from '../../users/selectors';
import { LogInModal } from "../../users";

const AddToShoppingCart = ({ productId, stock }) => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const shoppingCartId = useSelector(selectors.getShoppingCartId);
    const shoppingCartItems = useSelector(selectors.getShoppingCartItems);
    const [quantity, setQuantity] = useState(1);
    const [backendErrors, setBackendErrors] = useState(null);
    const [backendSuccess, setBackendSuccess] = useState(null);
    const loggedIn = useSelector(isLoggedIn);
    const [showLogInModal, setShowLogInModal] = useState(false);
    let disableIncrement = false;
    let disableAddToCart = false;

    if (shoppingCartItems) {
        for (let i = 0; i < shoppingCartItems.length; i++) {
            if (shoppingCartItems[i].productId === Number(productId)
                && shoppingCartItems[i].quantity + quantity >= stock) {
                disableIncrement = true;
            }
            if (shoppingCartItems[i].productId === Number(productId)
                && shoppingCartItems[i].quantity >= stock) {
                disableAddToCart = true;
            }
        }
    }

    const submitHandler = (event) => {
        event.preventDefault();
        if (!loggedIn) {
            setShowLogInModal(true);
            return;
        }
        dispatch(actions.addItemCart(shoppingCartId,
            {
                productId: productId,
                quantity: quantity
            },
            () => {
                setQuantity(1);
                setBackendSuccess(t('ShoppingCart.AddedToCartSuccess'));
                setTimeout(() => {
                    setBackendSuccess(null);
                }, 5000);
            },
            errors => setBackendErrors(errors)));
    }

    const onChange = (value) => {
        if (value > stock) {
            setQuantity(stock);
        } else {
            setQuantity(value);
        }
    }

    return (
        <>
            <Success success={backendSuccess} onClose={() => setBackendSuccess(null)}
                textLink={" " + t("ShoppingCart.LinkText")} link="/shopping/shoppingCart" />
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Form id="product-quantity-form" noValidate onSubmit={e => submitHandler(e)}>
                <Form.Row id="product-quantity-form-control" className="d-flex flex-row">
                    <Col xs={5} lg={3}>
                        <InputGroup size="sm">
                            <InputGroup.Prepend>
                                <Button onClick={() => setQuantity(quantity - 1)} disabled={quantity <= 1}>
                                    <FontAwesomeIcon icon={faMinus} size="sm" />
                                </Button>
                            </InputGroup.Prepend>
                            <Form.Control id="product-quantity-form-control" className="text-center" required type="number"
                                value={quantity} onChange={(e) => onChange(e.target.value)} />
                            <InputGroup.Append>
                                <Button onClick={() => setQuantity(Number(quantity) + 1)} disabled={quantity === stock || disableIncrement || stock === 0}>
                                    <FontAwesomeIcon icon={faPlus} size="sm" />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
                <Button type="submit" className="top-buffer" disabled={stock === 0 || quantity > stock || disableAddToCart}>
                    {t('ShoppingCart.AddToCartButton')}
                </Button>
            </Form>
            <LogInModal show={showLogInModal} onClose={() => setShowLogInModal(false)} />
        </>
    )
}

AddToShoppingCart.propTypes = {
    productId: PropTypes.string,
    stock: PropTypes.number
};

export default AddToShoppingCart;