import Container from "react-bootstrap/esm/Container";
import { useTranslation } from "react-i18next/";
import { useLocation } from "react-router";
import queryString from 'query-string';

const BuySuccess = () => {
    const [t] = useTranslation();
    const location = useLocation();
    const sessionId = queryString.parse(location.search).session_id ? queryString.parse(location.search).session_id : '';

    return (
        <>
            {sessionId ?
                <Container className="rounded">
                    <Container className="border border-4 border-success rounded text-center">
                        <h2 className="text-success font-weight-bold">{t('Order.OrderCompleted')}</h2>
                    </Container>
                    <br />
                    <p>{t('Order.OrderCompletedText')}</p>
                </Container>
                : ''}
        </>
    );

}

export default BuySuccess;