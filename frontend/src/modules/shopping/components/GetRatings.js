import GetUserRatings from "./GetUserRatings";
import PropTypes from 'prop-types';

const GetRatings = ({ username, hideAvg }) => {
    return (
        <GetUserRatings username={username} hideAvg={hideAvg}/>
    );
}

GetRatings.propTypes = {
    username: PropTypes.string,
    hideAvg: PropTypes.bool
};

export default GetRatings;

