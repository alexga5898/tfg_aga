import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux"
import * as actions from '../../shopping/actions';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Errors, Success } from '../../common';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import configData from "../../../i18n/locales/en/translation.json"
import Button from "react-bootstrap/esm/Button";
import PropTypes from 'prop-types';
import ImagesDropzone from "./ImagesDropzone";
import RemoveUserProduct from "./RemoveUserProduct";

const UpdateUserProductForm = ({ product }) => {
    const [t] = useTranslation();
    const { productId } = useParams();
    const dispatch = useDispatch();
    const [name, setName] = useState(product.name);
    const [price, setPrice] = useState(product.price);
    const [quantity, setQuantity] = useState(product.quantity);
    const [category, setCategory] = useState(product.category);
    const [condition, setCondition] = useState(product.condition);
    const [description, setDescription] = useState(product.description);
    const [backendErrors, setBackendErrors] = useState(null);
    const [backendSuccess, setBackendSuccess] = useState(null);
    const [changed, setChanged] = useState(false);
    const [image1, setImage1] = useState(null);
    const [image2, setImage2] = useState(null);
    const [image3, setImage3] = useState(null);
    const [image4, setImage4] = useState(null);
    const [image5, setImage5] = useState(null);
    const [invalidFile, setInvalidFile] = useState(false);
    const validImages = "image/jpeg,image/png,image/jpg";

    useEffect(() => {
        if (invalidFile) {
            if ((!image1 || validImages.includes(image1.type)) &&
                (!image2 || validImages.includes(image2.type)) &&
                (!image3 || validImages.includes(image3.type)) &&
                (!image4 || validImages.includes(image4.type)) &&
                (!image5 || validImages.includes(image5.type))) {
                setInvalidFile(false);
            }
        }

    }, [invalidFile, image1, image2, image3, image4, image5])

    const submitProductInfoHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() && !invalidFile) {
            let bodyFormData = new FormData();
            bodyFormData.append("name", name);
            bodyFormData.append("price", price);
            bodyFormData.append("quantity", quantity);
            bodyFormData.append("category", category);
            bodyFormData.append("condition", condition);
            bodyFormData.append("description", description);
            bodyFormData.append("images", image1);
            bodyFormData.append("images", image2);
            bodyFormData.append("images", image3);
            bodyFormData.append("images", image4);
            bodyFormData.append("images", image5);
            dispatch(actions.updateProduct(
                productId,
                bodyFormData,
                () => {
                    setBackendSuccess(t('Users.UserProfileUpdated'));
                    setBackendErrors(null);
                    setChanged(false);
                    setTimeout(() => {
                        setBackendSuccess(null);
                    }, 3000);
                },
                errors => setBackendErrors(errors)
            ));
        }
        else {
            setBackendErrors(null);
        }
    }

    const changeHandler = (event, object) => {
        const form = event.currentTarget;
        const elements = form.elements;
        for (let a = 0; a < elements.length; a++) {
            if ((elements[a].type === "text" || elements[a].type === "number") && elements[a].value.length === 0) {
                setChanged(false);
                return;
            }
        }
        for (let a = 0; a < elements.length; a++) {
            if ((elements[a].type === "text" || elements[a].type === "number") && elements[a].value !== object[elements[a].id]) {
                setChanged(true);
                return;
            }
        }
        setChanged(false);
    }

    if (!product) {
        return null;
    }

    let categories = [];
    let conditions = [];

    for (let categoryValue in configData.Categories) {
        categories.push(
            <option key={categoryValue} value={configData.Categories[categoryValue]}>{t(`Categories.${categoryValue}`)}</option>
        );
    }

    for (let conditionValue in configData.Conditions) {
        conditions.push(
            <option key={conditionValue} value={configData.Conditions[conditionValue]}>{t(`Conditions.${conditionValue}`)}</option>
        );
    }

    return (
        <div>
            <Container className={"rounded"}>
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <Success success={backendSuccess} onClose={() => setBackendSuccess(null)} />
                <h2>{t('Products.ProductHeader')}</h2>
                <Form noValidate onSubmit={e => submitProductInfoHandler(e)} onChange={e => changeHandler(e, product)}>
                    <Form.Group as={Row} controlId="name">
                        <Form.Label column sm={3}>{t('Field.Name')}:</Form.Label>
                        <Col xs={9} lg={9}>
                            <Form.Control required
                                defaultValue={name}
                                onChange={e => setName(e.target.value)} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="price">
                        <Form.Label column sm={3}>{t('Field.Price')}:</Form.Label>
                        <Col sm={9}>
                            <Form.Control required
                                defaultValue={price}
                                type="number"
                                step=".01"
                                onChange={e => setPrice(e.target.value)} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="quantity">
                        <Form.Label column sm={3}>{t('Field.Quantity')}:</Form.Label>
                        <Col sm={9}>
                            <Form.Control required
                                defaultValue={quantity}
                                type="number"
                                onChange={e => setQuantity(e.target.value)} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="category">
                        <Form.Label column sm={3}>{t('Field.Category')}:</Form.Label>
                        <Col sm={9}>
                            <Form.Control as="select"
                                required
                                defaultValue={category}
                                onChange={e => setCategory(e.target.value)} >
                                <option></option>
                                {categories}
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="condition">
                        <Form.Label column sm={3}>{t('Field.Condition')}:</Form.Label>
                        <Col sm={9}>
                            <Form.Control as="select"
                                required
                                defaultValue={condition}
                                onChange={e => setCondition(e.target.value)} >
                                <option></option>
                                {conditions}
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="description">
                        <Form.Label column sm={3}>{t('Field.Description')}:</Form.Label>
                        <Col sm={9}>
                            <Form.Control required
                                as="textarea" rows={6}
                                defaultValue={description}
                                onChange={e => setDescription(e.target.value)} />
                        </Col>
                    </Form.Group>
                    <ImagesDropzone product={product} setImage1={setImage1} setImage2={setImage2}
                        setImage3={setImage3} setImage4={setImage4} setImage5={setImage5}
                        setInvalidFile={setInvalidFile} setChanged={setChanged} validImages={validImages} />
                    <Button variant="success" type="submit" disabled={!changed || invalidFile}>
                        {t('Products.SaveChangesButton')}
                    </Button>
                    <RemoveUserProduct productId={productId} />
                </Form>
            </Container>


        </div>
    )
}

UpdateUserProductForm.propTypes = {
    product: PropTypes.object
};

export default UpdateUserProductForm;