import OrderDetails from "./OrderDetails"

const SellDetails = () => {
    return (
        <OrderDetails buyer={false}/>
    )
}

export default SellDetails;