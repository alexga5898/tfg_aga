import GetOrders from "./GetOrders"

const GetSells = () => {
    return (
        <GetOrders buyer={false}/>
    )
}

export default GetSells;