import { useHistory } from "react-router";
import ListGroup from "react-bootstrap/esm/ListGroup";
import { useTranslation } from "react-i18next/";
import Button from "react-bootstrap/esm/Button";
import Rate from "./Rate";
import { useState } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const Order = ({ order, buyer }) => {
    const [t] = useTranslation();
    const history = useHistory();
    const [rated, setRated] = useState(order.rated);

    return (
        <div>
            <ListGroup>
                <ListGroup.Item variant="dark">
                    <span className="font-weight-bold">{t("Order.OrderNumber")}: </span>
                    <span>{order.id}</span>
                    {buyer ?
                        <>
                            <Button className="float-right mt-2" onClick={() => history.push(`orders/${order.id}`)}>{t('Order.DetailsButton')}</Button>
                            <br />
                            <span className="font-weight-bold">{t('Order.Seller')}: </span>
                            <Link to={`/users/seller/${order.sellerUsername}`}>{order.sellerUsername}</Link>
                            <br />
                            {!rated ?
                                <Rate orderId={order.id} onClose={() => setRated(true)} />
                                : ''}
                        </>
                        :
                        <>
                            <br />
                            <span className="font-weight-bold">{t('Order.Buyer')}: </span>
                            <Link to={`/users/seller/${order.buyerUsername}`}>{order.buyerUsername}</Link>
                            <Button className="float-right mt-2" onClick={() => history.push(`sells/${order.id}`)}>{t('Order.DetailsButton')}</Button>
                            <br />
                        </>
                    }

                    <span className="font-weight-bold">{t('ShoppingCart.Total')}: </span>

                    <span>{order.totalPrice}€</span>
                    <br />
                    <span className="font-weight-bold">{t('Order.Date')}: </span>
                    <span>{t('Date.Format2', { date: new Date(order.date) })}</span>

                </ListGroup.Item>
                {order.items.map(orderItem => (
                    <ListGroup.Item key={orderItem.id} action onClick={() => history.push(`/products/${orderItem.productId}`)}>
                        <div className="order-info-img">
                            <img className="order-info-img-thumbnail" src={`http://localhost:8080/products/${orderItem.productId}/thumbnail`} alt="" />
                        </div>
                        <div>
                            <span>{orderItem.productName}</span>
                            <br />
                            <span>{orderItem.price}€ x {orderItem.quantity}</span>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup>
            <br />
        </div>
    )
}

Order.propTypes = {
    order: PropTypes.object,
    buyer: PropTypes.bool
};


export default Order;