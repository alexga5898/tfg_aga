import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom";
import * as selectors from "../selectors";
import * as actions from "../actions";
import { useEffect, useState } from "react";
import Container from "react-bootstrap/esm/Container";
import { useTranslation } from "react-i18next/";
import Button from "react-bootstrap/esm/Button";
import Table from "react-bootstrap/esm/Table";
import { BackButton, Errors } from "../../common";

const OrderDetails = ({ buyer }) => {
    const [t] = useTranslation();
    const history = useHistory();
    const dispatch = useDispatch();
    const { orderId } = useParams();
    const order = useSelector(selectors.getOrder);
    const [attempt, setAttempt] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);

    useEffect(() => {
        if (!attempt) {
            dispatch(actions.getOrder(
                orderId,
                error => setBackendErrors(error)));
            setAttempt(true);
        }

    }, [attempt, dispatch, backendErrors, orderId]);

    if (!order) {
        return null;
    }

    return (
        <>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Container className="rounded">
                <div className="d-flex">
                    <BackButton />
                    <h2 className="font-weight-bold ml-4">{t('Order.OrderSummary')}</h2>
                </div>
                <div key={order.id}>
                    <hr />
                    {buyer ?
                        <h5 className="font-weight-bold">{t('Order.Seller')}: {order.sellerUsername}</h5>
                        :
                        <h5 className="font-weight-bold">{t('Order.Buyer')}: {order.buyerUsername}</h5>
                    }
                    <br />
                    <Table responsive hover>
                        <thead className="thead-dark">
                            <tr>
                                <th></th>
                                <th className="text-center">{t('Field.Product')}</th>
                                <th className="text-center">{t('Field.Price')}</th>
                                <th className="text-center">{t('Field.Quantity')}</th>
                                <th className="text-center">{t('ShoppingCart.Total')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {order.items.map(item => (
                                <tr key={item.id} onClick={() => history.push(`/products/${item.productId}`)}>
                                    <td align="center"><img className="product_mini" src={`http://localhost:8080/products/${item.productId}/thumbnail`} alt="" /></td>
                                    <td align="center">{item.productName}</td>
                                    <td align="center">{item.price}€</td>
                                    <td align="center">{item.quantity}</td>
                                    <td align="center">{Number(item.quantity) * Number(item.price)}€</td>
                                </tr>
                            ))}
                            <tr className="table-active">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center" className="font-weight-bold">{order.totalPrice}€</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
                <hr />
                <h4 className="font-weight-bold">{t('Order.AddressSummary')}</h4>
                <hr />
                <Table responsive>
                    <thead className="thead-dark">
                        <tr>
                            <th className="text-center">{t('Field.Country')}</th>
                            <th className="text-center">{t('Field.City')}</th>
                            <th className="text-center">{t('Field.Street')}</th>
                            <th className="text-center">{t('Field.PostalCode')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td align="center">{order.buyerAddressDto.country}</td>
                            <td align="center">{order.buyerAddressDto.city}</td>
                            <td align="center">{order.buyerAddressDto.street}</td>
                            <td align="center">{order.buyerAddressDto.postalCode}</td>
                        </tr>
                    </tbody>
                </Table>
                <br />
                <Button className="return-button" onClick={() => history.goBack()}>{t('Order.ReturnFromDetailsButton')}</Button>
                <br />
            </Container>
        </>
    )
}

export default OrderDetails;