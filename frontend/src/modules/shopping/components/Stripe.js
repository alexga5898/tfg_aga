import React, { useState } from "react";
import { loadStripe } from "@stripe/stripe-js";
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next/";
import { Errors } from "../../common";
import conf from "../../../config.json";

const stripePromise = loadStripe(conf.StripeKey);

const Stripe = ({ shoppingCart, disabled }) => {
    const [t] = useTranslation();
    const [backendErrors, setBackendErrors] = useState(null);

    const handleClick = async (event) => {
        const stripe = await stripePromise;
        const response = await fetch("http://localhost:8080/shopping/shoppingCart/checkout", {
            method: "POST",
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("serviceToken"),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(shoppingCart)
        });
        const session = await response.json();
        const result = await stripe.redirectToCheckout({
            sessionId: session.id,
        });
        if (result.error) {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
            setBackendErrors(result.error.message);
        }
    };

    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors}/>
            <br />
            <button type="button" id="checkout-button" role="link" onClick={handleClick} disabled={disabled}>
                {t('ShoppingCart.Checkout')}
            </button>
            <br />
        </div>

    );
}

Stripe.propTypes = {
    shoppingCart: PropTypes.object,
    disabled: PropTypes.bool
};


export default Stripe;