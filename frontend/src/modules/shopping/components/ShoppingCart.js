import Container from "react-bootstrap/esm/Container";
import ListGroup from "react-bootstrap/esm/ListGroup";
import { useTranslation } from "react-i18next/";
import { useSelector } from "react-redux";
import * as selectors from "../selectors";
import ShoppingCartItem from "./ShoppingCartItem";
import Stripe from "./Stripe";

const ShoppingCart = () => {
    const [t] = useTranslation();
    const shoppingCartItems = useSelector(selectors.getShoppingCartItems);
    const shoppingCart = useSelector(selectors.getShoppingCart);
    let disableStripeButton = false

    for (let i = 0; i < shoppingCartItems.length; i++) {
        if (shoppingCartItems[i].quantity > shoppingCartItems[i].stock) {
            disableStripeButton = true;
        }
    }


    return (
        <Container>
            <h2>{t('Header.ShoppingCart')}</h2>
            <br />
            {shoppingCartItems ?
                <ListGroup>
                    {shoppingCartItems.map(shoppingCartItem =>
                        <ShoppingCartItem key={shoppingCartItem.productId} shoppingCartItem={shoppingCartItem} />
                    )}
                </ListGroup>
                : ''}
            {shoppingCart && shoppingCartItems.length > 0 ?
                <div className="text-right">
                    <hr />
                    <span className="font-weight-bold">{t('ShoppingCart.Items')}: {shoppingCart.totalQuantity}</span>
                    <br />
                    <span className="font-weight-bold">{t('ShoppingCart.Total')}: {shoppingCart.totalPrice}€</span>
                    <Stripe shoppingCart={shoppingCart} disabled={disableStripeButton} />
                </div>
                : ''}
        </Container>
    )
}

export default ShoppingCart;