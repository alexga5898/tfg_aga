import Col from "react-bootstrap/esm/Col";
import Container from "react-bootstrap/esm/Container";
import ListGroup from "react-bootstrap/esm/ListGroup"
import Row from "react-bootstrap/esm/Row";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import UpdateItemShoppingCart from "./UpdateItemShoppingCart";

const ShoppingCartItem = ({ shoppingCartItem }) => {

    return (
        <ListGroup.Item>
            <Link to={`/products/${shoppingCartItem.productId}`}>
                <div className="question-info-img">
                    <img className="question-info-img-thumbnail" src={`http://localhost:8080/products/${shoppingCartItem.productId}/thumbnail`} alt="" />
                </div>
            </Link>
            <div className="item-cart-info" >
                <Container fluid>
                    <Row className="item-cart-row">
                        <Col>
                            <div>
                                <span>
                                    <h5>
                                        {shoppingCartItem.productName}
                                    </h5>
                                </span>
                                <span>
                                    {shoppingCartItem.price}€
                            </span>
                            </div>
                        </Col>
                        <Col>
                            <div>
                                <UpdateItemShoppingCart shoppingCartItem={shoppingCartItem} />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div >
        </ListGroup.Item >
    )
}

ShoppingCartItem.propTypes = {
    shoppingCartItem: PropTypes.object,
};

export default ShoppingCartItem;