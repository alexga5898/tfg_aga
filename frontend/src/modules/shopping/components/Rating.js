import { Box, makeStyles, Typography } from "@material-ui/core";
import { Rating as Rate } from '@material-ui/lab';
import ListGroup from "react-bootstrap/esm/ListGroup";
import { Trans, useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});

const Rating = ({ rating }) => {
    const [t] = useTranslation();

    const classes = useStyles();

    return (
        <ListGroup.Item>
            {rating.orderId ?
                <>
                    <span className="font-weight-bold">{t('Order.OrderNumber')}: </span>
                    <Link to={`/shopping/orders/${rating.orderId}`}>{rating.orderId}</Link>
                    <br />
                </>
                : ''
            }
            <div className={classes.root}>
                <Box component="span" mr={1}>
                    <Typography component="span" className="font-weight-bold">{t('Field.Rate')}: </Typography>
                </Box>
                <Rate
                    readOnly
                    size="small"
                    name="simple-controlled"
                    value={rating.rate}
                />
            </div>
            <Typography component="span">{rating.comment}</Typography>
            <br />
            {rating.date ?
                <span className="text-muted small">
                    <Trans i18nKey='Rating.RatingAuthor' values={{ name: rating.buyerUsername, date: t('Date.Format', { date: new Date(rating.date) }) }} />
                </span>
                :
                <span className="text-muted small">
                    <Trans i18nKey='Rating.RatingAuthorNoDate' values={{ name: rating.buyerUsername }} />
                </span>
            }
        </ListGroup.Item>
    )
}

Rating.propTypes = {
    rating: PropTypes.object
};

export default Rating;