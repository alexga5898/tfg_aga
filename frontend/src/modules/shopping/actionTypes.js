export const ADDED_PRODUCT = "added_product";
export const GET_USER_PRODUCTS = "get_user_products";
export const GET_PRODUCT = "get_product";
export const UPDATED_USER_PRODUCT = "updated_user_product";
export const REMOVED_USER_PRODUCT = "removed_user_product";
export const ITEM_ADDED_TO_CART = "item_added_to_cart";
export const ITEM_UPDATED_CART = "item_updated_cart";
export const ITEM_DELETED_CART = "item_deleted_cart";
export const BUY_COMPLETED = "buy_completed";
export const GET_ORDERS = "get_orders";
export const GET_ORDER = "get_order";
export const RATED = "rated";
export const GET_USER_RATINGS = "get_user_ratings";
export const GET_RATINGS = "get_ratings";

