import { combineReducers } from 'redux';
import * as actionTypes from './actionTypes';
import * as userActions from '../users/actionTypes';

const initialState = {
    products: null,
    product: null,
    shoppingCart: null,
    orderPlaced: null,
    order: null,
    orders: [],
    rating: null,
    ratings: []
}

const products = (state = initialState.products, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_PRODUCTS:
            return action.products;
        default:
            return state;
    }
}

const product = (state = initialState.product, action) => {
    switch (action.type) {
        case actionTypes.ADDED_PRODUCT:
            return action.product;
        case actionTypes.GET_PRODUCT:
            return action.product;
        case actionTypes.REMOVED_USER_PRODUCT:
            return initialState.product
        default:
            return state;
    }
}

const shoppingCart = (state = initialState.shoppingCart, action) => {
    switch (action.type) {
        case userActions.LOGGED_IN:
            return action.authenticatedUser.shoppingCartDto;
        case userActions.REGISTERED:
            return action.authenticatedUser.shoppingCartDto;
        case actionTypes.ITEM_ADDED_TO_CART:
            return action.shoppingCart;
        case actionTypes.ITEM_UPDATED_CART:
            return action.shoppingCart;
        case actionTypes.ITEM_DELETED_CART:
            return action.shoppingCart;
        default:
            return state;
    }
}


const orderPlaced = (state = initialState.orderPlaced, action) => {
    switch (action.type) {
        case actionTypes.BUY_COMPLETED:
            return action.order;
        default:
            return state;
    }
}

const order = (state = initialState.order, action) => {
    switch (action.type) {
        case actionTypes.GET_ORDER:
            return action.order;
        default:
            return state;
    }
}

const orders = (state = initialState.orders, action) => {
    switch (action.type) {
        case actionTypes.GET_ORDERS:
            return action.orders;
        default:
            return state;
    }
}

const rating = (state = initialState.rating, action) => {
    switch (action.type) {
        case actionTypes.RATED:
            return action.rate;
        default:
            return state
    }
}

const ratings = (state = initialState.ratings, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_RATINGS:
            return action.ratings;
        case actionTypes.GET_RATINGS:
            return action.ratings;
        default:
            return state;
    }
}

const reducer = combineReducers({
    products,
    product,
    shoppingCart,
    orderPlaced,
    order,
    orders,
    rating,
    ratings
});

export default reducer;