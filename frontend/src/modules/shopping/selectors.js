const getState = state =>
    state.shopping

export const getProduct = state =>
    getState(state).product

export const getProductInf = state =>
    getProduct(state) ? getProduct(state).productDto : null

export const getUserProductId = state =>
    getProduct(state) ? getProduct(state).sellerId : null

export const getShoppingCart = state =>
    getState(state).shoppingCart

export const getShoppingCartId = state =>
    getShoppingCart(state) ? getShoppingCart(state).id : null

export const getShoppingCartItems = state =>
    getShoppingCart(state) ? getShoppingCart(state).items : []

export const getOrder = state =>
    getState(state).order

export const getOrderPlaced = state =>
    getState(state).orderPlaced

export const getOrderInf = state =>
    getOrderPlaced(state) ? getOrderPlaced(state).items : null

export const getTotal = state =>
    getOrderPlaced(state) ? getOrderPlaced(state).total : null

export const isOrderPlaced = state =>
    getOrderInf(state) ? true : false

const getOrders = state =>
    getState(state).orders

export const getOrdersList = state =>
    getOrders(state) ? getOrders(state).items : []

export const getHasNextOrders = state =>
    getOrders(state) ? getOrders(state).existMoreItems : false

export const getTotalPagesNumberOrders = state =>
    getOrders(state) ? getOrders(state).totalPages : 0

const getRatings = state =>
    getState(state).ratings

const getRatingsElement = state =>
    getRatings(state) ? getRatings(state).ratings : null

export const getRatingsList = state =>
    getRatingsElement(state) ? getRatingsElement(state).items : []

export const getHasNextRatings = state =>
    getRatingsElement(state) ? getRatingsElement(state).existMoreItems : false

export const getTotalPagesNumberRatings = state =>
    getRatingsElement(state) ? getRatingsElement(state).totalPages : 0

export const getRatingsAvg = state =>
    getRatings(state) ? getRatings(state).avg : 0