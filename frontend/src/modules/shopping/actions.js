import * as actionTypes from './actionTypes';
import backend from '../../backend';

const added_product = product => (
    {
        type: actionTypes.ADDED_PRODUCT,
        product
    }
);

export const sellProduct = (product, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.sellProduct(product,
        response => {
            dispatch(added_product(response.data));
            onSuccess();
        },
        onErrors);
}

const updated_product = (product) => (
    {
        type: actionTypes.UPDATED_USER_PRODUCT,
        product
    }
)

export const updateProduct = (productId, product, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.updateProduct(productId, product,
        response => {
            dispatch(updated_product(response.data));
            onSuccess();
        },
        onErrors);
}

export const removeProduct = (productId, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.removeProduct(productId, 
        response => {
            dispatch({type: actionTypes.REMOVED_USER_PRODUCT});
            onSuccess();
        },
        onErrors);
}

const item_added_to_cart = (shoppingCart) => (
    {
        type: actionTypes.ITEM_ADDED_TO_CART,
        shoppingCart
    }
)

export const addItemCart = (shoppingCartId, addItemsToCartParams, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.addItemCart(shoppingCartId, addItemsToCartParams,
        response => {
            dispatch(item_added_to_cart(response.data));
            onSuccess();
        },
        onErrors);
}

const item_updated_cart = (shoppingCart) => (
    {
        type: actionTypes.ITEM_UPDATED_CART,
        shoppingCart
    }
)

export const updateItemCart = (shoppingCartId, updateItemsCartParams, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.updateItemCart(shoppingCartId, updateItemsCartParams,
        response => {
            dispatch(item_updated_cart(response.data));
            onSuccess();
        },
        onErrors);
}

const item_deleted_cart = (shoppingCart) => (
    {
        type: actionTypes.ITEM_DELETED_CART,
        shoppingCart
    }
)

export const deleteItemCart = (shoppingCartId, deleteItemsCartParams, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.deleteItemCart(shoppingCartId, deleteItemsCartParams,
        response => {
            dispatch(item_deleted_cart(response.data));
            onSuccess();
        },
        onErrors);
}

const buy_completed = (order) => (
    {
        type: actionTypes.BUY_COMPLETED,
        order
    }
)

export const buySuccess = (sessionId, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.buySuccess(sessionId,
        response => {
            dispatch(buy_completed(response.data));
            onSuccess();
        },
        onErrors);
}

const get_orders = (orders) => (
    {
        type: actionTypes.GET_ORDERS,
        orders
    }
)

export const getOrders = (page, month, year, asc, buyer, onSuccess) => dispatch => {    
    backend.shoppingService.getOrders(page, month, year, asc, buyer,
        response => {
            dispatch(get_orders(response.data));
            onSuccess();
        });
}

const get_order = (order) => (
    {
        type: actionTypes.GET_ORDER,
        order
    }
)

export const getOrder = (orderId, onErrors) => dispatch => {    
    backend.shoppingService.getOrder(orderId, 
        response => {
            dispatch(get_order(response.data));
        },
        onErrors);
}

const rated = (rate) => (
    {
        type: actionTypes.RATED,
        rate
    }
)

export const addRating = (rate, onSuccess, onErrors) => dispatch => {
    backend.shoppingService.addRating(rate,
        response => {
            dispatch(rated(response.data));
            onSuccess();
        },
        onErrors)
}

const get_user_ratings = (ratings) => (
    {
        type: actionTypes.GET_USER_RATINGS,
        ratings
    }
)

export const getUserRatings = (page, onSuccess) => dispatch => {
    backend.shoppingService.getUserRatings(page,
        response => {
            dispatch(get_user_ratings(response.data));
            onSuccess();
        })
}

const get_ratings = (ratings) => (
    {
        type: actionTypes.GET_RATINGS,
        ratings
    }
)

export const getRatings = (username, page, onSuccess) => dispatch => {
    backend.shoppingService.getRatings(username, page,
        response => {
            dispatch(get_ratings(response.data));
            onSuccess();
        })
}