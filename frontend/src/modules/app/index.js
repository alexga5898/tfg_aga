import * as actions from './actions';
import reducer from './reducer';
import * as selectors from './selectors';

export {default as App} from "./components/App";

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {actions, reducer, selectors}