const getState = state => 
    state.app
    
export const getError = state => 
    getState(state).error;