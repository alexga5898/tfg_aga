import React from "react";
import { useTranslation } from 'react-i18next';

const Footer = () => {
    const [t] = useTranslation();

    return (
        <footer className="footer bg-dark">
            <div className="footer-copyright text-center">
                <p> {t('Footer.Text')} </p>
                <span className="text-muted small">
                    Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a>, <a href="https://www.flaticon.com/authors/iconixar" title="iconixar">iconixar</a>, <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> and <a href="https://smashicons.com/" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                </span>
            </div>
        </footer>
    )
}

export default Footer;