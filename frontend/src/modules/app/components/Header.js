import React, { useState } from 'react';
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Navbar from 'react-bootstrap/Navbar';
import DropdownButton from 'react-bootstrap/DropdownButton';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/esm/Dropdown';
import { LogInModal } from '../../users';
import { isLoggedIn } from '../../users/selectors';
import { LinkContainer } from 'react-router-bootstrap';
import Button from 'react-bootstrap/Button';
import { FindProductsForm } from '../../catalog';
import { useHistory } from 'react-router';
import Sidebar from './Sidebar';

const Header = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const loggedIn = useSelector(isLoggedIn);
    const [show, setShow] = useState(false);
    const [sidebar, setSidebar] = useState(false);

    return (
        <div className="header">
            <div className="topbar">
                <Navbar bg="dark" variant="dark" expand="sm">
                    <button className={'menu-trigger'}
                        onClick={() => setSidebar(!sidebar)}>
                        <FontAwesomeIcon icon={faBars} color="white" />
                    </button>
                    <LinkContainer to="/">
                        <Navbar.Brand>{t('Header.NavbarTitle')}</Navbar.Brand>
                    </LinkContainer>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <FindProductsForm />
                    <Navbar.Collapse className="justify-content-end" style={{whiteSpace: "nowrap"}}>
                        {loggedIn ?
                            <>
                                <Button
                                    onClick={() => history.push("/shopping/products/sellProduct")}>
                                    {t('Header.AddProduct')}
                                </Button>
                                <Button variant="link mr-0" className="shopping-cart-button"
                                    onClick={() => history.push("/shopping/shoppingCart")}>
                                    <FontAwesomeIcon icon={faShoppingCart} size="lg" />
                                </Button>
                            </>
                            : ''}
                        <DropdownButton
                            as={ButtonGroup}
                            id={`dropdown-variants-dark`}
                            variant="dark"
                            menuAlign="right"
                            title={
                                loggedIn ?
                                    <span>
                                        <FontAwesomeIcon icon={faUser} size="sm" /> {t('Header.NavDropdown')}
                                    </span>
                                    :
                                    <span>
                                        {t('Header.Welcome')}
                                    </span>
                            }
                        >
                            {loggedIn ?
                                <div>
                                    <LinkContainer to="/users/profile">
                                        <Dropdown.Item>
                                            {t('Header.Profile')}
                                        </Dropdown.Item>
                                    </LinkContainer>
                                    <LinkContainer to="/users/logOut">
                                        <Dropdown.Item>
                                            {t('Header.LogOut')}
                                        </Dropdown.Item>
                                    </LinkContainer>
                                </div>
                                :
                                <div>
                                    <Dropdown.Item onClick={() => setShow(true)}>
                                        {t('Header.LogIn')}
                                    </Dropdown.Item>
                                    <LinkContainer to="/users/sign">
                                        <Dropdown.Item>
                                            {t('Header.Register')}
                                        </Dropdown.Item>
                                    </LinkContainer>
                                </div>
                            }

                        </DropdownButton>
                    </Navbar.Collapse>
                </Navbar>
                <LogInModal show={show} onClose={() => setShow(false)} />
                <Sidebar show={sidebar} onClose={() => setSidebar(false)} />
            </div>
        </div>
    );
};

export default Header;