import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Nav from 'react-bootstrap/Nav';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faCoins } from "@fortawesome/free-solid-svg-icons";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { faBoxes } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Transition } from 'react-transition-group';
import { useTranslation } from "react-i18next/";
import users from "../../users";
import { Badge } from "@material-ui/core";
import { Errors } from "../../common";

const Sidebar = ({ show, onClose }) => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const userId = useSelector(users.selectors.getUserId);
    const loggedIn = useSelector(users.selectors.isLoggedIn);
    const messageReceived = useSelector(users.selectors.getMessage);
    const notification = useSelector(users.selectors.isNotification);
    const [backendErrors, setBackendErrors] = useState(null);
    const duration = 600;
    const defaultStyle = {
        transition: `width ${duration}ms`
    }

    const transitionStyles = {
        entering: { width: 0 },
        entered: { width: '250px' },
        exiting: { width: '250px' },
        exited: { width: 0 },
    };

    useEffect(() => {
        if (loggedIn) {
            dispatch(users.actions.checkNewMessagesGlobal(
                () => '',
                errors => setBackendErrors(errors)
            ))
        }
    } , [dispatch, loggedIn])

    useEffect(() => {
            if (loggedIn && messageReceived && messageReceived.senderId !== userId) {
                dispatch(users.actions.checkNewMessagesGlobal(
                    () => '',
                    errors => setBackendErrors(errors)
                ))
            
        }
    }, [dispatch, messageReceived, userId, loggedIn])

    return (
        <div className="sidebar">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Transition in={show} timeout={0}>
                {state => (
                    <div className="sidebar-content"
                        style={{ ...defaultStyle, ...transitionStyles[state] }}>
                        <button className={'menu-close'}
                            onClick={() => onClose(false)}>
                            <FontAwesomeIcon icon={faTimes} size="2x" color="white" />
                        </button>
                        <Nav defaultActiveKey="/" className="flex-column sidebar-links">
                            <Nav.Item className="sidebar-link">
                                <Link to="/" onClick={() => onClose(false)}>
                                    <FontAwesomeIcon icon={faHome} /> {t('Header.NavbarTitle')}
                                </Link>
                            </Nav.Item>
                            {loggedIn ?
                                <div>
                                    <Nav.Item className="sidebar-link">
                                        <Link to="/shopping/products/myProducts" onClick={() => onClose(false)}>
                                            <FontAwesomeIcon icon={faBoxes} /> {t('Header.MyProducts')}
                                        </Link>
                                    </Nav.Item>
                                    <Nav.Item className="sidebar-link">
                                        <Link to="/shopping/orders" onClick={() => onClose(false)}>
                                            <FontAwesomeIcon icon={faBook} /> {t('Header.Orders')}
                                        </Link>
                                    </Nav.Item>
                                    <Nav.Item className="sidebar-link">
                                        <Link to="/shopping/sells" onClick={() => onClose(false)}>
                                            <FontAwesomeIcon icon={faCoins} /> {t('Header.Sells')}
                                        </Link>
                                    </Nav.Item>
                                    <Nav.Item className="sidebar-link">
                                        <Link to="/users/chats" onClick={() => onClose(false)}>
                                            <FontAwesomeIcon icon={faQuestion} /> {t('Header.Chats')}
                                            <Badge className="ml-3" badgeContent="" color="secondary" variant="dot" invisible={!notification} />
                                        </Link>
                                    </Nav.Item>
                                    <Nav.Item className="sidebar-link">
                                        <Link to="/shopping/ratings" onClick={() => onClose(false)}>
                                            <FontAwesomeIcon icon={faStar} /> {t('Header.Ratings')}
                                        </Link>
                                    </Nav.Item>
                                </div>
                                : ''}
                        </Nav>

                    </div>
                )}
            </Transition>
        </div>
    );
}

export default Sidebar;