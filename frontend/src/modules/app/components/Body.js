import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from 'react-router-dom';
import { Register, LogOut, LogIn, UserProfile, RetripeConnect, UserPublicPage, Chats, ChatDetails, ChatDetailsProductPage } from '../../users';
import { SellProduct, ShoppingCart, UpdateUserProduct, BuySuccess, GetOrders, OrderDetails, GetSells, SellDetails, GetUserRatings, GetRatings } from '../../shopping';
import { GetProducts, GetUserProducts, ProductDetails, StartPage } from '../../catalog';
import users from '../../users';
import * as selectors from '../selectors';
import NetworkError from "./NetworkError";
import NotFound from "./NotFound";
import * as Stomp from 'stompjs';
import SockJS from 'sockjs-client';

const websocket = "http://localhost:8080/websocket";

const Body = () => {
    const loggedIn = useSelector(users.selectors.isLoggedIn);
    const error = useSelector(selectors.getError);
    const dispatch = useDispatch();
    const [stompClient, setStompClient] = useState(null);
    const userId = useSelector(users.selectors.getUserId);

    useEffect(() => {
        dispatch(users.actions.getChats());
    }, [dispatch])

    useEffect(() => {
        if (loggedIn) {
            let socket = new SockJS(websocket);
            setStompClient(Stomp.over(socket));
        }
    }, [loggedIn])

    useEffect(() => {

        if (loggedIn && stompClient && !stompClient.connected) {    
            stompClient.connect({ Authorization: 'Bearer ' + localStorage.getItem("serviceToken") }, function (frame) {
                dispatch(users.actions.storeConnection(stompClient));
                stompClient.subscribe("/user/" + userId + "/reply", (message) => {
                    let messageParsed = JSON.parse(message.body);
                    dispatch(users.actions.getMessageReceived(messageParsed));
                })
            })
        }

    }, [dispatch, loggedIn, stompClient, userId])

    return (
        <>
            <Switch>
                {error && <Route component={NetworkError} />}
                <Route exact path="/users/sign"><Register /></Route>
                <Route exact path="/users/logIn"><LogIn /></Route>
                {loggedIn && <Route exact path="/users/logOut"><LogOut /></Route>}
                {loggedIn && <Route exact path="/users/profile"><UserProfile /></Route>}
                {loggedIn && <Route exact path="/shopping/products/sellProduct"><SellProduct /></Route>}
                {loggedIn && <Route exact path="/shopping/products/myProducts"><GetUserProducts /></Route>}
                <Route exact path="/products/:productId"><ProductDetails /></Route>
                {loggedIn && <Route exact path="/products/:productId/update"><UpdateUserProduct /></Route>}
                <Route exact path="/products"><GetProducts /></Route>
                <Route exact path="/"><StartPage /></Route>
                {loggedIn && <Route exact path="/shopping/shoppingCart"><ShoppingCart /></Route>}
                {loggedIn && <Route exact path="/shopping/order"><BuySuccess /></Route>}
                {loggedIn && <Route exact path="/users/profile/connectStripe"><RetripeConnect /></Route>}
                {loggedIn && <Route exact path="/shopping/orders"><GetOrders buyer={true} /></Route>}
                {loggedIn && <Route exact path="/shopping/sells"><GetSells /></Route>}
                {loggedIn && <Route exact path="/shopping/orders/:orderId"><OrderDetails buyer={true} /></Route>}
                {loggedIn && <Route exact path="/shopping/sells/:orderId"><SellDetails /></Route>}
                {loggedIn && <Route exact path="/shopping/ratings"><GetUserRatings /></Route>}
                <Route exact path="/shopping/ratings/:username"><GetRatings /></Route>
                <Route exact path="/users/seller/:username"><UserPublicPage /></Route>
                {loggedIn && <Route exact path="/users/chats"><Chats /></Route>}
                {loggedIn && <Route exact path="/users/chats/:chatId"><ChatDetails /></Route>}
                {loggedIn && <Route exact path="/users/chats/:sellerId/:productId"><ChatDetailsProductPage /></Route>}
                {!error && <Route component={NotFound} />}
            </Switch>
        </>
    );
}
export default Body;