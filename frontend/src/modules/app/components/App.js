import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Router } from 'react-router-dom';
import Header from './Header';
import Body from './Body';
import Footer from './Footer'
import users from '../../users/'
import { createBrowserHistory } from 'history';
import Sidebar from './Sidebar';
import * as selectors from '../selectors';

const App = () => {
    const dispatch = useDispatch();
    const history = createBrowserHistory();
    const error = useSelector(selectors.getError);

    useEffect(() => {
        dispatch(users.actions.logInFromServiceToken(
            () => {
                dispatch(users.actions.logOut());
                history.push("/users/logIn");
            }
        ));
    }, [dispatch, history]);

    return (
        <div className="body-app">
            <Router forceRefresh={false} history={history}>
                {!error ?
                    <>
                        <Header />
                        <Sidebar />
                    </>
                    : ''}
                <div className="body-components-footer">
                    <div className="body-components">
                        <Body />
                    </div>
                    {!error ?
                        <Footer />
                        : ''}
                </div>
            </Router>
        </div>
    );
};

export default App;