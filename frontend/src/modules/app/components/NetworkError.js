import { useTranslation } from "react-i18next";

const NetworkError = () => {
    const [t] = useTranslation();

    return (
        <div className="min-vh-100 row align-items-center">
            <div className="text-center">
                <span className="display-1 d-block">Oops!</span>
                <br />
                <div className="mb-4 lead">{t('Error.NetworkError')}</div>
            </div>
        </div>
    )
}

export default NetworkError;