import Button from "react-bootstrap/esm/Button";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";

const NotFound = () => {
    const [t] = useTranslation();
    const history = useHistory();
    
    return (
        <div className="min-vh-100 row align-items-center">
            <div className="text-center">
                <span className="display-1 d-block">Oops!</span>
                <br />
                <div className="mb-4 lead">{t('Error.NotFound')}</div>
                <Button onClick={() => history.push('/')}>{t('Users.ReturnButton')}</Button>
            </div>
        </div>
    )
}

export default NotFound;