import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as actions from '../../catalog/actions';
import * as selectors from '../../catalog/selectors';
import Container from "react-bootstrap/Container";
import { Link, useHistory, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import Tabs from "react-bootstrap/esm/Tabs";
import Tab from "react-bootstrap/esm/Tab";
import Carousel from "react-bootstrap/Carousel";
import ImageGallery from 'react-image-gallery';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { LinkContainer } from 'react-router-bootstrap';
import users, { LogInModal } from '../../users';
import { BackButton, Errors } from "../../common";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AddToShoppingCart, GetRatings } from "../../shopping/index";

const ProductDetails = () => {
    const [t] = useTranslation();
    const loggedIn = useSelector(users.selectors.isLoggedIn);
    const history = useHistory();
    const { productId } = useParams();
    const product = useSelector(selectors.getProductInf);
    const dispatch = useDispatch();
    const [key, setKey] = useState(t('Products.DescriptionHeader'));
    const [show, setShow] = useState(false);
    const [imageId, setImageId] = useState(null);
    const [backendErrors, setBackendErrors] = useState(null);
    const userId = useSelector(users.selectors.getUserId);
    const [showLogin, setShowLogin] = useState(false);

    useEffect(() => {
        if (!Number.isNaN(productId) && backendErrors === null) {
            dispatch(actions.getProduct(productId,
                errors => setBackendErrors(errors)));
        }
    }, [productId, dispatch, backendErrors]);

    const images = [];
    let i;
    if (product) {
        for (i = 0; i < product.images.length; i++) {
            images.push({
                original: `http://localhost:8080/products/images/${product.images[i].id}`,
                thumbnail: `http://localhost:8080/products/images/${product.images[i].id}`
            })
        }
    }

    const modalHandler = (imageId) => {
        setShow(true);
        setImageId(imageId);
    }

    const goToChat = () => {
        dispatch(users.actions.cleanChat());
        loggedIn ? history.push(`/users/chats/${product.sellerId}/${product.id}`) : setShowLogin(true);
    }

    return (
        <div>
            <Container className={"rounded"}>
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                {product ?
                    <>
                        <Row>
                            <Col>
                                <BackButton />
                            </Col>
                            {product.sellerId === userId ?
                                <Col>
                                    <LinkContainer to={`${productId}/update`}>
                                        <Button variant="outline-dark" className="edit-button">
                                            <FontAwesomeIcon icon={faEdit} size="lg" />
                                        </Button>
                                    </LinkContainer>
                                </Col>
                                : ''}
                        </Row>
                        <Row>
                            <Col lg xs={6}>
                                <Carousel>
                                    {product.images.map(image =>
                                        <Carousel.Item key={image.id} interval={99999999999}>
                                            <div className="product_detail">
                                                <img onClick={() => modalHandler(product.images.indexOf(image))} src={`http://localhost:8080/products/images/${image.id}`} alt="" />
                                            </div>
                                        </Carousel.Item>
                                    )}
                                </Carousel>
                                <Modal centered show={show} onHide={() => setShow(false)} size="lg" contentClassName="images_modal">
                                    <Modal.Body>
                                        <ImageGallery items={images} showThumbnails showPlayButton={false} showFullscreenButton={false} showIndex startIndex={imageId}
                                        />
                                    </Modal.Body>
                                </Modal>
                            </Col>
                            <Col lg xs={6}>
                                <p className="mb-2 text-muted text-uppercase small">
                                    {t('Header.Products')}/{t(`Categories.${product.category.replace(/\s/g, '')}`)}
                                </p>
                                <h3>{product.name}</h3>
                                <p><span className="mr-1"><strong>{product.price}€</strong></span></p>
                                <Row>
                                    <Col xs={3} lg={2}>
                                        {t('Field.Condition')}:
                                    </Col>
                                    <Col md={5}>
                                        {t(`Conditions.${product.condition.replace(/\s/g, '')}`)}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={3} lg={2}>
                                        {t('Field.Quantity')}:
                                    </Col>
                                    <Col md={5}>
                                        {product.quantity} {t('Products.Available')}
                                    </Col>
                                </Row>
                                <br />
                                {product.sellerId !== userId ?
                                    <div>
                                        <AddToShoppingCart productId={productId} stock={product.quantity} />
                                        <br />
                                        <Button variant="link p-0" onClick={() => goToChat()}>{t('Products.QuestionLink')}</Button>
                                    </div>
                                    :
                                    ''
                                }
                            </Col>
                        </Row>
                        <br />
                        <Tabs
                            id="address-tab"
                            activeKey={key}
                            onSelect={(k) => setKey(k)}
                        >
                            <Tab eventKey={t('Products.DescriptionHeader')} key={t('Products.DescriptionHeader')} title={t('Products.DescriptionHeader')}>
                                <br />
                                <p>{product.description}</p>
                            </Tab>
                            <Tab eventKey={t('Products.Seller')} key={t('Products.Seller')} title={t('Products.Seller')}>
                                <br />
                                <span className="font-weight-bold">{t('Field.Seller')}: </span>
                                <Link to={`/users/seller/${product.sellerUsername}`}>{product.sellerUsername}</Link>
                                <GetRatings username={product.sellerUsername} />
                            </Tab>
                        </Tabs>
                    </>
                    : ''}
                    <LogInModal show={showLogin} onClose={() => setShowLogin(false)}/>
            </Container>
        </div >
    )
}

export default ProductDetails;