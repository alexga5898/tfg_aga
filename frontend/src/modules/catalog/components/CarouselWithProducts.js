import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import * as actions from '../actions';
import * as selectors from '../selectors';
import Spinner from 'react-bootstrap/Spinner';
import Carousel from 'react-multi-carousel';
import CarouselProduct from "./CarouselProduct";
import { Link } from "react-router-dom";
import configData from "../../../i18n/locales/en/translation.json";
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { Errors } from "../../common";

const CarouselWithProducts = ({ randomCategory }) => {
    const [t] = useTranslation();
    const products = useSelector(selectors.getProductsList);
    const productsByCategory = useSelector(selectors.getProductsByCategoryList);
    const dispatch = useDispatch();
    const [backendErrors, setBackendErrors] = useState(null);
    const [filterCategory, setFilterCategory] = useState('');
    const [loading, setLoading] = useState(true);

    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
            slidesToSlide: 1
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 5,
            slidesToSlide: 1
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 3,
            slidesToSlide: 1
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 3,
            slidesToSlide: 1
        }
    };

    useEffect(() => {
        if (randomCategory) {
            const categoriesArray = Object.keys(configData.Categories);
            const random = Math.floor(Math.random() * categoriesArray.length);
            dispatch(actions.getProductsByCategory(
                0,
                "",
                categoriesArray[random],
                "",
                "",
                "",
                0,
                () => setLoading(false),
                error => {
                    setBackendErrors(error);
                    setLoading(false);
                }));
            setFilterCategory(categoriesArray[random]);
        } else {
            dispatch(actions.getProducts(
                0,
                "",
                "",
                "",
                "",
                "",
                0,
                () => setLoading(false),
                error => {
                    setBackendErrors(error);
                    setLoading(false);
                }));
        }
    }, [randomCategory, dispatch]);

    let carouselItems = [];
    if (!randomCategory) {
        for (let i = 0; i < products.length; i++) {
            carouselItems.push(<CarouselProduct key={products[i].id} product={products[i]} />)
        }
    } else {
        for (let i = 0; i < productsByCategory.length; i++) {
            carouselItems.push(<CarouselProduct key={productsByCategory[i].id} product={productsByCategory[i]} />)
        }
    }

    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            {!randomCategory ?
                <h3>{t('Products.RecentlyAdded')}</h3> : <h3>{t(`Categories.${filterCategory}`)}</h3>}
            <br />
            {!loading ?
                <Carousel responsive={responsive}>
                    {carouselItems}
                </Carousel>
                :
                <div className="d-flex justify-content-center mt-3 mb-3">
                    <Spinner animation="border" />
                </div>
            }
            <br />
            <Link to={`/products?page=0&category=${filterCategory}`}>Ver más</Link>
        </div>
    )
}

CarouselWithProducts.propTypes = {
    randomCategory: PropTypes.bool
};

export default CarouselWithProducts;