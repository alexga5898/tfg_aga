import React from "react";
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import Card from "react-bootstrap/Card";

const Products = ({ products }) => {
    const history = useHistory();

    if (products && products.length === 0) {
        return null;
    }

    const clickHandler = (id) => {
        history.push(`/products/${id}`);
    }

    return (
        <div>
            {products && products.map(product =>
                <Card className="product-card" key={product.id} onClick={() => clickHandler(product.id)}>
                    <Card.Img variant="top" className="product-card-img" src={`http://localhost:8080/products/${product.id}/thumbnail`} />
                    <Card.Body>
                        <Card.Title className="font-weight-bold">
                            {product.name}
                            <p className="mb-0">{product.price}€</p>
                        </Card.Title>
                        <Card.Text>
                            {product.description}
                        </Card.Text>
                    </Card.Body>
                </Card>
            )}
        </div>
    )
}

Products.propTypes = {
    products: PropTypes.array
};

export default Products;