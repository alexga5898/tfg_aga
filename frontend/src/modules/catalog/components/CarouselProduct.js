import React from "react";
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import Card from "react-bootstrap/Card";

const CarouselProduct = ({ product }) => {
    const history = useHistory();
    
    const clickHandler = (id) => {
        history.push(`/products/${id}`);
    }

    return (
        <div>
            <Card className="carousel-product" key={product.id} onClick={() => clickHandler(product.id)}>
                <Card.Img variant="top" className="carousel-product-card-img" src={`http://localhost:8080/products/${product.id}/thumbnail`} />
                <Card.Body>
                    <Card.Title className="carousel-product-title">
                        {product.name}
                        <p>{product.price}€</p>
                    </Card.Title>
                </Card.Body>
            </Card>
        </div>
    )
}

CarouselProduct.propTypes = {
    product: PropTypes.object
}

export default CarouselProduct;