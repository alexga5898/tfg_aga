import React, { useState } from "react";
import { useTranslation } from "react-i18next/";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import queryString from 'query-string';
import { useHistory, useLocation } from "react-router";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/esm/Button";
import InputGroup from "react-bootstrap/InputGroup";
import { Errors } from "../../common";
import { useDispatch } from "react-redux";
import * as actions from "../actions"

const FindProductsForm = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const [keywords, setKeywords] = useState(queryString.parse(location.search).keywords ? queryString.parse(location.search).keywords : '');
    const category = queryString.parse(location.search).category ? queryString.parse(location.search).category : '';
    const condition = queryString.parse(location.search).condition ? queryString.parse(location.search).condition : '';
    const lPrice = queryString.parse(location.search).lPrice ? queryString.parse(location.search).lPrice : '';
    const hPrice = queryString.parse(location.search).hPrice ? queryString.parse(location.search).hPrice : '';
    const order = queryString.parse(location.search).order ? queryString.parse(location.search).order : '';
    const [backendErrors, setBackendErrors] = useState(null);
    const dispatch = useDispatch();

    const submitHandler = (event) => {
        event.preventDefault();
        dispatch(actions.loading_products());
        history.push(`/products?page=0&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${lPrice}&hPrice=${hPrice}&order=${order}`)
    }

    return (
        <>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Form inline noValidate onSubmit={e => submitHandler(e)} className="ml-4 mr-5 d-inline w-100">
                <InputGroup >
                    <Form.Control
                        type="text"
                        onChange={e => setKeywords(e.target.value)}
                        placeholder={t('Search.MyProductsFeedback')}
                        defaultValue={keywords}
                        style={{ display: "inline" }}
                    />
                    <InputGroup.Append>
                        <Button type="submit"><FontAwesomeIcon icon={faSearch} size="sm" /></Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
        </>
    )
}

export default FindProductsForm;