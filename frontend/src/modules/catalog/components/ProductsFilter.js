import React, { useState } from "react";
import { useDispatch } from "react-redux"
import * as actions from '../../catalog/actions';
import { Errors } from "../../common";
import { useTranslation } from "react-i18next/";
import queryString from 'query-string';
import { useHistory, useLocation } from "react-router";
import Form from "react-bootstrap/Form";
import configData from "../../../i18n/locales/en/translation.json"

const ProductsFilter = ({ onFilter }) => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const keywords = queryString.parse(location.search).keywords ? queryString.parse(location.search).keywords : '';
    const [category, setCategory] = useState(queryString.parse(location.search).category ? queryString.parse(location.search).category : '');
    const [condition, setCondition] = useState(queryString.parse(location.search).condition ? queryString.parse(location.search).condition : '');
    const [lPrice, setLPrice] = useState(queryString.parse(location.search).lPrice ? queryString.parse(location.search).lPrice : '');
    const [hPrice, setHPrice] = useState(queryString.parse(location.search).hPrice ? queryString.parse(location.search).hPrice : '');
    const [order, setOrder] = useState(queryString.parse(location.search).order ? queryString.parse(location.search).order : "0");
    const dispatch = useDispatch();
    const [backendErrors, setBackendErrors] = useState(null);

    const filterCategory = (value) => {
        history.push(`products?page=0&keywords=${keywords}&category=${value}&condition=${condition}&lPrice=${lPrice}&hPrice=${hPrice}&order=${order}`)
        dispatch(actions.getProducts(
            page,
            keywords,
            value,
            condition,
            lPrice,
            hPrice,
            order,
            () => {
                setBackendErrors(null);
                onFilter();
            },
            error => {
                setBackendErrors(error);
                onFilter();
            }));
        setCategory(value);
    }

    const filterCondition = (value) => {
        history.push(`products?page=0&keywords=${keywords}&category=${category}&condition=${value}&lPrice=${lPrice}&hPrice=${hPrice}&order=${order}`)
        dispatch(actions.getProducts(
            page,
            keywords,
            category,
            value,
            lPrice,
            hPrice,
            order,
            () => {
                setBackendErrors(null);
                onFilter();
            },
            error => {
                setBackendErrors(error);
                onFilter();
            }));
        setCondition(value);
    }

    const filterPrices = (min, max) => {
        history.push(`products?page=0&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${min}&hPrice=${max}&order=${order}`)
        dispatch(actions.getProducts(
            page,
            keywords,
            category,
            condition,
            min,
            max,
            order,
            () => {
                setBackendErrors(null);
                onFilter();
            },
            error => {
                setBackendErrors(error);
                onFilter();
            }));
        setLPrice(min);
        setHPrice(max);
    }

    const filterOrder = (value) => {
        history.push(`products?page=0&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${lPrice}&hPrice=${hPrice}&order=${value}`)
        dispatch(actions.getProducts(
            page,
            keywords,
            category,
            condition,
            lPrice,
            hPrice,
            value,
            () => {
                setBackendErrors(null);
                onFilter();
            },
            error => {
                setBackendErrors(error);
                onFilter();
            }));
        setOrder(value);
    }

    let categories = [];
    let conditions = [];

    for (let categoryValue in configData.Categories) {
        categories.push(
            <Form.Check key={categoryValue} type="radio" label={t(`Categories.${categoryValue}`)} name="category"
                value={configData.Categories[categoryValue]} onChange={e => filterCategory(e.target.value)}
                defaultChecked={configData.Categories[categoryValue] === category} />
        );
    }

    for (let conditionValue in configData.Conditions) {
        conditions.push(
            <Form.Check key={conditionValue} type="radio" label={t(`Conditions.${conditionValue}`)} name="condition"
                value={configData.Conditions[conditionValue]} onChange={e => filterCondition(e.target.value)}
                defaultChecked={configData.Conditions[conditionValue] === condition} />
        );
    }


    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Form>
                <h5>{t('Field.Order')}</h5>
                <Form.Check type="radio" label={t('Field.RecentOlderDate')} name="order"
                    value="0" onChange={e => filterOrder(e.target.value)}
                    defaultChecked={"0" === order} />
                <Form.Check type="radio" label={t('Field.OlderRecentDate')} name="order"
                    value="1" onChange={e => filterOrder(e.target.value)}
                    defaultChecked={"1" === order} />
                <Form.Check type="radio" label={t('Field.LowerHigherPrice')} name="order"
                    value="2" onChange={e => filterOrder(e.target.value)}
                    defaultChecked={"2" === order} />
                <Form.Check type="radio" label={t('Field.HigherLowerPrice')} name="order"
                    value="3" onChange={e => filterOrder(e.target.value)}
                    defaultChecked={"3" === order} />
                <br />
                <h5>{t('Field.Category')}</h5>
                <Form.Check type="radio" label={t('Field.All')} name="category"
                    value="" onChange={e => filterCategory(e.target.value)}
                    defaultChecked={"" === category} />
                {categories}
                <br />
                <h5>{t('Field.Condition')}</h5>
                <Form.Check type="radio" label={t('Field.All')} name="condition"
                    value={""} onChange={e => filterCondition(e.target.value)}
                    defaultChecked={"" === condition} />
                {conditions}
                <br />
                <h5>{t('Field.Price')}</h5>
                <Form.Check type="radio" label={t('Field.All')} name="price"
                    onChange={() => filterPrices("", "")}
                    defaultChecked={lPrice === "" && hPrice === ""} />
                <Form.Check type="radio" label="0 - 20 EUR" name="price"
                    onChange={() => filterPrices(0, 20)}
                    defaultChecked={lPrice === "0" && hPrice === "20"} />
                <Form.Check type="radio" label="20 - 50 EUR" name="price"
                    onChange={() => filterPrices(20, 50)}
                    defaultChecked={lPrice === "20" && hPrice === "50"} />
                <Form.Check type="radio" label="50 - 100 EUR" name="price"
                    onChange={() => filterPrices(50, 100)}
                    defaultChecked={lPrice === "50" && hPrice === "100"} />
                <Form.Check type="radio" label="100 - 200 EUR" name="price"
                    onChange={() => filterPrices(100, 200)}
                    defaultChecked={lPrice === "100" && hPrice === "200"} />
                <Form.Check type="radio" label="200 - 500 EUR" name="price"
                    onChange={() => filterPrices(200, 500)}
                    defaultChecked={lPrice === "200" && hPrice === "500"} />
                <Form.Check type="radio" label="+500 EUR" name="price"
                    onChange={() => filterPrices(500, "")}
                    defaultChecked={lPrice === "500" & hPrice === ''} />
            </Form>
        </div>
    )
}

export default ProductsFilter;