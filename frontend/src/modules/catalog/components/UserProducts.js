import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import Table from "react-bootstrap/esm/Table";

const UserProducts = ({ products }) => {
    const [t] = useTranslation();
    const history = useHistory();

    if (products.length === 0) {
        return null;
    }

    return (
        <div>
            <Table responsive hover>
                <thead className="thead-dark">
                    <tr>
                        <th></th>
                        <th className="text-center">{t('Products.ProductHeader')}</th>
                        <th className="text-center">{t('Products.QuantityHeader')}</th>
                        <th className="text-center">{t('Products.DateHeader')}</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(product =>
                        <tr key={product.id} onClick={() => history.push(`/products/${product.id}`)}>
                            <td align="center"> <img className="product_mini" src={`http://localhost:8080/products/${product.id}/thumbnail`} alt="" /></td>
                            <td align="center">{product.name}</td>
                            <td align="center">{product.quantity}</td>
                            <td align="center">{t('Date.Format', { date: new Date(product.date) })}</td>
                        </tr>)}
                </tbody>
            </Table>
        </div>
    )
}

UserProducts.propTypes = {
    products: PropTypes.array
};

export default UserProducts;