import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import * as actions from '../../catalog/actions';
import * as selectors from '../../catalog/selectors';
import UserProducts from "./UserProducts";
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import { Errors, Paginate } from "../../common";
import { useTranslation } from "react-i18next/";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { useHistory, useLocation } from "react-router";
import queryString from 'query-string';
import { Link } from "react-router-dom";
import Spinner from "react-bootstrap/esm/Spinner";

const GetUserProducts = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const products = useSelector(selectors.getUserProductsList);
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const [search, setSearch] = useState(queryString.parse(location.search).keywords ? queryString.parse(location.search).keywords : '');
    const [keywords, setKeywords] = useState(queryString.parse(location.search).keywords ? queryString.parse(location.search).keywords : '');
    const hasNext = useSelector(selectors.getHasNextUserProducts);
    const totalPages = useSelector(selectors.getTotalPagesNumberUserProducts);
    const [backendErrors, setBackendErrors] = useState(null);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        dispatch(actions.getUserProducts(page, keywords, () => setLoading(false)));
    }, [dispatch, keywords, page])

    const submitHandler = (event) => {
        event.preventDefault();
        setKeywords(search);
        history.push(`?page=0&keywords=${search}`);
    }

    const nextPage = () => {
        history.push(`?page=${page + 1}&keywords=${keywords}`);
    }

    const previousPage = () => {
        history.push(`?page=${page - 1}&keywords=${keywords}`);
    }

    const goPage = (page) => {
        history.push(`?page=${page}&keywords=${keywords}`);
    }

    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Container className={"rounded"}>
                <Form inline noValidate onSubmit={e => submitHandler(e)} className="products-search-form">
                    <InputGroup style={{ width: "100%" }}>
                        <Form.Control
                            type="text"
                            onChange={e => setSearch(e.target.value)}
                            placeholder={t('Search.MyProductsFeedback')}
                            defaultValue={keywords}
                            style={{ display: "inline" }}
                        />
                        <InputGroup.Append>
                            <Button type="submit"><FontAwesomeIcon icon={faSearch} size="sm" /></Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form>
                <br />
                {!loading ?
                    <>
                        {products && products.length > 0 ?
                            <>
                                <UserProducts products={products} />
                                <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()}
                                    next={() => nextPage()} go={(page) => goPage(page)} />
                            </>
                            : products && products.length === 0 ?
                                <div className="text-center">
                                    <br />
                                    <span className="text-muted">{t('Empty.MyProducts')} <Link to="/shopping/products/sellProduct">{t('Field.Selling')}.</Link> </span>
                                </div>
                                : ''}
                    </>
                    : <div className="d-flex justify-content-center mt-4 mb-3">
                        <Spinner animation="border" />
                    </div>
                }
            </Container>
        </div>
    )
}

export default GetUserProducts;