import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import * as actions from '../../catalog/actions';
import * as selectors from '../../catalog/selectors';
import Container from "react-bootstrap/Container";
import { Errors, Paginate } from "../../common";
import Col from "react-bootstrap/Col";
import Products from "./Products";
import { useHistory, useLocation } from "react-router";
import queryString from 'query-string';
import ProductsFilter from "./ProductsFilter";
import Spinner from "react-bootstrap/esm/Spinner";

const GetProducts = () => {
    const history = useHistory();
    const products = useSelector(selectors.getProductsList);
    const location = useLocation();
    const hasNext = useSelector(selectors.getHasNextProducts);
    const totalPages = useSelector(selectors.getTotalPagesNumberProducts);
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const keywords = queryString.parse(location.search).keywords ? queryString.parse(location.search).keywords : '';
    const category = queryString.parse(location.search).category ? queryString.parse(location.search).category : '';
    const condition = queryString.parse(location.search).condition ? queryString.parse(location.search).condition : '';
    const lPrice = queryString.parse(location.search).lPrice ? queryString.parse(location.search).lPrice : '';
    const hPrice = queryString.parse(location.search).hPrice ? queryString.parse(location.search).hPrice : '';
    const order = queryString.parse(location.search).order ? queryString.parse(location.search).order : 0;
    const dispatch = useDispatch();
    const [backendErrors, setBackendErrors] = useState(null);
    const [loading, setLoading] = useState(true);
    const state = useSelector(selectors.getProductsLoading);

    useEffect(() => {
        if (loading || state) {
            dispatch(actions.getProducts(
                page,
                keywords,
                category,
                condition,
                lPrice,
                hPrice,
                order,
                () => {
                    setBackendErrors(null);
                    setLoading(false);
                },
                error => {
                    setBackendErrors(error);
                    setLoading(false);
                }));
        }
    }, [dispatch, page, keywords, category, condition, lPrice, hPrice, order, loading, state]);

    const nextPage = () => {
        history.push(`?page=${page + 1}&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${lPrice}`
            + `&hPrice=${hPrice}&order=${order}`);
    }

    const previousPage = () => {
        history.push(`?page=${page - 1}&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${lPrice}`
            + `&hPrice=${hPrice}&order=${order}`);
    }

    const goPage = (page) => {
        history.push(`?page=${page}&keywords=${keywords}&category=${category}&condition=${condition}&lPrice=${lPrice}`
            + `&hPrice=${hPrice}&order=${order}`);
    }

    return (
        <Container className="products-container">
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Container className="products-fluid-container" fluid>
                <Col xs={3} lg={2}>
                    <div className="products-filter">
                        <ProductsFilter onFilter={() => setLoading(true)} />
                        <div className="products-divider" />
                    </div>
                </Col>
                <Col xs lg>
                    {loading || state ?
                        <div className="d-flex justify-content-center align-center">
                            <Spinner animation="border" />
                        </div>
                        :
                        <>
                            {products && products.length > 0 ?
                                <>
                                    <Products products={products} />
                                </>
                                : ''}
                        </>
                    }
                </Col>
            </Container>
            <div>
                <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()}
                    next={() => nextPage()} go={(page) => goPage(page)} />
            </div>
            <br />

        </Container>
    )
}

export default GetProducts;