import React from "react";
import Container from "react-bootstrap/Container";
import { useHistory } from "react-router";
import Jumbotron from "react-bootstrap/Jumbotron";
import Card from "react-bootstrap/esm/Card";
import Carousel from 'react-multi-carousel';
import { useTranslation } from "react-i18next";
import configData from "../../../i18n/locales/en/translation.json"
import CarouselWithProducts from "./CarouselWithProducts";

const StartPage = () => {
    const [t] = useTranslation();
    const history = useHistory();

    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 10,
            slidesToSlide: 10
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 7,
            slidesToSlide: 6
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 6,
            slidesToSlide: 5
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 4,
            slidesToSlide: 3
        }
    };

    let categories = [];

    for (let categoryValue in configData.Categories) {
        categories.push(
            <Card className="categories-card" key={categoryValue}
                onClick={() => history.push(`products?page=0&category=${configData.Categories[categoryValue]}`)}>
                <Card.Img variant="top" src={`/icons/${categoryValue}.svg`} className="categories-icon" />
                <Card.Body>
                    <Card.Text>
                        {t(`Categories.${categoryValue}`)}
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }

    return (
        <div>
            <Jumbotron id="jumbotron-padding">
                <h1>{t('Home.CategoriesHeader')}</h1>
                <Container id="categories-container">
                    <Carousel responsive={responsive}>
                        {categories}
                    </Carousel>
                </Container>
            </Jumbotron>
            <Container id="recently-added-container" className="rounded">
                <CarouselWithProducts randomCategory={false} />
            </Container>
            <br />
            <Container className="rounded">
                <CarouselWithProducts randomCategory={true} />
            </Container>
        </div>
    )
}

export default StartPage;