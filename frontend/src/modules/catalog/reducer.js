import { combineReducers } from 'redux';
import * as actionTypes from './actionTypes';

const initialState = {
    userProducts: null,
    product: null,
    products: null,
    productsByCategory: null,
    productsLoading: false
}

const userProducts = (state = initialState.products, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_PRODUCTS:
            return action.products;
        default:
            return state;
    }
}

const product = (state = initialState.product, action) => {
    switch (action.type) {
        case actionTypes.GET_PRODUCT:
            return action.product;
        default:
            return state;
    }
}

const products = (state = initialState.products, action) => {
    switch (action.type) {
        case actionTypes.GET_PRODUCTS:
            return action.products;
        case actionTypes.GET_PRODUCTS_BY_USERNAME:
            return action.products;
        default:
            return state;
    }
}

const productsByCategory = (state = initialState.productsByCategory, action) => {
    switch (action.type) {
        case actionTypes.GET_PRODUCTS_BY_CATEGORY:
            return action.products;
        default:
            return state;
    }
}

const productsLoading = (state = initialState.productsLoading, action) => {
    switch (action.type) {
        case actionTypes.LOADING:
            return true;
        case actionTypes.LOADED:
            return false;
        default: 
            return state;
    }
}

const reducer = combineReducers({
    userProducts,
    products,
    productsByCategory,
    product,
    productsLoading
});

export default reducer;