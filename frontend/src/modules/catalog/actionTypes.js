export const GET_USER_PRODUCTS = "get_user_products";
export const GET_PRODUCT = "get_product";
export const GET_PRODUCTS = "get_products";
export const GET_PRODUCTS_BY_CATEGORY = "get_products_by_category";
export const GET_PRODUCTS_BY_USERNAME = "get_products_by_username";
export const LOADING = "loading";
export const LOADED = "loaded";