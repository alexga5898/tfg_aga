import * as actionTypes from './actionTypes';
import backend from '../../backend';

const get_user_products = (products) => (
    {
        type: actionTypes.GET_USER_PRODUCTS,
        products
    }
);

export const getUserProducts = (page, keywords, onSuccess) => dispatch => {
    backend.catalogService.getUserProducts(page, keywords,
        response => {
            dispatch(get_user_products(response));
            onSuccess();
        });
}

const get_product = (product) => (
    {
        type: actionTypes.GET_PRODUCT,
        product
    }
);

export const getProduct = (productId, onErrors) => dispatch => {
    backend.catalogService.getProduct(productId,
        response => {
            dispatch(get_product(response));
        },
        onErrors);

}

const get_products = (products) => (
    {
        type: actionTypes.GET_PRODUCTS,
        products
    }
);

export const loading_products = () => ({
    type: actionTypes.LOADING
})

export const loaded_products = () => ({
    type: actionTypes.LOADED
});

export const getProducts = (page, keywords, category, condition, lPrice, hPrice, order, onSuccess, onErrors) => dispatch => {
    dispatch(loading_products());
    backend.catalogService.getProducts(page, keywords, category, condition, lPrice, hPrice, order,
        response => {
            dispatch(get_products(response));
            dispatch(loaded_products());
            onSuccess();
        },
        onErrors);
}

const get_products_by_category = (products) => (
    {
        type: actionTypes.GET_PRODUCTS_BY_CATEGORY,
        products
    }
)

export const getProductsByCategory = (page, keywords, category, condition, lPrice, hPrice, order, onSuccess, onErrors) => dispatch => {
    backend.catalogService.getProducts(page, keywords, category, condition, lPrice, hPrice, order,
        response => {
            dispatch(get_products_by_category(response));
            onSuccess();
        },
        onErrors);
}

const get_products_by_username = (products) => (
    {
        type: actionTypes.GET_PRODUCTS_BY_USERNAME,
        products
    }
)

export const getProductsByUsername = (page, username, onSuccess) => dispatch => {
    backend.catalogService.getProductsByUsername(page, username,
        response => {
            dispatch(get_products_by_username(response));
            onSuccess();
        })
}
