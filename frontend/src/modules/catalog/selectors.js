export const getState = state =>
    state.catalog

export const getUserProducts = state =>
    getState(state).userProducts

export const getUserProductsList = state =>
    getUserProducts(state) ? getUserProducts(state).items : []

export const getTotalPagesNumberUserProducts = state =>
    getUserProducts(state) ? getUserProducts(state).totalPages : 0

export const getTotalItemsUserProducts = state =>
    getUserProducts(state) ? getUserProducts(state).totalItems : 0

export const getHasNextUserProducts = state =>
    getUserProducts(state) ? getUserProducts(state).existMoreItems : false

const getProduct = state =>
    getState(state).product

export const getProductInf = state =>
    getProduct(state) ? getProduct(state) : null

const getProducts = state =>
    getState(state).products

const getProductsByCategory = state => 
    getState(state).productsByCategory

export const getProductsList = state =>
    getProducts(state) ? getProducts(state).items : []

export const getProductsByCategoryList = state =>
    getProductsByCategory(state) ? getProductsByCategory(state).items : []

export const getTotalPagesNumberProducts = state =>
    getProducts(state) ? getProducts(state).totalPages : 0

export const getTotalItemsProducts = state =>
    getProducts(state) ? getProducts(state).totalItems : 0

export const getHasNextProducts = state =>
    getProducts(state) ? getProducts(state).existMoreItems : false

export const getProductsLoading = state => 
    getState(state).productsLoading