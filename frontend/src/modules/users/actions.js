import * as actionTypes from './actionTypes';
import backend from '../../backend';

const registered = authenticatedUser => (
    {
        type: actionTypes.REGISTERED,
        authenticatedUser
    }
);

export const register = (user, onSuccess, onErrors, callback) => dispatch => {
    backend.userService.register(user,
        response => {
            dispatch(registered(response.data.userDto));
            onSuccess();
        },
        onErrors,
        callback);
}

const loggedIn = authenticatedUser => (
    {
        type: actionTypes.LOGGED_IN,
        authenticatedUser
    }
);

export const logIn = (email, password, onSuccess, onErrors, callback) => dispatch => {
    backend.userService.logIn({ email, password },
        response => {
            dispatch(loggedIn(response.data.userDto));
            onSuccess();
        },
        onErrors,
        callback);
}

export const logInFromServiceToken = (callback) => dispatch => {
    backend.userService.logInFromServiceToken(
        response => {
            if (response) {
                dispatch(loggedIn(response.data.userDto));
            }
        },
        callback);
}

const loggedOut = authenticatedUser => (
    {
        type: actionTypes.LOGGED_OUT,
        authenticatedUser
    }
);

export const logOut = () => dispatch => {
    backend.userService.logOut();
    dispatch(loggedOut(null));
}

const updatedProfile = user => (
    {
        type: actionTypes.UPDATED_PROFILE,
        user
    }
);

export const updateProfile = (user, onSuccess, onErrors) => dispatch => {
    backend.userService.updateProfile(user,
        response => {
            dispatch(updatedProfile(response.data));
            onSuccess();
        },
        onErrors);
}

const notification = isNotification => (
    {
        type: actionTypes.CHECK_NEWMESSAGES,
        isNotification
    }
)

export const checkNewMessages = (chatId, onSuccess, onErrors) => dispatch => {
    backend.userService.checkNewMessages(chatId,
        response => {
            // dispatch(notification(response.data));
            onSuccess(response.data);
        },
        onErrors
    )
}

export const checkNewMessagesGlobal = (onSuccess, onErrors) => dispatch => {
    backend.userService.checkNewMessagesGlobal(
        response => {
            dispatch(notification(response.data));
            onSuccess();
        },
        onErrors
    )
}

export const markAsRead = (chatId, onSuccess, onErrors) => {
    backend.userService.markAsRead(chatId,
        response => {
            onSuccess();
        },
        onErrors)
}

const chats = chats => (
    {
        type: actionTypes.GET_CHATS,
        chats
    }
)

export const getChats = (page, onSuccess) => dispatch => {
    backend.userService.getChats(page,
        response => {
            dispatch(chats(response.data));
            onSuccess();
        }
    )
}

const chatHist = chatMessages => (
    {
        type: actionTypes.GET_CHAT,
        chatMessages
    }
)

export const getChat = (chatId, onSuccess, onErrors) => dispatch => {
    backend.userService.getChat(chatId,
        response => {
            dispatch(chatHist(response.data));
            onSuccess();
        },
        onErrors)
}

export const getChatIfExistent = (sellerId, productId, onSuccess, onErrors) => dispatch => {
    backend.userService.getChatIfExistent(sellerId, productId, 
        response => {
            dispatch(chatHist(response.data));
            onSuccess();
        },
        onErrors)
}

const connectWebsocket = websocket => (
    {
        type: actionTypes.CONNECTED_WEBSOCKET,
        websocket
    }
)

export const storeConnection = (websocket) => dispatch => {
    dispatch(connectWebsocket(websocket));
}

export const cleanConnection = () => dispatch => {
    dispatch({type: actionTypes.CLEAN_CONNECTION})
}

const messageReceived = message => (
    {
        type: actionTypes.MESSAGE_RECEIVED,
        message
    }
)

export const getMessageReceived = message => dispatch => {
    dispatch(messageReceived(message));
}

export const cleanMessageReceived = () => dispatch => {
    dispatch({ type: actionTypes.CLEAN_MESSAGE_RECEIVED });
}

const messages = chatMessages => (
    {
        type: actionTypes.GET_MESSAGES,
        chatMessages
    }
)

export const getMessages = (chatId, onSuccess, onErrors) => dispatch => {
    backend.userService.getMessages(chatId, 0,
        response => {
            dispatch(messages(response.data));
            onSuccess();
        },
        onErrors)
}

const moreMessages = chatMessages => (
    {
        type: actionTypes.GET_MORE_MESSAGES,
        chatMessages
    }
)

export const getMoreMessages = (chatId, page, onSuccess, onErrors) => dispatch => {
    backend.userService.getMessages(chatId, page,
        response => {
            dispatch(moreMessages(response.data));
            onSuccess();
        },
        onErrors)
}

export const cleanChat = () => dispatch => {
    dispatch({ type: actionTypes.CLEAN_CHAT });
}