const getState = state =>
    state.users

const getUser = state =>
    getState(state).user

export const isLoggedIn = state =>
    getUser(state) !== null

export const getUserProfile = state =>
    getUser(state);

export const getUserId = state =>
    getUser(state) ? getUser(state).id : null

export const getUsername = state =>
    getUser(state) ? getUser(state).username : null

export const hasStripeAccId = state =>
    getUser(state) && getUser(state).activeTransfers ? true : false

const getQuestionsReceived = state =>
    getState(state).questionsReceived;

export const getQuestionsReceivedList = state =>
    getQuestionsReceived(state) ? getQuestionsReceived(state).items : []

export const getTotalPagesQuestionsReceived = state =>
    getQuestionsReceived(state) ? getQuestionsReceived(state).totalPages : 0

export const getHasNextQuestionsReceived = state =>
    getQuestionsReceived(state) ? getQuestionsReceived(state).existMoreItems : false

const getQuestionsMade = state =>
    getState(state).questionsMade;

export const getQuestionsMadeList = state =>
    getQuestionsMade(state) ? getQuestionsMade(state).items : []

export const getTotalPagesQuestionsMade = state =>
    getQuestionsMade(state) ? getQuestionsMade(state).totalPages : 0

export const getHasNextQuestionsMade = state =>
    getQuestionsMade(state) ? getQuestionsMade(state).existMoreItems : false

export const isNotification = state =>
    getState(state).notification

const getChats = state =>
    getState(state).chats

export const getChatsList = state => 
    getChats(state) ? getChats(state).items : []

export const getChatsTotalPages = state => 
    getChats(state) ? getChats(state).totalPages : 0

export const getChatsHasNext = state => 
    getChats(state) ? getChats(state).existMoreItems : false

export const getChat = state =>
    getState(state).chat

const getMessages = state =>
    getState(state).messages

export const getMessagesList = state => 
    getMessages(state) ? getMessages(state).items : []

export const getMessagesNext = state =>
    getMessages(state) ? getMessages(state).existMoreItems : false

export const getMessage = state => 
    getState(state).message

export const getWebsocket = state =>
    getState(state).websocket
