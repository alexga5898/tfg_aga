import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router";
import Products from "../../catalog/components/Products";
import { GetRatings } from "../../shopping";
import * as catalogActions from "../../catalog/actions";
import queryString from 'query-string';
import * as catalogSelectors from "../../catalog/selectors";
import * as shoppingSelectors from "../../shopping/selectors";
import Container from "react-bootstrap/esm/Container";
import { useTranslation } from "react-i18next";
import Tabs from "react-bootstrap/esm/Tabs";
import Tab from "react-bootstrap/esm/Tab";
import { Rating } from "@material-ui/lab";
import { Box, makeStyles, Typography } from "@material-ui/core";
import { Paginate } from "../../common";
import Spinner from "react-bootstrap/esm/Spinner";

const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});

const UserPublicPage = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const { username } = useParams();
    const dispatch = useDispatch();
    const location = useLocation();
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const products = useSelector(catalogSelectors.getProductsList);
    const hasNext = useSelector(catalogSelectors.getHasNextProducts);
    const totalPages = useSelector(catalogSelectors.getTotalPagesNumberProducts);
    const avg = useSelector(shoppingSelectors.getRatingsAvg);
    const [key, setKey] = useState(t('Header.Products'));
    const classes = useStyles();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        dispatch(catalogActions.getProductsByUsername(page, username,
            () => setLoading(false)));
    }, [dispatch, page, username]);

    const nextPage = () => {
        history.push(`/users/vendor/${username}?page=${page + 1}`);
    }

    const previousPage = () => {
        history.push(`/users/vendor/${username}?page=${page - 1}`);
    }

    const goPage = (page) => {
        history.push(`/users/vendor/${username}?page=${page}`);
    }

    return (
        <>
            <Container className="rounded">
                <h2>{t('Field.Seller')}: {username}</h2>
                <div className={classes.root}>
                    <Box component="span" mr={1}>
                        <Typography component="span" className="font-weight-bold">{t('Rating.Average')}: </Typography>
                    </Box>
                    <Rating
                        readOnly
                        name="half-rating"
                        precision={0.5}
                        value={avg || 0}
                    />
                </div>
            </Container>
            <br />
            <Container className="rounded">
                <Tabs
                    id="userPublicPage-tab"
                    activeKey={key}
                    onSelect={(k) => setKey(k)}
                >
                    <Tab eventKey={t('Header.Products')} key={t('Header.Products')} title={t('Header.Products')}>
                        <br />
                        {!loading ?
                            <>
                                {products && products.length > 0 ?
                                    <>
                                        <Container className="products-fluid-container px-5 pt-0 fluid" >
                                            <Products products={products} />
                                        </Container>
                                        <Paginate page={page} totalPages={totalPages} hasNext={hasNext} previous={() => previousPage()} next={() => nextPage()} go={(page) => goPage(page)} />
                                    </>
                                    : products && products.length === 0 ?
                                        <div className="text-center">
                                            <span className="text-muted">{t('Empty.Products')}</span>
                                        </div>
                                        : ''}
                            </>
                            : <div className="d-flex justify-content-center mt-3 mb-3">
                                <Spinner animation="border" />
                            </div>
                            }
                    </Tab>
                    <Tab eventKey={t('Header.Ratings')} key={t('Header.Ratings')} title={t('Header.Ratings')}>
                        <GetRatings username={username} hideAvg />
                    </Tab>
                </Tabs>
            </Container>
        </>
    )
}

export default UserPublicPage;