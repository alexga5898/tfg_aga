import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/esm/Button';
import Container from 'react-bootstrap/esm/Container';
import Form from 'react-bootstrap/esm/Form';
import { getWebsocket } from '../selectors';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as actions from '../actions';
import * as selectors from '../selectors';
import Messages from './Messages';
import { useTranslation } from 'react-i18next';
import { BackButton, Errors } from '../../common';

const ChatDetails = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const { chatId } = useParams();
    const userId = useSelector(selectors.getUserId);
    const [message, setMessage] = useState('');
    const stompClient = useSelector(getWebsocket);
    const initialChat = useSelector(selectors.getChat);
    const [chat, setChat] = useState(initialChat);
    const [backendErrors, setBackendErrors] = useState(null);
    const messageReceived = useSelector(selectors.getMessage);

    useEffect(() => {
        if (initialChat) {
            setChat(initialChat);
        }
    }, [initialChat, chat])

    useEffect(() => {
        if (!Number.isNaN(chatId) && backendErrors === null) {
            dispatch(actions.getChat(chatId,
                () => dispatch(actions.markAsRead(chatId)),
                errors => {
                    setBackendErrors(errors);
                }));
        }
    }, [chatId, backendErrors, dispatch])

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (chat && stompClient && message.length > 0) {
            const sendMessage = {
                productId: chat.productId,
                recipientId: userId === chat.sellerId ? chat.buyerId : chat.sellerId,
                content: message
            }
            stompClient.send('/app/sendMessage', {}, JSON.stringify(sendMessage));
            form.reset();
            setMessage('');
        }
    }

    if (!chat) {
        return null;
    }

    return (
        <Container>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <div className="d-flex">
                <BackButton />
                <Link to={`/products/${chat.productId}`}>
                    <div className="question-info-img ml-4">
                        <img className="question-info-img-thumbnail" src={`http://localhost:8080/products/${chat.productId}/thumbnail`} alt="" />
                    </div>
                </Link>
                <div>
                    <span>
                        <h5>
                            {chat.productName} {chat.productPrice}€
                        </h5>
                    </span>
                    <span className="small">{t('Field.User')}: </span>
                    <span className="text-muted small">{chat.sellerName}</span>
                </div>
            </div>
            <hr />
            <div className="chat-scroll">
                <Messages chat={chat} messageReceived={messageReceived} />
            </div>
            <br />
            <Form noValidate onSubmit={e => submitHandler(e)}>
                <Form.Group controlId="subject">
                    <Form.Control required onChange={(e) => setMessage(e.target.value)}/>
                </Form.Group>
                <Button variant="success" type="submit" disabled={message.length === 0 || message.length > 800}>{t('Questions.SendButton')}</Button>
            </Form>
        </Container>
    )
}

export default ChatDetails;