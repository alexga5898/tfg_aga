import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Container from "react-bootstrap/esm/Container";
import * as actions from "../actions";
import * as selectors from "../selectors";
import { useTranslation } from "react-i18next";
import Button from "react-bootstrap/esm/Button";
import { Errors } from '../../common';
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import PropTypes from 'prop-types';
import Spinner from "react-bootstrap/esm/Spinner";

const Messages = ({ chat, messageReceived }) => {
    const dispatch = useDispatch();
    const endRef = useRef(null);
    const [t] = useTranslation();
    const userId = useSelector(selectors.getUserId);
    const hasNext = useSelector(selectors.getMessagesNext);
    const messages = useSelector(selectors.getMessagesList);
    const [backendErrors, setBackendErrors] = useState(null);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (chat.id) {
            dispatch(actions.getMessages(chat.id,
                () => {
                    setLoading(false);
                    scrollToBottom();
                    dispatch(actions.markAsRead(chat.id,
                        () => '',
                        errors => setBackendErrors(errors)));
                },
                errors => setBackendErrors(errors)));
        }
    }, [dispatch, chat])

    useEffect(() => {
        if (messageReceived && Number(chat.id) === Number(messageReceived.chatId)) {
            dispatch(actions.getMessages(chat.id,
                () => {
                    setLoading(false);
                    scrollToBottom();
                    dispatch(actions.markAsRead(chat.id,
                        () => '',
                        errors => setBackendErrors(errors)));
                },
                errors => setBackendErrors(errors)));
        }
    }, [dispatch, messageReceived, chat])

    const loadMoreMessages = () => {
        if (chat.id) {
            dispatch(actions.getMoreMessages(chat.id, page,
                () => "",
                errors => setBackendErrors(errors)))
            setPage(page + 1);
        }
    }

    const scrollToBottom = () => {
        endRef.current?.scrollIntoView({ behavior: "smooth", block: 'nearest', inline: 'start' })
    }

    return (
        <>
            {!loading ?
                <>
                    <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                    <div className="d-grid gap-2 pr-3">
                        <Button variant="success" onClick={() => loadMoreMessages()} disabled={!hasNext}>{t('Questions.Load')}</Button>
                    </div>
                    <br />
                    {messages ? messages.slice(0).reverse().map(message =>
                        <div key={message.id}>
                            {userId !== message.senderId ?
                                <>
                                    <Row>
                                        <Col>
                                            <Container className="question-info-modal rounded break-word">
                                                <span>{message.content}</span>
                                            </Container>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <span className="text-muted small text-muted-small">
                                                {t('Date.Format3', { date: new Date(message.date) })}
                                            </span>
                                        </Col>
                                    </Row>
                                </>
                                :
                                <>
                                    <Row>
                                        <Col>
                                            <Container className="question-info-modal question-info-modal-response-container rounded break-word mr-3">
                                                <span>{message.content}</span>
                                            </Container>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col>
                                            <span className="question-info-modal-response text-muted small text-muted-small mr-3">
                                                {t('Date.Format3', { date: new Date(message.date) })}
                                            </span>
                                        </Col>
                                    </Row>
                                </>
                            }
                            <br />

                        </div>
                    ) : ''
                    }
                    <div ref={endRef} />
                </>
                :
                <div className="d-flex justify-content-center mt-3 mb-3">
                    <Spinner animation="border" />
                </div>
            }
        </>
    )
}

export default Messages;

Messages.propTypes = {
    chat: PropTypes.object,
    messageReceived: PropTypes.object
};
