import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import users from '../../users/';

const LogOut = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const stompClient = useSelector(users.selectors.getWebsocket);

    useEffect(() => {
        history.push("/");   
        dispatch(users.actions.logOut());
        stompClient.disconnect();
        dispatch(users.actions.cleanConnection());
     });
     
     return null;
}

export default LogOut;