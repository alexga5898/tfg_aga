import Col from "react-bootstrap/esm/Col";
import ListGroup from "react-bootstrap/esm/ListGroup";
import Row from "react-bootstrap/esm/Row";
import Container from "react-bootstrap/esm/Container";
import moment from 'moment';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as selectors from "../selectors";
import * as actions from "../actions";
import { useHistory } from "react-router-dom";
import { Badge } from "@material-ui/core";
import PropTypes from 'prop-types';

const Chat = ({ chat }) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const userId = useSelector(selectors.getUserId);
    const messageReceived = useSelector(selectors.getMessage);
    const [showBadge, setShowBadge] = useState(false);

    useEffect(() => {
        if (chat || (messageReceived && messageReceived.senderId !== userId)) {
            dispatch(actions.checkNewMessages(chat.id,
                response => setShowBadge(response),
                errors => console.log(errors)))
        }
    }, [dispatch, chat, messageReceived, showBadge, userId])


    if (!chat) {
        return null;
    }

    return (
        <>
            <ListGroup.Item action onClick={() => history.push(`chats/${chat.id}`)}>
                <div className="question-info-img">
                    <img className="question-info-img-thumbnail" src={`http://localhost:8080/products/${chat.productId}/thumbnail`} alt="" />
                </div>
                <div className="question-info-preview">
                    <Container fluid>
                        <Row>
                            <Col md="auto" style={{ padding: 0 }}>
                                <span className="text-muted small">
                                    {userId !== chat.buyerId ? chat.buyerName : chat.sellerName}
                                </span>
                            </Col>
                            <Col>
                                <span className="text-muted question-info-preview-time">
                                    {moment(chat.lastModified).fromNow()}
                                </span>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="pl-0">
                                <div className="pt-2">
                                    <span>
                                        <h5>
                                            {chat.productName}
                                        </h5>
                                    </span>
                                </div>
                            </Col>
                            <Col>
                                <br />
                                <Badge className="float-right mr-4" badgeContent="" color="secondary" invisible={!showBadge} />
                            </Col>
                        </Row>
                    </Container>
                </div>
            </ListGroup.Item>
        </>
    )
}

Chat.propTypes = {
    chat: PropTypes.object
};


export default Chat;