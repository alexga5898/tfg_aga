import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { Errors } from '../../common';
import * as actions from '../actions';
import Container from 'react-bootstrap/Container';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/esm/Spinner';

const LogInModal = ({ show, onClose }) => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [submitted, setSubmitted] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setSubmitted(true);
        if (form.checkValidity()) {
            setLoading(true);
            dispatch(actions.logIn(
                email,
                password,
                () => {
                    onClose();
                    setLoading(false);
                },
                errors => {
                    setBackendErrors(errors);
                    setLoading(false);
                },
                () => {
                    history.push('/users/logIn');
                    dispatch(actions.logOut());
                }
            ));
        }
        else {
            setBackendErrors(null);
        }
    }

    return (
        <div>
            <Modal centered show={show} onHide={() => onClose()} >
                <Modal.Header closeButton>
                    <Modal.Title>{t('Header.LogIn')}</Modal.Title>
                </Modal.Header>
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <Form noValidate validated={submitted} onSubmit={e => submitHandler(e)}>
                    <Modal.Body>
                        <Form.Group controlId="email">
                            <Form.Label>{t('Field.Email')}:</Form.Label>
                            <Form.Control required
                                onChange={e => setEmail(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>{t('Field.Password')}:</Form.Label>
                            <Form.Control required
                                type="password"
                                onChange={e => setPassword(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        {!loading ?
                            <Button block variant="primary" type="submit" >{t('Users.LogInButton')}</Button>
                            :
                            <Button variant="primary" block disabled >
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                {t('App.Loading')}
                            </Button>
                        }
                        <Container>
                            <span>{t('Users.NoAccountLink')}</span>
                            <Link to="/users/sign" onClick={() => onClose()}> {t('Users.ClickHere')} </Link>
                        </Container>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

LogInModal.propTypes = {
    show: PropTypes.bool,
    onClose: PropTypes.func
};

export default LogInModal;