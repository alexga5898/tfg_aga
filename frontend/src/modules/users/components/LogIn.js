import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import { Errors } from '../../common';
import * as actions from '../actions';
import Spinner from 'react-bootstrap/esm/Spinner';

const LogIn = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [submitted, setSubmitted] = useState(false);
    const [backendErrors, setBackendErrors] = useState(null);
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setSubmitted(true);
        if (form.checkValidity()) {
            setLoading(true);
            dispatch(actions.logIn(
                email,
                password,
                () => {
                    history.push('/');
                    setLoading(false);
                    setBackendErrors(null);
                },
                errors => {
                    setBackendErrors(errors);
                    setLoading(false);
                },
                () => {
                    dispatch(actions.logOut());
                    history.push('/users/logIn');
                }
            ));
        }
        else {
            setBackendErrors(null);
        }
    }


    return (
        <div>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Container className={"rounded"}>
                <h1>{t('Header.LogIn')}</h1>
                <Form noValidate validated={submitted} onSubmit={e => submitHandler(e)}>
                    <Form.Group controlId="email">
                        <Form.Label>{t('Field.Email')}:</Form.Label>
                        <Form.Control required
                            onChange={e => setEmail(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>{t('Field.Password')}:</Form.Label>
                        <Form.Control required
                            type="password"
                            onChange={e => setPassword(e.target.value)} />
                    </Form.Group>
                    {!loading ?
                        <Button variant="primary" type="submit" >{t('Users.LogInButton')}</Button>
                        :
                        <Button variant="primary" disabled >
                            <Spinner
                                as="span"
                                animation="border"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />
                            {t('App.Loading')}
                        </Button>
                    }
                    <hr />
                    <div>
                        <span>{t('Users.NoAccountLink')}</span>
                        <Link to="/users/sign"> {t('Users.ClickHere')} </Link>
                    </div>
                </Form>
            </Container>
        </div>
    );
}

export default LogIn;