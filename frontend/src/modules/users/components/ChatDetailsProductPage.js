import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/esm/Button';
import Container from 'react-bootstrap/esm/Container';
import Form from 'react-bootstrap/esm/Form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import * as actions from '../actions';
import * as selectors from '../selectors';
import catalog from '../../catalog';
import Messages from './Messages';
import { BackButton, Errors } from '../../common';
import { useTranslation } from 'react-i18next';

const ChatDetailsProductPage = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const userId = useSelector(selectors.getUserId);
    const { productId } = useParams();
    const { sellerId } = useParams();
    const [message, setMessage] = useState('');
    const stompClient = useSelector(selectors.getWebsocket);
    const initialChat = useSelector(selectors.getChat);
    const [chat, setChat] = useState(initialChat);
    const [backendErrors, setBackendErrors] = useState(null);
    const messageReceived = useSelector(selectors.getMessage);
    const product = useSelector(catalog.selectors.getProductInf);

    useEffect(() => {
        if (initialChat) {
            setChat(initialChat);
        }
    }, [initialChat, chat])

    useEffect(() => {
        if (!product) {
            dispatch(catalog.actions.getProduct(productId,
                error => setBackendErrors(error)));
        }
    }, [dispatch, product, productId])

    useEffect(() => {
        if (!chat && !Number.isNaN(productId) && !Number.isNaN(sellerId) && messageReceived
            && Number(messageReceived.senderId) === Number(userId) && Number(messageReceived.recipientId === Number(sellerId)
                && Number(messageReceived.productId && productId))) {
            dispatch(actions.getChatIfExistent(sellerId, productId,
                () => '',
                errors => setBackendErrors(errors)))
        }
    }, [chat, dispatch, productId, sellerId, messageReceived, userId])


    useEffect(() => {
        if (!chat && !Number.isNaN(productId) && !Number.isNaN(sellerId)) {
            dispatch(actions.getChatIfExistent(sellerId, productId,
                () => '',
                errors => setBackendErrors(errors)))
        }

    }, [chat, dispatch, productId, sellerId])

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (product && stompClient && message.length > 0) {
            const sendMessage = {
                productId: productId,
                recipientId: sellerId,
                content: message
            }
            stompClient.send('/app/sendMessage', {}, JSON.stringify(sendMessage));
            form.reset();
            setMessage('');
        }
    }

    if (!product) {
        return null;
    }

    return (
        <Container>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <div className="d-flex">
                <BackButton />
                <Link to={`/products/${productId}`}>
                    <div className="question-info-img ml-4">
                        <img className="question-info-img-thumbnail" src={`http://localhost:8080/products/${productId}/thumbnail`} alt="" />
                    </div>
                </Link>
                <div>
                    <span>
                        <h5>
                            {product.name} {product.price}€
                        </h5>
                    </span>
                    <span className="small">{t('Field.User')}: </span>
                    <span className="text-muted small">{product.sellerUsername}</span>
                </div>
            </div>
            <hr />
            <div className="chat-scroll">
                {chat ?
                    <Messages chat={chat} messageReceived={messageReceived} />
                    : ''}
            </div>
            <br />
            <Form noValidate onSubmit={e => submitHandler(e)}>
                <Form.Group controlId="subject">
                    <Form.Control required onChange={(e) => setMessage(e.target.value)} />
                </Form.Group>
                <Button variant="success" type="submit" disabled={message.length === 0 || message.length > 800}>{t('Questions.SendButton')}</Button>
            </Form>
        </Container>
    )
}

export default ChatDetailsProductPage;
