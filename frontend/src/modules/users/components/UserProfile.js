import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import * as selectors from '../selectors';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Errors, Success } from '../../common';
import { useTranslation } from 'react-i18next';
import * as actions from '../actions';
import backend from '../../../backend';

const UserProfile = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const user = useSelector(selectors.getUserProfile);
    const [backendErrors, setBackendErrors] = useState(null);
    const [backendSuccess, setBackendSuccess] = useState(null);
    const [firstName, setFirstName] = useState(user.firstName);
    const [lastName, setLastName] = useState(user.lastName);
    const [email, setEmail] = useState(user.email);
    const [username, setUsername] = useState(user.username);
    const activeTransfers = user.activeTransfers;
    const [changed, setChanged] = useState(false);
    const history = useHistory();

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();

        if (form.checkValidity()) {
            dispatch(actions.updateProfile(
                {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    username: username
                },
                () => {
                    history.push('/users/profile');
                    setBackendSuccess(t('Users.UserProfileUpdated'));
                    setBackendErrors(null);
                    setTimeout(() => {
                        setBackendSuccess(null);
                    }, 3000);
                    setChanged(false);
                },
                errors => {
                    setBackendErrors(errors);
                }
            ))
        }
    }

    const changeHandler = (event) => {
        const form = event.currentTarget;

        if (form.firstName.value.length === 0) {
            setChanged(false)
            return;
        }
        if (form.lastName.value.length === 0) {
            setChanged(false)
            return;
        }
        if (form.email.value.length === 0) {
            setChanged(false)
            return;
        }
        if (form.username.value.length < 3) {
            setChanged(false)
            return;
        }
        if (form.firstName.value !== user.firstName) {
            setChanged(true);
            return;
        }
        if (form.lastName.value !== user.lastName) {
            setChanged(true);
            return;
        }
        if (form.email.value !== user.email) {
            setChanged(true);
            return;
        }
        if (form.username.value !== user.username) {
            setChanged(true);
            return;
        }
        setChanged(false);
    }

    const handleClick = () => {
        backend.userService.connectStripe(
            response => window.location.href = response.data.url,
            errors => setBackendErrors(errors)
        )
    };

    return (
        <div>
            <Container className={"rounded"} >
                <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                <Success success={backendSuccess} onClose={() => setBackendSuccess(null)} />
                <h2 className="d-inline-block mr-3"> {t('Users.ProfileHeader')} </h2>
                <Button variant="link" onClick={() => history.push(`/users/seller/${user.username}`)} className="button-as-link d-inline-block mb-2">{t('Users.PublicProfile')}</Button>
                <Form noValidate onSubmit={e => submitHandler(e)}
                    onChange={e => changeHandler(e)}>
                    <Form.Group as={Row} controlId="firstName">
                        <Form.Label column sm={2}>{t('Field.FirstName')}:</Form.Label>
                        <Col sm={10}>
                            <Form.Control required
                                defaultValue={firstName}
                                onChange={e => {
                                    setFirstName(e.target.value);
                                }} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="lastName">
                        <Form.Label column sm={2}>{t('Field.LastName')}:</Form.Label>
                        <Col sm={10}>
                            <Form.Control required
                                defaultValue={lastName}
                                onChange={e => {
                                    setLastName(e.target.value);
                                }} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="email">
                        <Form.Label column sm={2}>{t('Field.Email')}:</Form.Label>
                        <Col sm={10}>
                            <Form.Control required
                                defaultValue={email}
                                onChange={e => {
                                    setEmail(e.target.value);
                                }}
                            />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="username">
                        <Form.Label column sm={2}>{t('Field.Username')}:</Form.Label>
                        <Col sm={10}>
                            <Form.Control required
                                defaultValue={username}
                                onChange={e => {
                                    setUsername(e.target.value);
                                }} />
                        </Col>
                    </Form.Group>
                    <Button variant="success" type="submit" disabled={!changed} >
                        {t('Users.SaveChangesButton')}
                    </Button>
                </Form>
                <hr />
                <h2>Stripe</h2>
                <p>{t('Users.StripeText')}</p>
                {!activeTransfers ?
                    <>
                        <Button onClick={handleClick}>{t('Users.ConnectWithStripe')}</Button>
                        <p className="mt-3">{t('Users.ConnectWithStripeText')}</p>
                    </>
                    :
                    <Button disabled variant="success">{t('Users.Connected')}</Button>}

            </Container>
        </div >
    )
}

export default UserProfile;