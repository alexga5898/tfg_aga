import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as selectors from '../selectors';
import * as actions from '../actions';
import Container from 'react-bootstrap/esm/Container';
import Chat from './Chat';
import { useTranslation } from 'react-i18next';
import { Paginate } from '../../common';
import { useHistory, useLocation } from 'react-router-dom';
import queryString from 'query-string';

const Chats = () => {
    const [t] = useTranslation();
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    const page = queryString.parse(location.search).page > 0 ? Number(queryString.parse(location.search).page) : 0;
    const totalPages = useSelector(selectors.getChatsTotalPages);
    const hasNext = useSelector(selectors.getChatsHasNext);
    const chats = useSelector(selectors.getChatsList);
    const messageReceived = useSelector(selectors.getMessage);

    useEffect(() => {
        dispatch(actions.getChats(page,
            () => ''));
    }, [dispatch, messageReceived, page])

    const nextPage = () => {
        history.push(`?page=${page + 1}`);
    }

    const previousPage = () => {
        history.push(`?page=${page - 1}`);
    }

    const goPage = (page) => {
        history.push(`?page=${page}`);
    }


    return (
        <Container className="rounded">
            <h3>{t('Header.Chats')}</h3>
            <br />
            {chats ? chats.map(chat =>
                <Chat key={chat.id} chat={chat} />
            ) : ''}
            <Paginate page={page} totalPages={totalPages} hasNext={hasNext}
                previous={() => previousPage()} next={() => nextPage()} go={(page) => goPage(page)} />
        </Container>
    )
}

export default Chats;