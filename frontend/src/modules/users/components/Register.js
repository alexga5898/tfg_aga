import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useTranslation, Trans } from 'react-i18next';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import { Errors } from '../../common';
import * as actions from '../actions';
import Spinner from 'react-bootstrap/esm/Spinner';

const Register = () => {
    const [t] = useTranslation();
    const dispatch = useDispatch();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [submitted, setSubmitted] = useState(false);
    const [passwordMatch, setPasswordMatch] = useState(true);
    const [backendErrors, setBackendErrors] = useState(null);
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    const submitHandler = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        setSubmitted(true);
        if (validateForm(form) && passwordMatch) {
            setLoading(true);
            dispatch(actions.register(
                {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    username: username,
                    password: password,
                },
                () => {
                    history.push('/');
                    setLoading(false);
                    setBackendErrors(null);
                },
                errors => {
                    setBackendErrors(errors);
                    setLoading(false);
                },
                () => {
                    history.push('/users/logIn');
                    dispatch(actions.logOut());
                }
            ));
        }
        else {
            setBackendErrors(null);
        }
    }

    const checkConfirmPassword = (password1, password2) => {
        if (password1 === password2) {
            return true;
        }
        else return false;
    }

    const validateForm = (form) => {
        if (form.firstName.value.length === 0) {
            return false;
        }
        if (form.lastName.value.length === 0) {
            return false;
        }
        if (form.email.value.length < 5) {
            return false;
        }
        if (form.username.value.length < 3) {
            return false;
        }
        if (form.password.value.length < 6 || form.password.value.length > 30) {
            return false;
        }
        return true;
    }

    return (
        <div>
            <Container className={"rounded"}>
                <div>
                    <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
                    <h1>{t('Users.NewAccountHeader')}</h1>
                    <Form noValidate onSubmit={e => submitHandler(e)}>
                        <Form.Group controlId="firstName">
                            <Form.Label>{t('Field.FirstName')}:</Form.Label>
                            <Form.Control
                                required
                                onChange={e => setFirstName(e.target.value)}
                                isInvalid={submitted && firstName.length === 0}
                                isValid={submitted} />
                            <Form.Control.Feedback type="invalid">
                                <Trans i18nKey='Field.InvalidInput' values={{ name: t('Field.FirstName') }} />
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="lastName">
                            <Form.Label>{t('Field.LastName')}:</Form.Label>
                            <Form.Control name="lastName" required
                                onChange={e => setLastName(e.target.value)}
                                isInvalid={submitted && lastName.length === 0}
                                isValid={submitted} />
                            <Form.Control.Feedback type="invalid">
                                <Trans i18nKey='Field.InvalidInput' values={{ name: t('Field.LastName') }} />
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="email">
                            <Form.Label>{t('Field.Email')}:</Form.Label>
                            <Form.Control name="email" required
                                onChange={e => setEmail(e.target.value)}
                                isInvalid={submitted && email.length < 5}
                                isValid={submitted} />
                            <Form.Control.Feedback type="invalid">
                                {email.length < 5 && email.length !== 0 ?
                                    t('Field.MinEmail')
                                    :
                                    <Trans i18nKey='Field.InvalidInput' values={{ name: t('Field.Email') }} />
                                }
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="username">
                            <Form.Label>{t('Field.Username')}:</Form.Label>
                            <Form.Control name="username" required
                                onChange={e => setUsername(e.target.value)}
                                isInvalid={submitted && username.length < 3}
                                isValid={submitted} />
                            <Form.Control.Feedback type="invalid">
                                {username.length < 3 && username.length !== 0 ?
                                    t('Field.MinUsername')
                                    :
                                    <Trans i18nKey='Field.InvalidInput' values={{ name: t('Field.Username') }} />
                                }
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>{t('Field.Password')}:</Form.Label>
                            <Form.Control type="password" name="password" required
                                onChange={e => {
                                    setPassword(e.target.value);
                                    setPasswordMatch(checkConfirmPassword(e.target.value, confirmPassword))
                                }}
                                isInvalid={(submitted && password.length < 6)
                                    || (submitted && password.length > 30)}
                                isValid={submitted && password.length >= 6
                                    && password.length <= 30} />
                            <Form.Text id="passwordHelp" muted>
                                {t('Field.PasswordHelpText')}</Form.Text>
                            <Form.Control.Feedback type="invalid">
                                {(password.length < 6 && password.length !== 0) || password.length > 30 ?
                                    t('Field.PasswordHelpText')
                                    :
                                    <Trans i18nKey='Field.InvalidInput' values={{ name: t('Field.Password') }} />
                                }
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group controlId="confirmPassword">
                            <Form.Label>{t('Field.ConfirmPassword')}:</Form.Label>
                            <Form.Control type="password" name="confirmPassword" required
                                onChange={e => {
                                    setConfirmPassword(e.target.value);
                                    setPasswordMatch(checkConfirmPassword(password, e.target.value))
                                }}
                                isInvalid={(submitted && !passwordMatch)
                                    || (submitted && confirmPassword.length < 6)}
                                isValid={submitted && passwordMatch && confirmPassword.length >= 6} />
                            <Form.Control.Feedback type="invalid">
                                {confirmPassword.length === 0 ?
                                    <Trans i18nKey='Field.InvalidInput'
                                        values={{ name: t('Field.ConfirmPassword') }} />
                                    : passwordMatch && confirmPassword.length > 0 ?
                                        <p> {t('Field.PasswordHelpText')}</p>
                                        :
                                        <p> {t('Field.ConfirmPasswordMatch')}</p>
                                }
                            </Form.Control.Feedback>
                        </Form.Group>
                        {!loading ?
                            <Button variant="primary" type="submit">{t('Users.CreateButton')}</Button>
                            :
                            <Button variant="primary" disabled >
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                {t('App.Loading')}
                            </Button>
                        }
                    </Form>
                    <hr />
                    <div>
                        <span>{t('Users.AlreadyAccount')}</span>
                        <Link to="/users/logIn"> {t('Users.LogInHere')} </Link>
                    </div>
                </div>
            </Container>
        </div>
    );

}

export default Register;