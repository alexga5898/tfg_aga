import React, { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import Container from "react-bootstrap/esm/Container";
import { useHistory } from "react-router";
import backend from '../../../backend';
import { Errors } from "../../common";

const RetripeConnect = () => {
    const [backendErrors, setBackendErrors] = useState(null);
    const history = useHistory();

    const refreshHandler = () => {
        backend.userService.connectStripe(
            response => window.location.href = response.data.url,
            errors => setBackendErrors(errors)
        )
    }

    return (
        <>
            <Errors errors={backendErrors} onClose={() => setBackendErrors(null)} />
            <Container className="bg-transparent">
                <Button onClick={() => refreshHandler()} variant="success">Volver a intentar</Button>
                <Button onClick={() => history.push('/users/profile')} variant="danger" className="float-right">Cancelar</Button>
            </Container>
        </>
    )
}

export default RetripeConnect;