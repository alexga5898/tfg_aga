import { combineReducers } from 'redux';
import * as actionTypes from './actionTypes';

const initialState = {
    user: null,
    notification: null,
    chats: null,
    chat: null,
    messages: null,
    message: null,
    websocket: null
}

const user = (state = initialState.user, action) => {
    switch (action.type) {
        case actionTypes.REGISTERED:
            return action.authenticatedUser;
        case actionTypes.LOGGED_IN:
            return action.authenticatedUser;
        case actionTypes.LOGGED_OUT:
            return initialState.user;
        case actionTypes.UPDATED_PROFILE:
            return action.user;
        default:
            return state;
    }
}

const notification = (state = initialState.notification, action) => {
    switch (action.type) {
        case actionTypes.CHECK_NEWMESSAGES:
            return action.isNotification;
        default:
            return state;
    }
}

const chats = (state = initialState.chats, action) => {
    switch (action.type) {
        case actionTypes.GET_CHATS:
            return action.chats;
        default:
            return state;
    }
}

const chat = (state = initialState.chat, action) => {
    switch (action.type) {
        case actionTypes.GET_CHAT:
            return action.chatMessages;
        case actionTypes.CLEAN_CHAT:
            return initialState.chat;
        default:
            return state;
    }
}

const messages = (state = initialState.messages, action) => {
    switch (action.type) {
        case actionTypes.GET_MESSAGES:
            return action.chatMessages;
        case actionTypes.GET_MORE_MESSAGES:
            return {
                existMoreItems: action.chatMessages.existMoreItems,
                items: [...state.items, ...action.chatMessages.items]
            }
        default:
            return state;
    }
}

const message = (state = initialState.message, action) => {
    switch (action.type) {
        case actionTypes.MESSAGE_RECEIVED:
            return action.message;
        case actionTypes.CLEAN_MESSAGE_RECEIVED:
            return initialState.message;
        default:
            return state;
    }
}

const websocket = (state = initialState.websocket, action) => {
    switch (action.type) {
        case actionTypes.CONNECTED_WEBSOCKET:
            return action.websocket;
        default:
            return state;
    }
}

const reducer = combineReducers({
    user,
    notification,
    chats,
    chat,
    messages,
    message,
    websocket
});

export default reducer;