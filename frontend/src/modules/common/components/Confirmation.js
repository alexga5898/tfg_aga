import React from "react";
import Button from "react-bootstrap/esm/Button";
import Modal from "react-bootstrap/Modal";
import PropTypes from 'prop-types';
import { useTranslation } from "react-i18next";
import { Errors } from "..";

const Confirmation = ({ show, onClose, action, errors, closeErrors }) => {
    const [t] = useTranslation();

    return (
        <Modal centered show={show} onHide={() => onClose()}>
            <Errors errors={errors} onClose={() => closeErrors()}/>
            <Modal.Header closeButton>
                <Modal.Title>
                    {t("Products.ConfirmDialogHeader")}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    {t("Products.ConfirmDialogBody")}
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={() => onClose()}> {t("Products.CancelButton")}</Button>
                <Button variant="danger" onClick={() => action()}> {t("Products.DeleteButton")}</Button>
            </Modal.Footer>
        </Modal>
    );
}

Confirmation.propTypes = {
    show: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    action: PropTypes.func.isRequired,
    errors: PropTypes.object,
    closeErrors: PropTypes.func
};


export default Confirmation;