import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/Alert';
import Media from 'react-bootstrap/Media';
import { useTranslation } from 'react-i18next';

const Errors = ({ errors, onClose }) => {
    const [t] = useTranslation();

    if (errors === null) {
        return null;
    }

    let networkError = false;
    let error;
    let fieldErrors;

    if (typeof errors === "boolean") {
        networkError = true;
    }

    else if (errors.error != null) {
        error = errors.error;

    } else {
        fieldErrors = [];
        const field = errors.fieldErrors;
        for (let i = 0; i < field.length; i++) {
            fieldErrors.push({
                key: field[i].field.charAt(0).toUpperCase() + field[i].field.slice(1),
                value: field[i].message
            });
        }
    }

    return (
        <div>
            {networkError ?
                <Alert variant='danger' onClose={() => onClose()} dismissible>
                    {t('Error.NetworkError')}
                </Alert>
                :
                ''
            }

            {error ?
                <Alert variant="danger" onClose={() => onClose()} dismissible>
                    {error}
                </Alert>
                :
                ''
            }

            {fieldErrors ?
                <ul className="list-unstyled">
                    <Alert variant='danger' onClose={() => onClose()} dismissible>
                        {fieldErrors.map((fieldError) => (
                            <Media as="li" key={fieldError.key}>
                                <Media.Body>
                                    {t('Field.' + fieldError.key) + ": " + fieldError.value + "."}
                                </Media.Body>
                            </Media>
                        ))}
                    </Alert>
                </ul>
                :
                ''
            }
        </div>
    )
}

Errors.propTypes = {
    errors: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.bool]),
    onClose: PropTypes.func.isRequired
};

export default Errors;