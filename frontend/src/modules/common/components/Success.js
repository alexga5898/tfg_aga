import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/esm/Alert';
import { Link } from 'react-router-dom';

const Success = ({ success, onClose, textLink, link }) => {

    if (success === null) {
        return null;
    }

    return (

        // <div
        //     style={{
        //         position: 'absolute',
        //         top: "70px",
        //         right: 0,
        //         backgroundColor: "#31c571"
        //     }}
        // >
        //     {success ?
        //         <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
        //             <Toast.Header>
        //                 <strong className="mr-auto">Success!</strong>
        //             </Toast.Header>
        //             <Toast.Body>
        //                 {success}
        //             </Toast.Body>
        //         </Toast>
        //         :
        //         ''
        //     }
        // </div>

        <div>
            <Alert variant="success" onClose={() => onClose()} dismissible className="global-alert">
                {success}
                {link && textLink ?
                    <Link to={link}>
                        {textLink}
                    </Link>
                    : '' }
                </Alert>
        </div>

    )
}

Success.propTypes = {
    success: PropTypes.string,
    textLink: PropTypes.string,
    link: PropTypes.string,
    onClose: PropTypes.func.isRequired
};

export default Success;