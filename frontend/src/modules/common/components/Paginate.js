import React from "react";
import Pagination from "react-bootstrap/Pagination";
import PropTypes from 'prop-types';

const Paginate = ({ page, totalPages, hasNext, previous, next, go }) => {
    let pages = [];
    for (let i = 0; i < totalPages; i++) {
        if (i === 0 || i === totalPages - 1 || Math.abs(i - page) <= 2) {
            pages.push(<Pagination.Item active={page === i} key={i} onClick={() => go(i)}> {i + 1} </Pagination.Item>);
        }
    }
    return (
        <div className="d-flex justify-content-center mt-5">
            <Pagination page={page}>
                <Pagination.Prev disabled={page === 0} onClick={() => { previous(page) }} />
                {pages}
                <Pagination.Next disabled={!hasNext} onClick={() => { next(page) }} />
            </Pagination>
        </div>
    );
}

Paginate.propTypes = {
    page: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired,
    hasNext: PropTypes.bool.isRequired,
    previous: PropTypes.func.isRequired,
    next: PropTypes.func.isRequired
};

export default Paginate;