import React from "react";
import { faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";

const BackButton = () => {
    const history = useHistory();

    return (
        <div>
            <Button variant="outline-dark" onClick={() => history.goBack()} className="back-button" size="lg">
                <FontAwesomeIcon icon={faArrowCircleLeft}/>
            </Button>
        </div>
    )
}


export default BackButton;