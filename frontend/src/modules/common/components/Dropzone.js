import React from "react";
import DropzoneComponent from 'react-dropzone-component';
import ReactDOMServer from 'react-dom/server';
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { faImage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from "react-bootstrap/Button";
import { useTranslation } from "react-i18next";
import PropTypes from 'prop-types';

const Dropzone = ({ imageName, imageUrl, addImage, removeImage, invalidFile }) => {
    const [t] = useTranslation();
    const validImages = "image/jpeg,image/png,image/jpg"
    const componentConfig = { postUrl: 'no-url' };
    const djsConfig = {
        maxFiles: 1, autoProcessQueue: false, acceptedFiles: validImages,
        maxFilesize: 10,
        previewTemplate: ReactDOMServer.renderToStaticMarkup(
            <div className="dz-preview dz-file-preview">
                <img data-dz-thumbnail alt={""} />
                <div className="dz-details">
                    <div className="dz-filename"><span data-dz-name></span></div>
                </div>
                <Button variant="outline-danger" data-dz-remove block size="sm" className="delete-image-button">
                    <span><FontAwesomeIcon icon={faTimesCircle} size="xs" /></span>
                </Button>
                <div className="dz-error-mark"><span><FontAwesomeIcon icon={faExclamationTriangle} /></span></div>
                <div className="dz-error-message"><span>{t('Products.DropdownErrorMessage')}</span></div>
            </div>
        )
    }
    const eventHandlers = {
        addedfile: (file) => {
            if (file.size !== -1) {
                addImage(file);
                if (!validImages.includes(file.type))
                    invalidFile();
            } else {
                addImage(new File([], imageName))
            }
        },
        removedfile: (file) => {
            if (validImages.includes(file.type))
                removeImage(new File([], "deleted"));
            else removeImage(null);
        },
        init: (myDropzone) => {
            if (imageName && imageUrl) {
                let mockFile = { name: imageName, size: -1, type: "image/jpg", accepted: true };
                myDropzone.files.push(mockFile)
                myDropzone.emit("addedfile", mockFile);
                myDropzone.emit("thumbnail", mockFile, imageUrl);
                myDropzone.emit("complete", mockFile);
            }
            myDropzone.on("maxfilesexceeded", function (file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
            });
        },
    };

    return (
        <div>
            <DropzoneComponent
                config={componentConfig}
                eventHandlers={eventHandlers}
                djsConfig={djsConfig}>
                <div className="dz-message">
                    <span>
                        <FontAwesomeIcon icon={faImage} size="4x" color={"grey"} />
                    </span>
                </div>
            </DropzoneComponent>
        </div>
    )
}

Dropzone.propTypes = {
    imageName: PropTypes.string,
    imageUrl: PropTypes.string,
    addImage: PropTypes.func.isRequired,
    removeImage: PropTypes.func.isRequired,
    invalidFile: PropTypes.func.isRequired
};

export default Dropzone;