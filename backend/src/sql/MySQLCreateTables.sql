----- Drop tables

ALTER TABLE ProductTable DROP FOREIGN KEY userIdProductPK;
ALTER TABLE OrderTable DROP FOREIGN KEY buyerIdOrderPK;
ALTER TABLE OrderTable DROP FOREIGN KEY sellerIdOrderPK;
ALTER TABLE OrderItemTable DROP FOREIGN KEY productIdOrderItemPK;
ALTER TABLE OrderItemTable DROP FOREIGN KEY orderIdOrderItemPK;
ALTER TABLE RatingTable DROP FOREIGN KEY buyerIdRatingPK;
ALTER TABLE RatingTable DROP FOREIGN KEY sellerIdRatingPK;
ALTER TABLE RatingTable DROP FOREIGN KEY orderIdRatingPK;
ALTER TABLE ShoppingCartItemTable DROP FOREIGN KEY productIdShoppingCartItemPK;
ALTER TABLE ShoppingCartItemTable DROP FOREIGN KEY shoppingCartIdShoppingCartItemPK;
ALTER TABLE AddressTable DROP FOREIGN KEY orderIdAddressPK;
ALTER TABLE ShoppingCartTable DROP FOREIGN KEY userIdShoppingCartPK;
ALTER TABLE ImagesTable DROP FOREIGN KEY productIdImagePK;
ALTER TABLE ChatTable DROP FOREIGN KEY buyerIdChatPK;
ALTER TABLE ChatTable DROP FOREIGN KEY sellerIdChatPK;
ALTER TABLE ChatTable DROP FOREIGN KEY productIdChatPK;
ALTER TABLE MessageTable DROP FOREIGN KEY chatIdMessagePK;
ALTER TABLE MessageTable DROP FOREIGN KEY senderIdMessagePK;
ALTER TABLE MessageTable DROP FOREIGN KEY recipientIdMessagePK;
ALTER TABLE MessageTable DROP FOREIGN KEY productIdMessagePK;
DROP TABLE UserTable;
DROP TABLE ProductTable;
DROP TABLE ImagesTable;
DROP TABLE OrderTable;
DROP TABLE RatingTable;
DROP TABLE CategoryTable;
DROP TABLE ShoppingCartTable;
DROP TABLE ShoppingCartItemTable;
DROP TABLE OrderItemTable;
DROP TABLE ConditionTable;
DROP TABLE AddressTable;
DROP TABLE ChatTable;
DROP TABLE MessageTable;

----- User

CREATE TABLE UserTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    username VARCHAR(20) NOT NULL UNIQUE,
    firstName VARCHAR(40) NOT NULL,
    lastName VARCHAR(40) NOT NULL,
    email VARCHAR(40) NOT NULL UNIQUE,
    password VARCHAR(100) NOT NULL,
    stripeAccId VARCHAR(100),
    activeTransfers BIT NOT NULL,
    role TINYINT NOT NULL,
    CONSTRAINT userIdPK PRIMARY KEY(id)
) ENGINE = InnoDB;

----- Category

CREATE TABLE CategoryTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT categoryIdPK PRIMARY KEY(id)
) ENGINE = InnoDB;

----- Condition

CREATE TABLE ConditionTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT conditionIdPK PRIMARY KEY(id)
) ENGINE = InnoDB;

----- Product

CREATE TABLE ProductTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    quantity INT NOT NULL,
    description VARCHAR(800) NOT NULL,
    conditionId BIGINT NOT NULL,
    categoryId BIGINT NOT NULL,
    userId BIGINT NOT NULL,
    date DATETIME NOT NULL,
    CONSTRAINT productIdPK PRIMARY KEY(id),
    CONSTRAINT userIdProductPK FOREIGN KEY(userId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT categoryIdProductPK FOREIGN KEY(categoryId) REFERENCES CategoryTable(id) ON DELETE CASCADE,
    CONSTRAINT conditionIdProductPK FOREIGN KEY(conditionId) REFERENCES ConditionTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

------ Images

CREATE TABLE ImagesTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    productId BIGINT NOT NULL,
    name VARCHAR(40) NOT NULL,
    link VARCHAR(200) NOT NULL,
    CONSTRAINT imageIdPK PRIMARY KEY(id),
    CONSTRAINT productIdImagePK FOREIGN KEY(productId) REFERENCES ProductTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- ShoppingCart

CREATE TABLE ShoppingCartTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userId BIGINT NOT NULL,
    CONSTRAINT shoppingCartIdPK PRIMARY KEY(id),
    CONSTRAINT userIdShoppingCartPK FOREIGN KEY(userId) REFERENCES UserTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- ShoppingCartItem

CREATE TABLE ShoppingCartItemTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    shoppingCartId BIGINT NOT NULL,
    productId BIGINT NOT NULL,
    quantity INT NOT NULL,
    CONSTRAINT shoppingCartItemIdPK PRIMARY KEY(id),
    CONSTRAINT shoppingCartIdShoppingCartItemPK FOREIGN KEY(shoppingCartId) REFERENCES ShoppingCartTable(id) ON DELETE CASCADE,
    CONSTRAINT productIdShoppingCartItemPK FOREIGN KEY(productId) REFERENCES ProductTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- Order

CREATE TABLE OrderTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    buyerId BIGINT NOT NULL,
    sellerId BIGINT NOT NULL,
    date DATETIME NOT NULL,
    CONSTRAINT orderId PRIMARY KEY(id),
    CONSTRAINT buyerIdOrderPK FOREIGN KEY(buyerId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT sellerIdOrderPK FOREIGN KEY(sellerId) REFERENCES UserTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- OrderItem

CREATE TABLE OrderItemTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    orderId BIGINT NOT NULL,
    productId BIGINT NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    quantity INT NOT NULL,
    CONSTRAINT orderItemIdPK PRIMARY KEY(id),
    CONSTRAINT orderIdOrderItemPK FOREIGN KEY(orderId) REFERENCES OrderTable(id) ON DELETE CASCADE,
    CONSTRAINT productIdOrderItemPK FOREIGN KEY(productId) REFERENCES ProductTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- Address

CREATE TABLE AddressTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    orderId BIGINT NOT NULL,
    country VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    street VARCHAR(100) NOT NULL,
    postalCode VARCHAR(5) NOT NULL,
    CONSTRAINT addressIdPK PRIMARY KEY(id),
    CONSTRAINT orderIdAddressPK FOREIGN KEY(orderid) REFERENCES OrderTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- Rating

CREATE TABLE RatingTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    buyerId BIGINT NOT NULL,
    sellerId BIGINT NOT NULL,
    orderId BIGINT NOT NULL,
    date DATETIME NOT NULL,
    rate TINYINT NOT NULL,
    comment VARCHAR(800) NOT NULL,
    CONSTRAINT ratingIdPK PRIMARY KEY(id),
    CONSTRAINT buyerIdRatingPK FOREIGN KEY(buyerId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT sellerIdRatingPK FOREIGN KEY(sellerId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT orderIdRatingPK FOREIGN KEY(orderId) REFERENCES OrderTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- Chat

CREATE TABLE ChatTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    buyerId BIGINT NOT NULL,
    sellerId BIGINT NOT NULL,
    productId BIGINT NOT NULL,
    lastModified DATETIME NOT NULL,
    CONSTRAINT chatIdPK PRIMARY KEY(id),
    CONSTRAINT buyerIdChatPK FOREIGN KEY(buyerId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT sellerIdChatPK FOREIGN KEY(sellerId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT productIdChatPK FOREIGN KEY(productId) REFERENCES ProductTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;

----- Message

CREATE TABLE MessageTable (
    id BIGINT NOT NULL AUTO_INCREMENT,
    chatId BIGINT NOT NULL,
    senderId BIGINT NOT NULL,
    recipientId BIGINT NOT NULL,
    productId BIGINT NOT NULL,
    content VARCHAR(300) NOT NULL,
    date DATETIME NOT NULL,
    status ENUM('DELIVERED', 'RECEIVED') NOT NULL,
    CONSTRAINT messageIdPK PRIMARY KEY(id),
    CONSTRAINT chatIdMessagePK FOREIGN KEY(chatId) REFERENCES ChatTable(id) ON DELETE CASCADE,
    CONSTRAINT senderIdMessagePK FOREIGN KEY(senderId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT recipientIdMessagePK FOREIGN KEY(recipientId) REFERENCES UserTable(id) ON DELETE CASCADE,
    CONSTRAINT productIdMessagePK FOREIGN KEY(productId) REFERENCES ProductTable(id) ON DELETE CASCADE
) ENGINE = InnoDB;