----- Spanish conditions

-- INSERT INTO ConditionTable(name) VALUES ('Nuevo');
-- INSERT INTO ConditionTable(name) VALUES ('Como nuevo');
-- INSERT INTO ConditionTable(name) VALUES ('Bueno');
-- INSERT INTO ConditionTable(name) VALUES ('Aceptable');
-- INSERT INTO ConditionTable(name) VALUES ('Roto/No funciona');

----- English conditions

INSERT INTO ConditionTable(name) VALUES ('New');
INSERT INTO ConditionTable(name) VALUES ('Like new');
INSERT INTO ConditionTable(name) VALUES ('Good');
INSERT INTO ConditionTable(name) VALUES ('Acceptable');
INSERT INTO ConditionTable(name) VALUES ('Broken');

----- Spanish categories

-- INSERT INTO CategoryTable(name) VALUES ('Tecnología');
-- INSERT INTO CategoryTable(name) VALUES ('Videojuegos');
-- INSERT INTO CategoryTable(name) VALUES ('Películas');
-- INSERT INTO CategoryTable(name) VALUES ('Moda');
-- INSERT INTO CategoryTable(name) VALUES ('Hogar');
-- INSERT INTO CategoryTable(name) VALUES ('Coleccionables');
-- INSERT INTO CategoryTable(name) VALUES ('Juguetes');
-- INSERT INTO CategoryTable(name) VALUES ('Libros');
-- INSERT INTO CategoryTable(name) VALUES ('Arte');
-- INSERT INTO CategoryTable(name) VALUES ('Música');
-- INSERT INTO CategoryTable(name) VALUES ('Mascotas');

----- English categories

INSERT INTO CategoryTable(name) VALUES ('Tecnology');
INSERT INTO CategoryTable(name) VALUES ('Videogames');
INSERT INTO CategoryTable(name) VALUES ('Movies');
INSERT INTO CategoryTable(name) VALUES ('Fashion');
INSERT INTO CategoryTable(name) VALUES ('Home');
INSERT INTO CategoryTable(name) VALUES ('Collectibles');
INSERT INTO CategoryTable(name) VALUES ('Toys');
INSERT INTO CategoryTable(name) VALUES ('Books');
INSERT INTO CategoryTable(name) VALUES ('Art');
INSERT INTO CategoryTable(name) VALUES ('Music');
INSERT INTO CategoryTable(name) VALUES ('Pets');