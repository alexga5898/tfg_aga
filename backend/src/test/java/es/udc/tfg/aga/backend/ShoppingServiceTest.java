package es.udc.tfg.aga.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.amazonaws.util.IOUtils;
import com.stripe.exception.StripeException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.mock.web.MockMultipartFile;

import es.udc.tfg.aga.backend.model.entities.Address;
import es.udc.tfg.aga.backend.model.entities.CategoryDao;
import es.udc.tfg.aga.backend.model.entities.ConditionDao;
import es.udc.tfg.aga.backend.model.entities.CreateOrdersParams;
import es.udc.tfg.aga.backend.model.entities.Image;
import es.udc.tfg.aga.backend.model.entities.ImageDao;
import es.udc.tfg.aga.backend.model.entities.Order;
import es.udc.tfg.aga.backend.model.entities.OrderDao;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.ProductDao;
import es.udc.tfg.aga.backend.model.entities.Rating;
import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.entities.TransferDataParams;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.exceptions.AlreadyRatedException;
import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.EmptyShoppingCartException;
import es.udc.tfg.aga.backend.model.exceptions.ExistingOrderWithProductException;
import es.udc.tfg.aga.backend.model.exceptions.FileDeleteException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InsufficientStockException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxItemQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxProductQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class ShoppingServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ConditionDao conditionDao;

    @Autowired
    private CategoryDao categoryDao;

    private User createUser(String username, String email) {
        User user = new User(username, "Alex", "Garcia", email, "pass123");
        return user;
    }

    private MultipartFile[] getTestUpdateAndDeleteImages() throws IOException {
        File file = new File("src/test/resources/michi2.jpg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "image/jpeg", IOUtils.toByteArray(input));
        MultipartFile emptyImage = new MockMultipartFile("file", "deleted", "image/jpeg", (byte[]) null);
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        images.add(emptyImage);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    private MultipartFile[] getEmptyTestDeleteImage() throws IOException {
        MultipartFile emptyImage = new MockMultipartFile("file", "deleted", "image/jpeg", (byte[]) null);
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(emptyImage);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    private MultipartFile[] getEmptyTestImage() throws IOException {
        MultipartFile[] images = new MultipartFile[0];
        return images;
    }

    private MultipartFile[] getInvalidFile() throws IOException {
        File file = new File("src/test/resources/prueba.txt");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    private MultipartFile[] getTestImage() throws IOException {
        File file = new File("src/test/resources/michi.jpeg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "image/jpeg", IOUtils.toByteArray(input));
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    private MultipartFile[] getTestImages() throws IOException {
        File file = new File("src/test/resources/michi.jpeg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "image/jpeg", IOUtils.toByteArray(input));
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        images.add(image);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    @Test
    public void testAddProductInexistentUser() throws InstanceNotFoundException {
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.sellProduct(1L, "Portatil",
                new BigDecimal(100), 1, "New", "Tecnology", "Descripcion", getTestImages()));
    }

    @Test
    public void testAddProductUserWithoutStrippAcc() throws InstanceNotFoundException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        assertThrows(StripeAccMissingException.class, () -> shoppingService.sellProduct(user.getId(), "Portatil",
                new BigDecimal(100), 1, "New", "Tecnology", "Descripcion", getTestImages()));
    }

    @Test
    public void testAddProductInvalidCategory() throws InstanceNotFoundException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.sellProduct(user.getId(), "Portatil",
                new BigDecimal(100), 1, "New", "No existe", "Descripcion", getTestImages()));
    }

    @Test
    public void testAddProductInvalidCondition() throws InstanceNotFoundException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.sellProduct(user.getId(), "Portatil",
                new BigDecimal(100), 1, "No existe", "Tecnology", "Descripcion", getTestImages()));
    }

    @Test
    public void testAddProductInvalidImageQuantity() throws DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        assertThrows(InvalidQuantityException.class, () -> shoppingService.sellProduct(user.getId(), "Portatil",
                new BigDecimal(100), 1, "New", "Tecnology", "Descripcion", getEmptyTestImage()));
    }

    @Test
    public void testAddProductInvalidFile() throws DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        assertThrows(InvalidFileException.class, () -> shoppingService.sellProduct(user.getId(), "Portatil",
                new BigDecimal(100), 1, "New", "Tecnology", "Descripcion", getInvalidFile()));
    }

    @Test
    public void testAddProduct() throws InstanceNotFoundException, DuplicateInstanceException, InvalidQuantityException,
            InvalidFileException, IOException, FileUploadException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product sellingProduct = productDao.findByName("portatil").get();
        Image image = imageDao.findByProductId(product.getId()).get(0);
        assertEquals(product, sellingProduct);
        assertEquals(product, image.getProduct());
        // assertEquals(product, user.getProducts().iterator().next());
        assertEquals(image, product.getImages().iterator().next());
    }

    @Test
    public void testUpdateProductNotExistent() {
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.updateProduct(1L, 1L, "name",
                new BigDecimal(1), 1, "New", "Tecnology", "description", getTestImages()));
    }

    @Test
    public void testUpdateProductCategoryNotExistent()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImages());
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.updateProduct(user.getId(), product.getId(),
                "name", new BigDecimal(1), 1, "New", "No existe", "description", getTestImages()));
    }

    @Test
    public void testUpdateProductConditionNotExistent()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImages());
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.updateProduct(user.getId(), product.getId(),
                "name", new BigDecimal(1), 1, "No existe", "Tecnology", "description", getTestImages()));
    }

    @Test
    public void testUpdateProduct()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, PermissionException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImages());
        Product updatedProduct = shoppingService.updateProduct(user.getId(), product.getId(), "new Name",
                new BigDecimal(1), 1, "Good", "Tecnology", "New description", getTestUpdateAndDeleteImages());
        Product sellingProduct = productDao.findByName("new Name").get();
        assertEquals(updatedProduct.getName(), sellingProduct.getName());
        assertEquals(updatedProduct.getPrice(), sellingProduct.getPrice());
        assertEquals(updatedProduct.getQuantity(), sellingProduct.getQuantity());
        assertEquals(updatedProduct.getCondition(), sellingProduct.getCondition());
        assertEquals(updatedProduct.getCategory(), sellingProduct.getCategory());
        assertEquals(updatedProduct.getDescription(), sellingProduct.getDescription());
        assertEquals(updatedProduct.getImages(), sellingProduct.getImages());
        assertEquals(1, updatedProduct.getImages().size());
    }

    @Test
    public void testUpdateInvalidProductImage()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(InvalidFileException.class, () -> shoppingService.updateProduct(user.getId(), product.getId(),
                "new Name", new BigDecimal(1), 1, "Good", "Tecnology", "New description", getInvalidFile()));
    }

    @Test
    public void testUpdateProductPermissionException()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImages());
        assertThrows(PermissionException.class, () -> shoppingService.updateProduct(8L, product.getId(), "new Name",
                new BigDecimal(1), 1, "Good", "Tecnology", "New description", getTestUpdateAndDeleteImages()));
    }

    @Test
    public void testUpdateProductImageInvalidQuantity()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(InvalidQuantityException.class, () -> shoppingService.updateProduct(user.getId(), product.getId(),
                "new Name", new BigDecimal(1), 1, "Good", "Tecnology", "New description", getEmptyTestDeleteImage()));
    }

    @Test
    public void testDeleteProduct()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, PermissionException, FileDeleteException, StripeAccMissingException, ExistingOrderWithProductException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Image image = product.getImages().iterator().next();
        shoppingService.removeProduct(user.getId(), product.getId());
        Optional<Product> sellingProductOpt = productDao.findByName(product.getName());
        Optional<Image> sellingProductImagesOpt = imageDao.findById(image.getId());
        assertEquals(false, sellingProductOpt.isPresent());
        assertEquals(false, sellingProductImagesOpt.isPresent());
    }

    @Test
    public void testDeleteProductNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, PermissionException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New", "Tecnology", "Descripcion",
                getTestImage());
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.removeProduct(user.getId(), 8L));

    }

    @Test
    public void testDeleteProductPermissionException()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, PermissionException, FileDeleteException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());

        assertThrows(PermissionException.class, () -> shoppingService.removeProduct(8L, product.getId()));
    }

    @Test
    public void testDeleteProductOrderWithProductExistent()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, PermissionException, FileDeleteException, StripeAccMissingException, ExistingOrderWithProductException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        shoppingService.buy(user2.getId(), toBuy, address);
        assertThrows(ExistingOrderWithProductException.class, () -> shoppingService.removeProduct(user.getId(), product.getId()));
    }

    @Test
    public void testAddItemCartUserCartOrProductNotFound() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.addItemCart(user2.getId(), 5L, product.getId(), 1));
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), 5L, 1));
    }

    @Test
    public void testAddItemPermissionException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(PermissionException.class,
                () -> shoppingService.addItemCart(user2.getId(), user3.getShoppingCart().getId(), product.getId(), 1));
        assertThrows(PermissionException.class,
                () -> shoppingService.addItemCart(user.getId(), user.getShoppingCart().getId(), product.getId(), 1));
    }

    @Test
    public void testAddItemMaxQuantityException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(MaxProductQuantityException.class,
                () -> shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 6));
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(MaxProductQuantityException.class,
                () -> shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 5));
    }

    @Test // this was tested setting maxitems to 1
    public void testAddMaxItemException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product2 = shoppingService.sellProduct(user.getId(), "Portatil2", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 2);
        assertThrows(MaxItemQuantityException.class,
                () -> shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product2.getId(), 3));
    }

    @Test
    public void testAddItemCart() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, MaxProductQuantityException,
            MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        ShoppingCart shoppingCart = shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(),
                product.getId(), 1);
        assertEquals(product, shoppingCart.getItem(product.getId()).get().getProduct());
    }

    @Test
    public void testAddItemAlreadyExistentCart() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        ShoppingCart shoppingCart = shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(),
                product.getId(), 1);
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertEquals(product, shoppingCart.getItem(product.getId()).get().getProduct());
        assertEquals(2, shoppingCart.getItem(product.getId()).get().getQuantity());
    }

    @Test
    public void testUpdateItemCartUserCartOrProductNotFound() throws DuplicateInstanceException,
            InstanceNotFoundException, FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.updateItemCart(user2.getId(), 5L, product.getId(), 2));
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.updateItemCart(user2.getId(), user2.getShoppingCart().getId(), 5L, 2));
    }

    @Test
    public void testUpdateItemPermissionException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(PermissionException.class, () -> shoppingService.updateItemCart(user2.getId(),
                user3.getShoppingCart().getId(), product.getId(), 2));
        assertThrows(PermissionException.class,
                () -> shoppingService.updateItemCart(user.getId(), user.getShoppingCart().getId(), product.getId(), 2));
    }

    @Test
    public void testUpdateItemMaxQuantityException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(MaxProductQuantityException.class, () -> shoppingService.updateItemCart(user2.getId(),
                user2.getShoppingCart().getId(), product.getId(), 6));
    }

    @Test
    public void testUpdateItemCart() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, MaxProductQuantityException,
            MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        ShoppingCart shoppingCart = shoppingService.updateItemCart(user2.getId(), user2.getShoppingCart().getId(),
                product.getId(), 3);
        assertEquals(3, shoppingCart.getItem(product.getId()).get().getQuantity());
        shoppingService.updateItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 2);
        assertEquals(2, shoppingCart.getItem(product.getId()).get().getQuantity());
    }

    @Test
    public void testDeleteItemCartUserCartOrProductNotFound() throws DuplicateInstanceException,
            InstanceNotFoundException, FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.deleteItemCart(user2.getId(), 5L, product.getId()));
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.deleteItemCart(user2.getId(), user2.getShoppingCart().getId(), 5L));
    }

    @Test
    public void testDeleteItemPermissionException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        assertThrows(PermissionException.class,
                () -> shoppingService.deleteItemCart(user2.getId(), user3.getShoppingCart().getId(), product.getId()));
        assertThrows(PermissionException.class,
                () -> shoppingService.deleteItemCart(user.getId(), user.getShoppingCart().getId(), product.getId()));
    }

    @Test
    public void testRemoveItemCart() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, MaxProductQuantityException,
            MaxItemQuantityException, PermissionException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 1);
        ShoppingCart shoppingCart = shoppingService.deleteItemCart(user2.getId(), user2.getShoppingCart().getId(),
                product.getId());
        assertEquals(Optional.empty(), shoppingCart.getItem(product.getId()));
    }

    @Test
    public void testCheckShoppingCartDoesntExist()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException, EmptyShoppingCartException, StripeException {
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.checkShoppingCartExists(9L));
    }

    @Test
    public void testCheckShoppingCartExists()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, IOException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException, EmptyShoppingCartException, StripeException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        shoppingService.checkShoppingCartExists(user.getShoppingCart().getId());
    }

    @Test
    public void testCheckAndChangeStockProductNotFound() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, EmptyShoppingCartException,
            StripeException, InsufficientStockException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product falseProduct = new Product("Portatil", new BigDecimal(100), 5, conditionDao.findByName("New").get(),
                categoryDao.findByName("Tecnology").get(), "Descripcion", LocalDateTime.now());
        falseProduct.setId(9L);
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(falseProduct, product.getPrice(), 2));
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.checkAndChangeStock(toBuy));
    }

    @Test
    public void testCheckAndChangeStockProductInsufficientStock() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, EmptyShoppingCartException,
            StripeException, InsufficientStockException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 5));
        product.setQuantity(4);
        assertThrows(InsufficientStockException.class, () -> shoppingService.checkAndChangeStock(toBuy));
    }

    @Test
    public void testCheckAndChangeStock() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, EmptyShoppingCartException,
            StripeException, InsufficientStockException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        shoppingService.checkAndChangeStock(toBuy);
        assertEquals(3, product.getQuantity());
    }

    @Test
    public void testBuy() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, MaxProductQuantityException,
            MaxItemQuantityException, PermissionException, EmptyShoppingCartException, StripeException,
            InsufficientStockException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        user3.setStripeAccId("test");
        User user4 = createUser("UserTest4", "EmailTest4@Test.com");
        userService.registerUser(user4);
        user4.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product2 = shoppingService.sellProduct(user3.getId(), "Sofa", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product3 = shoppingService.sellProduct(user3.getId(), "Armario", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product4 = shoppingService.sellProduct(user4.getId(), "Bicicleta", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product2, product2.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product3, product3.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product4, product4.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        shoppingService.buy(user2.getId(), toBuy, address);
        List<Order> orders = orderDao.findByBuyerIdOrderByDateDesc(user2.getId(), PageRequest.of(0, 5)).getContent();
        assertEquals("Portatil", orders.get(0).getItems().iterator().next().getProduct().getName());
        assertEquals(2, orders.get(1).getItems().size());
        assertEquals("Bicicleta", orders.get(2).getItems().iterator().next().getProduct().getName());
    }

    @Test
    public void testGetOrders() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        user3.setStripeAccId("test");
        User user4 = createUser("UserTest4", "EmailTest4@Test.com");
        userService.registerUser(user4);
        user4.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product2 = shoppingService.sellProduct(user3.getId(), "Sofa", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product3 = shoppingService.sellProduct(user3.getId(), "Armario", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product4 = shoppingService.sellProduct(user4.getId(), "Bicicleta", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product2, product2.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product3, product3.getPrice(), 2));
        toBuy.add(new CreateOrdersParams(product4, product4.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        shoppingService.buy(user2.getId(), toBuy, address);
        List<Order> orders = shoppingService.getOrders(user2.getId(), null, null, true, true, 0, 5).getItems();
        assertEquals("Portatil", orders.get(0).getItems().iterator().next().getProduct().getName());
        assertEquals(2, orders.get(1).getItems().size());
        assertEquals("Bicicleta", orders.get(2).getItems().iterator().next().getProduct().getName());
        List<Order> orders2 = shoppingService.getOrders(user2.getId(), null, null, false, true, 0, 5).getItems();
        assertEquals("Bicicleta", orders2.get(2).getItems().iterator().next().getProduct().getName());
        assertEquals(2, orders2.get(1).getItems().size());
        assertEquals("Portatil", orders2.get(0).getItems().iterator().next().getProduct().getName());
        List<Order> sells = shoppingService.getOrders(user3.getId(), null, null, true, false, 0, 5).getItems();
        assertEquals(2, sells.get(0).getItems().size());
    }

    @Test
    public void testGetOrderInexistent() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        assertThrows(InstanceNotFoundException.class, () -> shoppingService.getOrder(1L, 1L));
    }

    @Test
    public void testGetOrderPermissionException() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        User user3 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user3);
        user3.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        shoppingService.addItemCart(user2.getId(), user2.getShoppingCart().getId(), product.getId(), 2);
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        shoppingService.buy(user2.getId(), toBuy, address);
        Order order = orderDao.findByBuyerIdOrderByDateDesc(user2.getId(), PageRequest.of(0, 1)).getContent().get(0);
        assertThrows(PermissionException.class, () -> shoppingService.getOrder(user3.getId(), order.getId()));
    }

    @Test
    public void testGetOrder() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        shoppingService.buy(user2.getId(), toBuy, address);
        Order order = orderDao.findByBuyerIdOrderByDateDesc(user2.getId(), PageRequest.of(0, 1)).getContent().get(0);
        Order getOrder = shoppingService.getOrder(user2.getId(), order.getId());
        Order getSell = shoppingService.getOrder(user.getId(), order.getId());
        assertEquals(order, getOrder);
        assertEquals(order, getSell);
    }

    @Test
    public void testAddRatingOrderNotFound() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        assertThrows(InstanceNotFoundException.class,
                () -> shoppingService.addRating(user.getId(), 8L, 1, "No me ha gustado"));
    }

    @Test
    public void testAddRatingDuplicated() throws DuplicateInstanceException, InstanceNotFoundException,
            FileUploadException, InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, AlreadyRatedException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        List<TransferDataParams> transfers = shoppingService.buy(user2.getId(), toBuy, address);
        shoppingService.addRating(user2.getId(), transfers.get(0).getOrderId(), 1, "No me ha gustado");
        assertThrows(AlreadyRatedException.class,
                () -> shoppingService.addRating(user2.getId(), transfers.get(0).getOrderId(), 1, "No me ha gustado"));
    }

    @Test
    public void testAddRating() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, AlreadyRatedException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        List<TransferDataParams> transfers = shoppingService.buy(user2.getId(), toBuy, address);
        Rating rating = shoppingService.addRating(user2.getId(), transfers.get(0).getOrderId(), 1, "No me ha gustado");
        assertEquals(1, rating.getRate());
    }

    @Test
    public void testGetUserRatings() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, AlreadyRatedException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        List<TransferDataParams> transfers = shoppingService.buy(user2.getId(), toBuy, address);
        shoppingService.addRating(user2.getId(), transfers.get(0).getOrderId(), 1, "No me ha gustado");
        Product product2 = shoppingService.sellProduct(user.getId(), "Armario", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy2 = new ArrayList<>();
        toBuy2.add(new CreateOrdersParams(product2, product2.getPrice(), 2));
        List<TransferDataParams> transfers2 = shoppingService.buy(user2.getId(), toBuy, address);
        shoppingService.addRating(user2.getId(), transfers2.get(0).getOrderId(), 5, "Me ha encantado");
        List<Rating> ratings = shoppingService.getUserRatings(user.getId(), 0, 10).getRatings().getItems();
        assertEquals(2, ratings.size());
    }

    @Test
    public void testGetRatings() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException, AlreadyRatedException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        user2.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy = new ArrayList<>();
        toBuy.add(new CreateOrdersParams(product, product.getPrice(), 2));
        Address address = new Address("España", "A Coruña", "Calle de prueba", "15000");
        List<TransferDataParams> transfers = shoppingService.buy(user2.getId(), toBuy, address);
        shoppingService.addRating(user2.getId(), transfers.get(0).getOrderId(), 1, "No me ha gustado");
        Product product2 = shoppingService.sellProduct(user.getId(), "Armario", new BigDecimal(100), 5, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<CreateOrdersParams> toBuy2 = new ArrayList<>();
        toBuy2.add(new CreateOrdersParams(product2, product2.getPrice(), 2));
        List<TransferDataParams> transfers2 = shoppingService.buy(user2.getId(), toBuy, address);
        shoppingService.addRating(user2.getId(), transfers2.get(0).getOrderId(), 5, "Me ha encantado");
        List<Rating> ratings = shoppingService.getRatings(user.getUsername(), 0, 10).getRatings().getItems();
        assertEquals(2, ratings.size());
    }
}
