package es.udc.tfg.aga.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import com.amazonaws.util.IOUtils;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;

import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.FilesDownloadException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;
import es.udc.tfg.aga.backend.model.model.CatalogService;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CatalogServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    private CatalogService catalogService;

    private User createUser(String username, String email) {
        User user = new User(username, "Alex", "Garcia", email, "pass123");
        return user;
    }

    private MultipartFile[] getTestImage() throws IOException {
        File file = new File("src/test/resources/michi.jpeg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "image/jpeg", IOUtils.toByteArray(input));
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    @Test
    void testGetUserProducts() throws DuplicateInstanceException, InstanceNotFoundException, InvalidQuantityException,
            InvalidFileException, FileUploadException, IOException, FilesDownloadException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product2 = shoppingService.sellProduct(user.getId(), "Camara", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<Product> products = catalogService.getUserProducts(user.getId(), null, 0, 2).getItems();
        assertEquals(2, products.size());
        assertEquals(products.contains(product), true);
        assertEquals(products.contains(product2), true);
    }

    @Test
    void testGetUserProductsFiltered()
            throws DuplicateInstanceException, InstanceNotFoundException, InvalidQuantityException,
            InvalidFileException, FileUploadException, IOException, FilesDownloadException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        List<Product> products = catalogService.getUserProducts(user.getId(), "Portatil", 0, 2).getItems();
        assertEquals(1, products.size());
        assertEquals(products.contains(product), true);
    }

    @Test
    public void testGetInexistentProduct() {
        assertThrows(InstanceNotFoundException.class, () -> catalogService.getProduct(1L));
    }

    @Test
    public void testGetProduct() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");
        Product product = shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product storedProduct = catalogService.getProduct(product.getId());
        assertEquals(product, storedProduct);
    }

    @Test
    public void testGetProductsInexistentCondition() throws InstanceNotFoundException {
        assertThrows(InstanceNotFoundException.class,
                () -> catalogService.getProducts("", "", "inexistent", null, null, 0, 0, 5).getItems());
    }

    @Test
    public void testGetProductsInexistentCategory() throws InstanceNotFoundException {
        assertThrows(InstanceNotFoundException.class,
                () -> catalogService.getProducts("", "inexistent", "", null, null, 0, 0, 5).getItems());
    }

    @Test
    public void testGetProducts() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");

        shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New", "Tecnology", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Muñeco", new BigDecimal(5.50), 1, "Broken", "Toys", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Camisa", new BigDecimal(10), 1, "Good", "Fashion", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Armario", new BigDecimal(300), 1, "Like new", "Home", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Moneda antigua", new BigDecimal(50), 1, "Good", "Collectibles",
                "Descripcion", getTestImage());

        List<Product> allProducts = catalogService.getProducts("", "", "", null, null, 0, 0, 5).getItems();
        List<Product> goodConditionProducts = catalogService.getProducts("", "", "good", null, null, 0, 0, 5)
                .getItems();
        List<Product> betweenPricesProducts = catalogService
                .getProducts("", "", "", new BigDecimal(5), new BigDecimal(50), 0, 0, 5).getItems();
        List<Product> goodConditionProductName = catalogService.getProducts("camisa", "", "good", null, null, 0, 0, 5)
                .getItems();
        List<Product> homeProducts = catalogService.getProducts("", "home", "", null, null, 0, 0, 5).getItems();

        assertEquals(5, allProducts.size());
        assertEquals(2, goodConditionProducts.size());
        assertEquals(3, betweenPricesProducts.size());
        assertEquals(1, goodConditionProductName.size());
        assertEquals(1, homeProducts.size());

    }

    @Test
    public void testGetProductsByUsername() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, IOException, StripeAccMissingException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        user.setStripeAccId("test");

        shoppingService.sellProduct(user.getId(), "Portatil", new BigDecimal(100), 1, "New", "Tecnology", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Muñeco", new BigDecimal(5.50), 1, "Broken", "Toys", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Camisa", new BigDecimal(10), 1, "Good", "Fashion", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Armario", new BigDecimal(300), 1, "Like new", "Home", "Descripcion",
                getTestImage());
        shoppingService.sellProduct(user.getId(), "Moneda antigua", new BigDecimal(50), 1, "Good", "Collectibles",
                "Descripcion", getTestImage());

        List<Product> allProducts = catalogService.getProductsByUsername(user.getUsername(), 0, 10).getItems();

        assertEquals(5, allProducts.size());

    }


}
