package es.udc.tfg.aga.backend;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import com.amazonaws.util.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.mock.web.MockMultipartFile;

import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Chat;
import es.udc.tfg.aga.backend.model.entities.ChatDao;
import es.udc.tfg.aga.backend.model.entities.Message;
import es.udc.tfg.aga.backend.model.entities.MessageStatus;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.entities.UserDao;
import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.IncorrectLogInException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ShoppingService shoppingService;

    @Autowired
    ChatDao chatDao;

    private User createUser(String username, String email) {
        User user = new User(username, "Alex", "Garcia", email, "pass123");
        return user;
    }

    private MultipartFile[] getTestImage() throws IOException {
        File file = new File("src/test/resources/michi.jpeg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("file", file.getName(), "image/jpeg", IOUtils.toByteArray(input));
        ArrayList<MultipartFile> images = new ArrayList<MultipartFile>();
        images.add(image);
        return Arrays.copyOf(images.toArray(), images.size(), MultipartFile[].class);
    }

    @Test
    public void testRegisterUser() throws DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        assertEquals(user, userDao.findById(user.getId()).get());
    }

    @Test
    public void testDuplicatedRegisterUser() throws DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        User user2 = createUser("UserTest", "EmailTest2@Test.com");
        User user3 = createUser("UserTest2", "EmailTest3@Test.com");
        User user4 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(user);
        userService.registerUser(user3);
        assertThrows(DuplicateInstanceException.class, () -> userService.registerUser(user2));
        assertThrows(DuplicateInstanceException.class, () -> userService.registerUser(user4));
    }

    @Test
    public void testIncorrectLogIn()
            throws InstanceNotFoundException, IncorrectLogInException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        assertThrows(IncorrectLogInException.class, () -> userService.logIn("EmailTest@Test.com", "incorrect"));
        assertThrows(IncorrectLogInException.class, () -> userService.logIn("Israel@email.com", "State"));
    }

    @Test
    public void testLogIn() throws InstanceNotFoundException, IncorrectLogInException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        User loggedIn = userService.logIn("EmailTest@Test.com", "pass123");
        assertEquals(user, loggedIn);
    }

    @Test
    public void testLogInFromInexistentUserId() throws InstanceNotFoundException {
        assertThrows(InstanceNotFoundException.class, () -> userService.logInFromUserId(Long.valueOf(1)));
    }

    @Test
    public void testLogInFromUserId() throws InstanceNotFoundException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        User registeredUser = userDao.findByUsername("UserTest").get();
        User loggedIn = userService.logInFromUserId(registeredUser.getId());
        assertEquals(registeredUser, loggedIn);
    }

    @Test
    public void testUpdateUserInexistentProfile() throws InstanceNotFoundException {
        assertThrows(InstanceNotFoundException.class,
                () -> userService.updateProfile(1L, "username", "new@email", "first name", "lastname"));
    }

    @Test
    public void testUpdateUserProfileDuplicatedInstance() throws InstanceNotFoundException, DuplicateInstanceException {
        User user = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(user);
        User user2 = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(user2);
        assertThrows(DuplicateInstanceException.class, () -> userService.updateProfile(user.getId(),
                user2.getUsername(), "new@email", user.getFirstName(), "new lastname"));
        assertThrows(DuplicateInstanceException.class, () -> userService.updateProfile(user.getId(), user.getUsername(),
                user2.getEmail(), user.getFirstName(), "new lastname"));
    }

    @Test
    public void testSendMessageInstanceNotFound()
            throws InstanceNotFoundException, DuplicateInstanceException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        assertThrows(InstanceNotFoundException.class,
                () -> userService.sendMessage(29L, seller.getId(), product.getId(), "Mensaje2"));
        assertThrows(InstanceNotFoundException.class,
                () -> userService.sendMessage(buyer.getId(), 229035L, product.getId(), "Mensaje2"));
        assertThrows(InstanceNotFoundException.class,
                () -> userService.sendMessage(buyer.getId(), seller.getId(), 29L, "Mensaje2"));
    }

    @Test
    public void testSendMessage() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Product product2 = shoppingService.sellProduct(seller.getId(), "Armario", new BigDecimal(100), 1, "New", "Home",
                "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        userService.sendMessage(buyer.getId(), seller.getId(), product2.getId(), "Mensaje2");
        Chat chat = chatDao.findChat(buyer.getId(), seller.getId(), product.getId()).get();
        assertEquals("Mensaje", message.getContent());
        assertEquals(chat, message.getChat());
        assertEquals(2, chatDao.findAll(PageRequest.of(0, 10)).getNumberOfElements());
    }

    @Test
    public void testChangeMessageStatusInstanceNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(InstanceNotFoundException.class, () -> userService.changeMessagesStatus(seller.getId(), 29L));
    }

    @Test
    public void testChangeMessageStatusPermission()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        User random = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(random);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(PermissionException.class,
                () -> userService.changeMessagesStatus(random.getId(), message.getChat().getId()));
    }

    @Test
    public void testChangeMessageStatus()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        userService.changeMessagesStatus(seller.getId(), message.getChat().getId());
        assertEquals(MessageStatus.RECEIVED, message.getStatus());
    }

    @Test
    public void testCheckNewMessagesInstanceNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(InstanceNotFoundException.class, () -> userService.checkNewMessages(seller.getId(), 29L));
    }

    @Test
    public void testCheckNewMessagesPermission()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        User random = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(random);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(PermissionException.class,
                () -> userService.checkNewMessages(random.getId(), message.getChat().getId()));
    }

    @Test
    public void testCheckNewMessages()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        Boolean newMessages = userService.checkNewMessages(seller.getId(), message.getChat().getId());
        userService.changeMessagesStatus(seller.getId(), message.getChat().getId());
        Boolean newMessages2 = userService.checkNewMessages(seller.getId(), message.getChat().getId());
        assertEquals(true, newMessages);
        assertEquals(false, newMessages2);
    }

    @Test
    public void testCheckNewMessagesGlobalNotFound() {
        assertThrows(InstanceNotFoundException.class, () -> userService.checkNewMessagesGlobal(5843L));
    }

    @Test
    public void testCheckNewMessagesGlobal()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        Boolean newMessages = userService.checkNewMessagesGlobal(seller.getId());
        userService.changeMessagesStatus(seller.getId(), message.getChat().getId());
        Boolean newMessages2 = userService.checkNewMessagesGlobal(seller.getId());
        assertEquals(true, newMessages);
        assertEquals(false, newMessages2);
    }

    @Test
    public void testGetChats() throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException,
            InvalidQuantityException, InvalidFileException, StripeAccMissingException, IOException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        User buyer2 = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(buyer2);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        userService.sendMessage(buyer2.getId(), seller.getId(), product.getId(), "Mensaje");
        Block<Chat> chats = userService.getChats(seller.getId(), 0, 10);
        assertEquals(2, chats.getTotalItems());
    }

    @Test
    public void testGetChatIfExistentInstanceNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(InstanceNotFoundException.class,
                () -> userService.getChatIfExistent(1000L, seller.getId(), product.getId()));
        assertThrows(InstanceNotFoundException.class,
                () -> userService.getChatIfExistent(buyer.getId(), 1000L, product.getId()));
        assertThrows(InstanceNotFoundException.class,
                () -> userService.getChatIfExistent(buyer.getId(), seller.getId(), 1000L));

    }

    @Test
    public void testGetChatIfExistent()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        Chat chat = userService.getChatIfExistent(buyer.getId(), seller.getId(), product.getId());
        assertEquals(message.getChat(), chat);
    }

    @Test
    public void testGetChatInstanceNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(InstanceNotFoundException.class, () -> userService.getChat(buyer.getId(), 300L));
    }

    @Test
    public void testGetChatPermission()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        User random = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(random);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(PermissionException.class, () -> userService.getChat(random.getId(), message.getChat().getId()));
    }

    @Test
    public void testGetChat()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertEquals(message.getChat(), userService.getChat(buyer.getId(), message.getChat().getId()));
    }

    @Test
    public void testGetMessagesNotFound()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(InstanceNotFoundException.class, () -> userService.getMessages(buyer.getId(), 300L, 0, 10));
    }

    @Test
    public void testGetMessagesPermission()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        User random = createUser("UserTest3", "EmailTest3@Test.com");
        userService.registerUser(random);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertThrows(PermissionException.class,
                () -> userService.getMessages(random.getId(), message.getChat().getId(), 0, 10));
    }

    @Test
    public void testGetMessages()
            throws DuplicateInstanceException, InstanceNotFoundException, FileUploadException, InvalidQuantityException,
            InvalidFileException, StripeAccMissingException, IOException, PermissionException {
        User buyer = createUser("UserTest", "EmailTest@Test.com");
        userService.registerUser(buyer);
        User seller = createUser("UserTest2", "EmailTest2@Test.com");
        userService.registerUser(seller);
        seller.setStripeAccId("test");
        Product product = shoppingService.sellProduct(seller.getId(), "Portatil", new BigDecimal(100), 1, "New",
                "Tecnology", "Descripcion", getTestImage());
        Message message = userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        userService.sendMessage(seller.getId(), buyer.getId(), product.getId(), "Mensaje");
        userService.sendMessage(buyer.getId(), seller.getId(), product.getId(), "Mensaje");
        assertEquals(4, userService.getMessages(buyer.getId(), message.getChat().getId(), 0, 10).getItems().size());

    }

}
