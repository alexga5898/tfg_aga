package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.udc.tfg.aga.backend.model.exceptions.MaxItemQuantityException;

@Entity
@Table(name = "ShoppingCartTable")
public class ShoppingCart {

    public static final int MAX_ITEMS = 20;

    private Long id;
    private User user;
    private Set<ShoppingCartItem> items = new HashSet<>();

    public ShoppingCart() {

    }

    public ShoppingCart(User user) {
        this.user = user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(mappedBy = "shoppingCart")
    public Set<ShoppingCartItem> getItems() {
        return this.items;
    }

    public void setItems(Set<ShoppingCartItem> items) {
        this.items = items;
    }

    @Transient
    public Optional<ShoppingCartItem> getItem(Long productId) {
        return items.stream().filter(item -> item.getProduct().getId().equals(productId)).findFirst();
    }

    public void addItem(ShoppingCartItem shoppingCartItem) throws MaxItemQuantityException {
        if (items.size() == MAX_ITEMS) {
            throw new MaxItemQuantityException();
        }
        items.add(shoppingCartItem);
        shoppingCartItem.setShoppingCart(this);
    }

    @Transient
    public int getTotalQuantity() {
        return items.stream().map(i -> i.getQuantity()).reduce(0, (a, b) -> a + b);
    }

    @Transient
    public BigDecimal getTotalPrice() {
        return items.stream().map(i -> i.getTotalPrice()).reduce(new BigDecimal(0), (a, b) -> a.add(b));
    }

    public void removeItem(ShoppingCartItem shoppingCartItem) {
        items.remove(shoppingCartItem);
    }

    public void removeAll() {
        items = new HashSet<>();
    }
}
