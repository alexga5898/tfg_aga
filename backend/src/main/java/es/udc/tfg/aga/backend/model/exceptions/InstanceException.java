package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class InstanceException extends Exception{
    private String name;
    private Object object;

    public InstanceException(String message){
        super(message);
    }

    public InstanceException(String name, Object object){
        this.name = name;
        this.object = object;
    }

    public String getName(){
        return name;
    }

    public Object getObject(){
        return object;
    }
}
