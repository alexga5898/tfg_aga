package es.udc.tfg.aga.backend.rest.controllers;

import java.math.BigDecimal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.exceptions.FilesDownloadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.model.CatalogService;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;
import es.udc.tfg.aga.backend.rest.dtos.ErrorsDto;
import es.udc.tfg.aga.backend.rest.dtos.PageDto;
import es.udc.tfg.aga.backend.rest.dtos.ProductConversor;
import es.udc.tfg.aga.backend.rest.dtos.ProductDto;
import es.udc.tfg.aga.backend.rest.dtos.ProductDetailsDto;

@RestController
@RequestMapping("/products")
@Validated
public class CatalogController {

    @Autowired
    ShoppingService shoppingService;

    @Autowired
    UserService userService;

    @Autowired
    CatalogService catalogService;

    @Autowired
    private MessageSource messageSource;

    private String FILES_DOWNLOAD_EXCEPTION = "exceptions.FileDownloadException";

    @ExceptionHandler(FilesDownloadException.class)
    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ResponseBody
    public ErrorsDto handleFilesUploadException(FilesDownloadException e, Locale locale) {
        String message = messageSource.getMessage(FILES_DOWNLOAD_EXCEPTION, new Object[] { e.getFileName() },
                FILES_DOWNLOAD_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @GetMapping("/myProducts")
    public PageDto<ProductDetailsDto> getUserProducts(@RequestAttribute Long userId,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "keywords", defaultValue = "") String keywords) {

        Block<Product> pageBlock = catalogService.getUserProducts(userId, keywords, page, 10);

        return new PageDto<ProductDetailsDto>(ProductConversor.toProductDetailsDtos(pageBlock.getItems()), pageBlock.getTotalItems(),
                pageBlock.getTotalPages(), pageBlock.getExistMoreItems());
    }

    @GetMapping("/images/{imageId}")
    public byte[] getProductImage(@PathVariable Long imageId) throws InstanceNotFoundException, FilesDownloadException {

        byte[] image = catalogService.getImage(imageId);

        return image;
    }

    @GetMapping("/{productId}/thumbnail")
    public byte[] getProductThumbnail(@PathVariable Long productId)
            throws InstanceNotFoundException, FilesDownloadException {

        byte[] image = catalogService.getThumbnail(productId);

        return image;
    }

    @GetMapping("/{productId}")
    public ProductDto getProduct(@PathVariable Long productId) throws InstanceNotFoundException {

        Product product = catalogService.getProduct(productId);

        return ProductConversor.toProductDto(product);
    }

    @GetMapping("")
    public PageDto<ProductDetailsDto> getProducts(@RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "keywords", defaultValue = "") String keywords,
            @RequestParam(name = "category", defaultValue = "") String category,
            @RequestParam(name = "condition", defaultValue = "") String condition,
            @RequestParam(name = "lPrice", defaultValue = "0") BigDecimal lPrice,
            @RequestParam(name = "hPrice", defaultValue = "500000") BigDecimal hPrice,
            @RequestParam(name = "order", defaultValue = "0") int order) throws InstanceNotFoundException {

        Block<Product> pageBlock = catalogService.getProducts(keywords, category, condition, lPrice, hPrice, order,
                page, 15);

        return new PageDto<ProductDetailsDto>(ProductConversor.toProductDetailsDtos(pageBlock.getItems()), pageBlock.getTotalItems(),
                pageBlock.getTotalPages(), pageBlock.getExistMoreItems());
    }

    @GetMapping("/user/{username}")
    public PageDto<ProductDetailsDto> getProductsByUsername(@PathVariable String username,
            @RequestParam(name = "page", defaultValue = "0") int page) {

        Block<Product> pageBlock = catalogService.getProductsByUsername(username, page, 15);

        return new PageDto<ProductDetailsDto>(ProductConversor.toProductDetailsDtos(pageBlock.getItems()), pageBlock.getTotalItems(),
                pageBlock.getTotalPages(), pageBlock.getExistMoreItems());
    }

}
