package es.udc.tfg.aga.backend.model.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import es.udc.tfg.aga.backend.model.amazon.FileStore;
import es.udc.tfg.aga.backend.model.entities.Address;
import es.udc.tfg.aga.backend.model.entities.AddressDao;
import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Category;
import es.udc.tfg.aga.backend.model.entities.CategoryDao;
import es.udc.tfg.aga.backend.model.entities.Condition;
import es.udc.tfg.aga.backend.model.entities.ConditionDao;
import es.udc.tfg.aga.backend.model.entities.CreateOrdersParams;
import es.udc.tfg.aga.backend.model.entities.Image;
import es.udc.tfg.aga.backend.model.entities.ImageDao;
import es.udc.tfg.aga.backend.model.entities.Order;
import es.udc.tfg.aga.backend.model.entities.OrderDao;
import es.udc.tfg.aga.backend.model.entities.OrderItem;
import es.udc.tfg.aga.backend.model.entities.OrderItemDao;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.ProductDao;
import es.udc.tfg.aga.backend.model.entities.Rating;
import es.udc.tfg.aga.backend.model.entities.RatingBlock;
import es.udc.tfg.aga.backend.model.entities.RatingDao;
import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.entities.ShoppingCartDao;
import es.udc.tfg.aga.backend.model.entities.ShoppingCartItem;
import es.udc.tfg.aga.backend.model.entities.ShoppingCartItemDao;
import es.udc.tfg.aga.backend.model.entities.TransferDataParams;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.entities.UserDao;
import es.udc.tfg.aga.backend.model.exceptions.AlreadyRatedException;
import es.udc.tfg.aga.backend.model.exceptions.ExistingOrderWithProductException;
import es.udc.tfg.aga.backend.model.exceptions.FileDeleteException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InsufficientStockException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxItemQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxProductQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;

import static org.apache.http.entity.ContentType.*;

@Service
@Transactional
public class ShoppingServiceImpl implements ShoppingService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ConditionDao conditionDao;

    @Autowired
    private ShoppingCartDao shoppingCartDao;

    @Autowired
    private ShoppingCartItemDao shoppingCartItemDao;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private AddressDao addressDao;

    @Autowired
    private RatingDao ratingDao;

    @Autowired
    private FileStore fileStore;

    @Value("${my.aws.s3.bucket}")
    private String bucketName;

    private static final String USERID_FIELD = "user.fields.userId";
    private static final String CATEGORY_FIELD = "product.fields.category";
    private static final String CONDITION_FIELD = "product.fields.condition";
    private static final String IMAGES_FIELD = "product.fields.images";
    private static final String PRODUCTID_FIELD = "product.fields.productId";
    private static final String SHOPPINGCARTID_FIELD = "product.fields.shoppingCartId";
    private static final String ORDERID_FIELD = "order.fields.orderId";
    private static final int MIN_IMAGES = 1;
    private static final int MAX_IMAGES = 5;
    // private static final List<String> ADMITTED_CONTENT_TYPE = new
    // ArrayList<String>(
    // Arrays.asList("image/png", "image/jpeg", "image/jpg"));

    @Override
    public Product sellProduct(Long userId, String name, BigDecimal price, int quantity, String condition,
            String category, String description, MultipartFile[] images) throws InstanceNotFoundException,
            InvalidQuantityException, InvalidFileException, FileUploadException, StripeAccMissingException {
        Optional<User> userOpt = userDao.findById(userId);
        if (!userOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        User user = userOpt.get();
        if (user.getStripeAccId() == null) {
            throw new StripeAccMissingException();
        }
        if (!(categoryDao.findByName(category)).isPresent()) {
            throw new InstanceNotFoundException(CATEGORY_FIELD, category);
        }
        if (!conditionDao.findByName(condition).isPresent()) {
            throw new InstanceNotFoundException(CONDITION_FIELD, condition);
        }
        if (images.length < MIN_IMAGES || images.length > MAX_IMAGES) {
            throw new InvalidQuantityException(IMAGES_FIELD, MIN_IMAGES, MAX_IMAGES);
        }
        for (MultipartFile errorFileCheck : images) {
            if (errorFileCheck.getSize() == -1 || errorFileCheck.getContentType() == null) {
                throw new InvalidQuantityException(IMAGES_FIELD, MIN_IMAGES, MAX_IMAGES);
            }
            if (!Arrays.asList(IMAGE_JPEG.getMimeType(), IMAGE_PNG.getMimeType())
                    .contains(errorFileCheck.getContentType())) {
                throw new InvalidFileException(errorFileCheck.getContentType());
            }
        }
        Category categoryObj = categoryDao.findByName(category).get();
        Condition conditionObj = conditionDao.findByName(condition).get();
        Product product = new Product(name, price, quantity, conditionObj, categoryObj, description,
                LocalDateTime.now());
        product.setUser(user);
        productDao.save(product);
        // user.addProduct(product);
        storeImages(userId, product, images);
        return product;
    }

    @Override
    public Product updateProduct(Long userId, Long productId, String name, BigDecimal price, int quantity,
            String condition, String category, String description, MultipartFile[] images)
            throws InstanceNotFoundException, PermissionException, FileUploadException, FileDeleteException,
            InvalidQuantityException, InvalidFileException {
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();
        if (!product.getUser().getId().equals(userId)) {
            throw new PermissionException();
        }
        Optional<Category> categoryOpt = categoryDao.findByName(category);
        if (!categoryOpt.isPresent()) {
            throw new InstanceNotFoundException(CATEGORY_FIELD, category);
        }
        Optional<Condition> conditionOpt = conditionDao.findByName(condition);
        if (!conditionOpt.isPresent()) {
            throw new InstanceNotFoundException(CONDITION_FIELD, condition);
        }
        updateImages(product, images);
        product.setName(name);
        product.setPrice(price);
        product.setQuantity(quantity);
        product.setCondition(conditionOpt.get());
        product.setCategory(categoryOpt.get());
        product.setDescription(description);
        return product;
    }

    @Override
    public void removeProduct(Long userId, Long productId) throws InstanceNotFoundException, FileDeleteException,
            PermissionException, ExistingOrderWithProductException {
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();
        if (!product.getUser().getId().equals(userId)) {
            throw new PermissionException();
        }
        if (orderItemDao.existsByProductId(productId)) {
            throw new ExistingOrderWithProductException();
        }
        deleteAllImages(product);
    }

    @Override
    public ShoppingCart addItemCart(Long userId, Long shoppingCartId, Long productId, int quantity)
            throws InstanceNotFoundException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException {
        Optional<ShoppingCart> shoppingCartOpt = shoppingCartDao.findById(shoppingCartId);
        if (!shoppingCartOpt.isPresent()) {
            throw new InstanceNotFoundException(SHOPPINGCARTID_FIELD, shoppingCartId);
        }
        ShoppingCart shoppingCart = shoppingCartOpt.get();
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();
        if (!shoppingCart.getUser().getId().equals(userId) || userId.equals(product.getUser().getId())) {
            throw new PermissionException();
        }

        Optional<ShoppingCartItem> shoppingCartItemOpt = shoppingCart.getItem(productId);
        if (shoppingCartItemOpt.isPresent()) {
            shoppingCartItemOpt.get().incrementQuantity(quantity, product.getQuantity());
        } else {
            if (quantity > product.getQuantity()) {
                throw new MaxProductQuantityException();
            }
            ShoppingCartItem shoppingCartItem = new ShoppingCartItem(shoppingCart, product, quantity);
            shoppingCart.addItem(shoppingCartItem);
            shoppingCartItemDao.save(shoppingCartItem);
        }

        return shoppingCart;

    }

    @Override
    public ShoppingCart updateItemCart(Long userId, Long shoppingCartId, Long productId, int quantity)
            throws InstanceNotFoundException, MaxProductQuantityException, PermissionException {
        Optional<ShoppingCart> shoppingCartOpt = shoppingCartDao.findById(shoppingCartId);
        if (!shoppingCartOpt.isPresent()) {
            throw new InstanceNotFoundException(SHOPPINGCARTID_FIELD, shoppingCartId);
        }
        ShoppingCart shoppingCart = shoppingCartOpt.get();
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();
        if (!shoppingCart.getUser().getId().equals(userId) || userId.equals(product.getUser().getId())) {
            throw new PermissionException();
        }

        Optional<ShoppingCartItem> shoppingCartItemOpt = shoppingCart.getItem(productId);
        if (!shoppingCartItemOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }

        shoppingCartItemOpt.get().updateQuantity(quantity, product.getQuantity());

        return shoppingCart;

    }

    @Override
    public ShoppingCart deleteItemCart(Long userId, Long shoppingCartId, Long productId)
            throws InstanceNotFoundException, PermissionException {
        Optional<ShoppingCart> shoppingCartOpt = shoppingCartDao.findById(shoppingCartId);
        if (!shoppingCartOpt.isPresent()) {
            throw new InstanceNotFoundException(SHOPPINGCARTID_FIELD, shoppingCartId);
        }
        ShoppingCart shoppingCart = shoppingCartOpt.get();
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();
        if (!shoppingCart.getUser().getId().equals(userId) || userId.equals(product.getUser().getId())) {
            throw new PermissionException();
        }

        Optional<ShoppingCartItem> shoppingCartItemOpt = shoppingCart.getItem(productId);
        if (!shoppingCartItemOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        shoppingCart.removeItem(shoppingCartItemOpt.get());
        shoppingCartItemDao.delete(shoppingCartItemOpt.get());

        return shoppingCart;
    }

    @Override
    public void checkShoppingCartExists(Long shoppingCartId) throws InstanceNotFoundException {
        if (!shoppingCartDao.existsById(shoppingCartId)) {
            throw new InstanceNotFoundException(SHOPPINGCARTID_FIELD, shoppingCartId);
        }
    }

    @Override
    public void checkAndChangeStock(List<CreateOrdersParams> createOrdersParams)
            throws InstanceNotFoundException, InsufficientStockException {
        for (CreateOrdersParams params : createOrdersParams) {
            Optional<Product> productOpt = productDao.findById(params.getProduct().getId());
            if (!productOpt.isPresent()) {
                throw new InstanceNotFoundException(PRODUCTID_FIELD, params.getProduct().getId());
            }
            if (productOpt.get().getQuantity() < params.getQuantity()) {
                throw new InsufficientStockException();
            }
        }
        for (CreateOrdersParams params : createOrdersParams) {
            params.getProduct().decrementQuantity(params.getQuantity());
        }

    }

    @Override
    public List<TransferDataParams> buy(Long userId, List<CreateOrdersParams> createOrdersParams, Address address) {
        User user = userDao.findById(userId).get();

        // Create the first order
        Order order = new Order(user, createOrdersParams.get(0).getProduct().getUser(), LocalDateTime.now(), address);
        order.setAddress(address);
        address.setOrder(order);
        orderDao.save(order);
        addressDao.save(address);

        // Create the orders list and add an item to the first order
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        OrderItem orderItem = new OrderItem(orders.get(0), createOrdersParams.get(0).getProduct(),
                createOrdersParams.get(0).getQuantity(), createOrdersParams.get(0).getPrice());
        orderItemDao.save(orderItem);
        orders.get(0).addItem(orderItem);

        // Iterate through shoppingCartItems, adding them to an existing order or
        // creating a new one when the seller changes
        int index = 0;
        for (int i = 1; i < createOrdersParams.size(); i++) {
            if (createOrdersParams.get(i).getProduct().getUser().getId()
                    .equals(createOrdersParams.get(i - 1).getProduct().getUser().getId())) {
                OrderItem item = new OrderItem(orders.get(index), createOrdersParams.get(i).getProduct(),
                        createOrdersParams.get(i).getQuantity(), createOrdersParams.get(i).getPrice());
                orderItemDao.save(item);
                orders.get(index).addItem(item);
            } else {
                Order newOrder = new Order(user, createOrdersParams.get(i).getProduct().getUser(), LocalDateTime.now(),
                        address);
                Address addressCopy = new Address(address.getCountry(), address.getCity(), address.getStreet(),
                        address.getPostalCode());
                newOrder.setAddress(addressCopy);
                addressCopy.setOrder(newOrder);
                orderDao.save(newOrder);
                addressDao.save(addressCopy);
                orders.add(newOrder);
                index += 1;
                OrderItem orderItemDifferentSeller = new OrderItem(orders.get(index),
                        createOrdersParams.get(i).getProduct(), createOrdersParams.get(i).getQuantity(),
                        createOrdersParams.get(i).getPrice());
                orderItemDao.save(orderItemDifferentSeller);
                orders.get(index).addItem(orderItemDifferentSeller);
            }
        }

        // Return the stripeAccId for each vendor and the total in order to create one
        // transfer for each one and add orders to a transaction
        List<TransferDataParams> transfers = new ArrayList<>();
        for (Order orderTransfer : orders) {
            transfers.add(new TransferDataParams(orderTransfer.getSeller().getStripeAccId(),
                    orderTransfer.getTotalPrice(), orderTransfer.getId(), orderTransfer.getSeller().getEmail()));
        }

        shoppingCartItemDao.deleteByShoppingCartId(user.getShoppingCart().getId());
        user.getShoppingCart().removeAll();

        return transfers;

    }

    @Override
    @Transactional(readOnly = true)
    public Block<Order> getOrders(Long userId, Integer month, Integer year, Boolean asc, Boolean buyer, int page,
            int size) {
        Page<Order> orders = orderDao.findOrders(userId, month, year, asc, buyer, page, size);
        return new Block<Order>(orders.getContent(), orders.getTotalElements(), orders.getTotalPages(),
                orders.hasNext());
    }

    @Override
    @Transactional(readOnly = true)
    public Order getOrder(Long userId, Long orderId) throws InstanceNotFoundException, PermissionException {
        Optional<Order> orderOpt = orderDao.findById(orderId);
        if (!orderOpt.isPresent()) {
            throw new InstanceNotFoundException(ORDERID_FIELD, orderId);
        }
        Order order = orderOpt.get();
        if (!order.getBuyer().getId().equals(userId) && !order.getSeller().getId().equals(userId)) {
            throw new PermissionException();
        }
        return order;
    }

    @Override
    public Rating addRating(Long userId, Long orderId, int rate, String comment)
            throws InstanceNotFoundException, PermissionException, AlreadyRatedException {
        Optional<Order> orderOpt = orderDao.findById(orderId);
        if (!orderOpt.isPresent()) {
            throw new InstanceNotFoundException(ORDERID_FIELD, orderId);
        }
        Order order = orderOpt.get();
        User buyer = order.getBuyer();
        if (!buyer.getId().equals(userId)) {
            throw new PermissionException();
        }
        if (order.getRating() != null) {
            throw new AlreadyRatedException(orderId);
        }
        Rating rating = new Rating(buyer, order.getSeller(), order, LocalDateTime.now(), rate, comment);
        order.setRating(rating);
        ratingDao.save(rating);
        return rating;
    }

    @Override
    @Transactional(readOnly = true)
    public RatingBlock getUserRatings(Long userId, int page, int size) {
        Page<Rating> ratings = ratingDao.findBySellerIdOrderByDateDesc(userId, PageRequest.of(page, size));
        double avg = 0;
        try {
            avg = ratingDao.getAvgBySellerId(userId);
        } catch (AopInvocationException e) {
        }
        return new RatingBlock(new Block<Rating>(ratings.getContent(), ratings.getTotalElements(),
                ratings.getTotalPages(), ratings.hasNext()), avg);
    }

    @Override
    @Transactional(readOnly = true)
    public RatingBlock getRatings(String username, int page, int size) {
        Page<Rating> ratings = ratingDao.findBySellerUsernameOrderByDateDesc(username, PageRequest.of(page, size));
        double avg = 0;
        try {
            avg = ratingDao.getAvgBySellerUsername(username);
        } catch (AopInvocationException e) {
        }
        return new RatingBlock(new Block<Rating>(ratings.getContent(), ratings.getTotalElements(),
                ratings.getTotalPages(), ratings.hasNext()), avg);
    }

    private void storeImage(Long userId, Product product, MultipartFile file, int i) throws FileUploadException {
        Map<String, String> metadata = new HashMap<>();
        metadata.put("Content-Type", file.getContentType());
        metadata.put("Content-Length", String.valueOf(file.getSize()));
        String path = String.format("%s/%s/%s", bucketName, userId, product.getId());
        String name = String.format("%s_%s.%s", product.getName(), i, file.getContentType().split("/")[1]);
        String link = String.format("%s/%s/%s", userId, product.getId(), name);

        try {
            fileStore.saveImage(path, name, Optional.of(metadata), file.getInputStream());
            Image image = new Image(name, link);
            image.setProduct(product);
            imageDao.save(image);
            product.addImage(image);
        } catch (IOException e) {
            throw new FileUploadException(file.getOriginalFilename());
        }
    }

    private void storeImages(Long userId, Product product, MultipartFile[] files) throws FileUploadException {
        for (int i = 0; i < files.length; i++) {
            storeImage(userId, product, files[i], i);
        }
    }

    private void updateImages(Product product, MultipartFile[] files)
            throws FileUploadException, FileDeleteException, InvalidQuantityException, InvalidFileException {
        List<Image> images = new ArrayList<Image>(product.getImages());
        int imagesToDelete = 0;
        for (int i = 0; i < files.length; i++) {
            if (files[i].getSize() > 0 && !Arrays.asList(IMAGE_JPEG.getMimeType(), IMAGE_PNG.getMimeType())
                    .contains(files[i].getContentType())) {
                throw new InvalidFileException(files[i].getContentType());
            }
            if (files[i].getSize() <= 0 && files[i].getOriginalFilename().equals("deleted")) {
                imagesToDelete += 1;
            }
        }
        if (imagesToDelete >= files.length) {
            throw new InvalidQuantityException(IMAGES_FIELD, MIN_IMAGES, MAX_IMAGES);
        }
        images.sort(Comparator.comparing(Image::getName));

        for (int i = 0; i < files.length; i++) {
            if (files[i].getSize() > 0 && i < images.size()) {
                Map<String, String> metadata = new HashMap<>();
                metadata.put("Content-Type", files[i].getContentType());
                metadata.put("Content-Length", String.valueOf(files[i].getSize()));
                String[] parts = images.get(i).getLink().split("/");
                String path = String.format("%s/%s/%s", bucketName, parts[0], parts[1]);
                try {
                    fileStore.saveImage(path, images.get(i).getName(), Optional.of(metadata),
                            files[i].getInputStream());
                } catch (IOException e) {
                    throw new FileUploadException(files[i].getOriginalFilename());
                }

            } else if (i >= images.size()) {
                String[] parts = images.get(images.size() - 1).getName().split("_");
                String[] parts2 = parts[parts.length - 1].split("\\.");
                int nextNumb = Integer.valueOf(parts2[0]) + 1;
                storeImage(product.getUser().getId(), product, files[i], nextNumb);
            } else if (files[i].getSize() <= 0 && files[i].getOriginalFilename().equals("deleted")) {
                deleteImage(images.get(i));
            }
        }
    }

    private void deleteImage(Image image) throws FileDeleteException {
        fileStore.deleteImage(image.getLink(), image.getName(), bucketName);
        image.getProduct().removeImage(image);
        imageDao.delete(image);
    }

    private void deleteAllImages(Product product) throws FileDeleteException {
        String path = String.format("%s/%s/", product.getUser().getId(), product.getId());
        fileStore.deleteImage(path, product.getName(), bucketName);
        imageDao.deleteAll(product.getImages());
        product.removeImages();
        productDao.delete(product);
    }

}
