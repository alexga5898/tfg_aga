package es.udc.tfg.aga.backend.rest.common.stripe;

import java.util.HashMap;
import java.util.Map;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Account;
import com.stripe.model.AccountLink;
import com.stripe.param.AccountCreateParams;
import com.stripe.param.AccountLinkCreateParams;
import com.stripe.param.AccountLinkCreateParams.Type;

import org.springframework.beans.factory.annotation.Value;

import es.udc.tfg.aga.backend.rest.dtos.StripeConnectDto;

public class ConnectAccount {

    @Value("${my.stripe.key}")
    private String stripeKey;

    private static final String YOUR_DOMAIN = "http://localhost:3000/users/profile";

    public StripeConnectDto createExpressAccount(Long userId) throws StripeException {
        Stripe.apiKey = stripeKey;
        Map<String,String> metadata = new HashMap<>();
        metadata.put("userId", userId.toString());
        AccountCreateParams params = AccountCreateParams.builder().setType(AccountCreateParams.Type.EXPRESS)
                .setMetadata(metadata)
                .build();

        Account account = Account.create(params);

        AccountLinkCreateParams linkParams = AccountLinkCreateParams.builder().setAccount(account.getId())
                .setType(Type.ACCOUNT_ONBOARDING).setRefreshUrl(YOUR_DOMAIN + "/connectStripe")
                .setReturnUrl(YOUR_DOMAIN).build();

        AccountLink accountLink = AccountLink.create(linkParams);

        return new StripeConnectDto(accountLink.getUrl());
    }

    public StripeConnectDto connectExpressAccount(String accountId) throws StripeException {
        Stripe.apiKey = stripeKey;
        AccountLinkCreateParams linkParams = AccountLinkCreateParams.builder().setAccount(accountId)
                .setType(Type.ACCOUNT_ONBOARDING).setRefreshUrl(YOUR_DOMAIN + "/connectStripe")
                .setReturnUrl(YOUR_DOMAIN).build();

        AccountLink accountLink = AccountLink.create(linkParams);

        return new StripeConnectDto(accountLink.getUrl());
    }
}
