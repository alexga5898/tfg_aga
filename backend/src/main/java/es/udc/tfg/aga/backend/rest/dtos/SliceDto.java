package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;

public class SliceDto<T> {
    private List<T> items;
    private boolean existMoreItems;

    public SliceDto(){
        
    }

    public SliceDto(List<T> items, boolean existMoreItems) {
        this.items = items;
        this.existMoreItems = existMoreItems;
    }

    public List<T> getItems() {
        return this.items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public boolean isExistMoreItems() {
        return this.existMoreItems;
    }

    public boolean getExistMoreItems() {
        return this.existMoreItems;
    }

    public void setExistMoreItems(boolean existMoreItems) {
        this.existMoreItems = existMoreItems;
    }

}
