package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ConditionDao extends PagingAndSortingRepository<Condition, Long>{
    Optional<Condition> findByName(String name);
}
