package es.udc.tfg.aga.backend.rest.common.amazon;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;

public class Email {
    @Value("${my.aws.smtp.user}")
    private String smtpUsername;

    @Value("${my.aws.smtp.password}")
    private String smtpPassword;

    @Value("${my.aws.smtp.from}")
    private String from;

    @Value("${my.aws.smtp.fromname}")
    private String fromName;

    @Value("${my.aws.smtp.host}")
    private String host;

    @Value("${my.aws.smtp.port}")
    private int port;

    public void sendEmail(String to, String subject, String body) throws Exception {
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);

        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(from, fromName));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
        msg.setSubject(subject, "text/plain; charset=UTF-8");
        msg.setContent(body, "text/html; charset=UTF-8");

        Transport transport = session.getTransport();

        try {
            transport.connect(host, smtpUsername, smtpPassword);
            transport.sendMessage(msg, msg.getAllRecipients());
        } catch (Exception ex) {
        } finally {
            transport.close();
        }
    }
}
