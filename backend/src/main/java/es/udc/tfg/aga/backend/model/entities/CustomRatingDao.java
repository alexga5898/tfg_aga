package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

public interface CustomRatingDao {
    Optional<Rating> findBySellerIdBuyerId(Long sellerId, Long buyerId);
}
