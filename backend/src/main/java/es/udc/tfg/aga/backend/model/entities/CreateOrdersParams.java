package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;

public class CreateOrdersParams {
    private Product product;
    private BigDecimal price;
    private int quantity;

    public CreateOrdersParams() {
        
    }

    public CreateOrdersParams(Product product, BigDecimal price, int quantity) {
        this.product = product;
        this.price = price;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
