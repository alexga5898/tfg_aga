package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;

public class ShoppingCartItemDto {
    private Long productId;
    private String productName;
    private BigDecimal price;
    private int quantity;
    private int stock;
    private String sellerName;

    public ShoppingCartItemDto() {
    }

    public ShoppingCartItemDto(Long productId, String productName, BigDecimal price, int quantity, int stock,
            String sellerName) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
        this.stock = stock;
        this.sellerName = sellerName;
    }

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStock() {
        return this.stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getSellerName() {
        return this.sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

}