package es.udc.tfg.aga.backend.model.exceptions;

public class AlreadyRatedException extends Exception {

    private Long orderId;

    public AlreadyRatedException(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    
}
