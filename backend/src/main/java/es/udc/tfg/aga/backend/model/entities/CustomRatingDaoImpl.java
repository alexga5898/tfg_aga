package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CustomRatingDaoImpl implements CustomRatingDao {
    
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Rating> findBySellerIdBuyerId(Long sellerId, Long buyerId) {
        String queryString = "SELECT r from Rating r";

        if (sellerId != null || buyerId != null) {
            queryString += " WHERE ";
            if (sellerId != null) {
                queryString += "r.seller.id = :sellerId";
            }
            if (buyerId != null) {
                if (sellerId != null) {
                    queryString += " AND ";
                }
                queryString += "r.buyer.id = :buyerId";
            }
        }

        Query query = entityManager.createQuery(queryString);

        if (sellerId != null) {
            query.setParameter("sellerId", sellerId);
        }

        if (buyerId != null) {
            query.setParameter("buyerId", buyerId);
        }

        Optional<Rating> rating = Optional.empty();

        try {
            rating = Optional.of((Rating) query.getSingleResult());
        } catch (NoResultException e) {

        }        

        return rating;
 
    }
}
