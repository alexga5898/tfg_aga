package es.udc.tfg.aga.backend.rest.dtos;

public class StripeConnectDto {
    private String url;

    public StripeConnectDto() {
    }

    public StripeConnectDto(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
