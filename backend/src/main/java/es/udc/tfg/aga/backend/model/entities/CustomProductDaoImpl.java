package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class CustomProductDaoImpl implements CustomProductDao {

    @PersistenceContext
    private EntityManager entityManager;

    private String[] getTokens(String keywords) {

        if (keywords == null || keywords.length() == 0) {
            return new String[0];
        } else {
            return keywords.split("\\s");
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<Product> findUserProducts(Long userId, String keywords, int page, int size) {
        String[] tokens = getTokens(keywords);
        String queryString = "SELECT p FROM Product p";
        String countQueryString = "SELECT COUNT(p) FROM Product p";

        if (userId != null || tokens.length > 0) {
            queryString += " WHERE ";
            countQueryString += " WHERE ";
        }

        if (userId != null) {
            queryString += "p.user.id = :userId";
            countQueryString += "p.user.id = :userId";
        }

        if (tokens.length != 0) {

            if (userId != null) {
                queryString += " AND ";
                countQueryString += " AND ";
            }

            for (int i = 0; i < tokens.length - 1; i++) {
                queryString += "LOWER(p.name) LIKE LOWER(:token" + i + ") AND ";
                countQueryString += "LOWER(p.name) LIKE LOWER(:token" + i + ") AND ";
            }

            queryString += "LOWER(p.name) LIKE LOWER(:token" + (tokens.length - 1) + ")";
            countQueryString += "LOWER(p.name) LIKE LOWER(:token" + (tokens.length - 1) + ")";

        }

        queryString += " ORDER BY p.date DESC";
        countQueryString += " ORDER BY p.date DESC";

        Query query = entityManager.createQuery(queryString).setFirstResult(page * size).setMaxResults(size + 1);
        Query countQuery = entityManager.createQuery(countQueryString);

        if (userId != null) {
            query.setParameter("userId", userId);
            countQuery.setParameter("userId", userId);
        }

        if (tokens.length != 0) {
            for (int i = 0; i < tokens.length; i++) {
                query.setParameter("token" + i, '%' + tokens[i] + '%');
                countQuery.setParameter("token" + i, '%' + tokens[i] + '%');
            }

        }

        List<Product> products = query.getResultList();

        long totalProducts = (long) countQuery.getResultList().get(0);

        boolean hasNext = products.size() == (size + 1);

        if (hasNext) {
            products.remove(products.size() - 1);
        }

        return new PageImpl<>(products, PageRequest.of(page, size), totalProducts);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<Product> findProducts(String keywords, Long categoryId, Long conditionId, BigDecimal lPrice,
            BigDecimal hPrice, int order, int page, int size) {

        String[] tokens = getTokens(keywords);

        String queryString = "SELECT p FROM Product p";
        String countQueryString = "SELECT COUNT(p) FROM Product p";

        if (categoryId != null || conditionId != null || lPrice != null || hPrice != null || tokens.length != 0) {
            queryString += " WHERE";
            countQueryString += " WHERE";
            if (categoryId != null) {
                queryString += " p.category.id = :categoryId";
                countQueryString += " p.category.id = :categoryId";
            }
            if (conditionId != null) {
                if (categoryId != null) {
                    queryString += " AND";
                    countQueryString += " AND";
                }
                queryString += " p.condition.id = :conditionId";
                countQueryString += " p.condition.id = :conditionId";
            }

            if (lPrice != null || hPrice != null) {
                if (categoryId != null || conditionId != null) {
                    queryString += " AND";
                    countQueryString += " AND";
                }
                queryString += " p.price BETWEEN :lPrice AND :hPrice";
                countQueryString += " p.price BETWEEN :lPrice AND :hPrice";
            }
            if (tokens.length != 0) {
                if (categoryId != null || conditionId != null || lPrice != null || hPrice != null) {
                    queryString += " AND";
                    countQueryString += " AND";
                }
                for (int i = 0; i < tokens.length - 1; i++) {
                    queryString += " LOWER(p.name) LIKE LOWER(:token" + i + ") AND";
                    countQueryString += " LOWER(p.name) LIKE LOWER(:token" + i + ") AND";
                }
                queryString += " LOWER(p.name) LIKE LOWER(:token" + (tokens.length - 1) + ")";
                countQueryString += " LOWER(p.name) LIKE LOWER(:token" + (tokens.length - 1) + ")";
            }
            if (order == 0) {
                queryString += " ORDER BY p.date DESC";
                countQueryString += " ORDER BY p.date DESC";
            } else if (order == 1) {
                queryString += " ORDER BY p.date ASC";
                countQueryString += " ORDER BY p.date ASC";
            } else if (order == 2) {
                queryString += " ORDER BY p.price ASC";
                countQueryString += " ORDER BY p.price ASC";
            } else if (order == 3) {
                queryString += " ORDER BY p.price DESC";
                countQueryString += " ORDER BY p.price DESC";
            }
        }

        // if (rate) {
        // queryString += " p.user.rating.rate";
        // } else {
        // queryString += " p.date";
        // }
        // if (asc) {
        // queryString += " DESC";
        // countQueryString += " DESC";
        // }

        Query query = entityManager.createQuery(queryString).setFirstResult(page * size).setMaxResults(size + 1);
        Query countQuery = entityManager.createQuery(countQueryString);

        if (categoryId != null) {
            query.setParameter("categoryId", categoryId);
            countQuery.setParameter("categoryId", categoryId);
        }
        if (conditionId != null) {
            query.setParameter("conditionId", conditionId);
            countQuery.setParameter("conditionId", conditionId);
        }
        if (lPrice != null || hPrice != null) {
            if (lPrice == null) {
                query.setParameter("lPrice", 0);
                query.setParameter("hPrice", hPrice);
                countQuery.setParameter("lPrice", 0);
                countQuery.setParameter("hPrice", hPrice);
            } else if (hPrice == null) {
                query.setParameter("lPrice", lPrice);
                query.setParameter("hPrice", 999999);
                countQuery.setParameter("lPrice", lPrice);
                countQuery.setParameter("hPrice", 999999);
            } else {
                query.setParameter("lPrice", lPrice);
                query.setParameter("hPrice", hPrice);
                countQuery.setParameter("lPrice", lPrice);
                countQuery.setParameter("hPrice", hPrice);
            }
        }
        if (tokens.length != 0) {
            for (int i = 0; i < tokens.length; i++) {
                query.setParameter("token" + i, '%' + tokens[i] + '%');
                countQuery.setParameter("token" + i, '%' + tokens[i] + '%');
            }
        }

        List<Product> products = query.getResultList();

        long totalProducts = (long) countQuery.getResultList().get(0);

        boolean hasNext = products.size() == (size + 1);

        if (hasNext) {
            products.remove(products.size() - 1);
        }

        return new PageImpl<>(products, PageRequest.of(page, size), totalProducts);
    }
}
