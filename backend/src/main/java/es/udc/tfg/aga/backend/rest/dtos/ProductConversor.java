package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Product;

public class ProductConversor {
    private ProductConversor() {

    }

    public final static List<ProductDetailsDto> toProductDetailsDtos(List<Product> products) {
        return products.stream().map(product -> toProductDetailsDto(product)).collect(Collectors.toList());
    }

    public final static ProductDto toProductDto(Product product) {
        return new ProductDto(product.getId(), product.getUser().getId(), product.getUser().getUsername(),
                product.getName(), product.getPrice(), product.getQuantity(),
                ImageConversor.toImagesDto(product.getImages()), product.getCondition().getName(),
                product.getCategory().getName(), product.getDescription(), product.getDate());
    }

    public final static ProductDetailsDto toProductDetailsDto(Product product) {
        return new ProductDetailsDto(product.getId(), product.getName(), product.getPrice(), product.getQuantity(),
                product.getDescription(), product.getDate());
    }
}