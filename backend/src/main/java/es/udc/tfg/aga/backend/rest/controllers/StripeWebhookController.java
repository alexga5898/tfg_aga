package es.udc.tfg.aga.backend.rest.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.Account;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.LineItem;
import com.stripe.model.PaymentIntent;
import com.stripe.model.Product;
import com.stripe.model.StripeObject;
import com.stripe.model.Transfer;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import com.stripe.param.PaymentIntentCaptureParams;
import com.stripe.param.TransferCreateParams;
import com.stripe.param.checkout.SessionRetrieveParams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.udc.tfg.aga.backend.model.entities.Address;
import es.udc.tfg.aga.backend.model.entities.CreateOrdersParams;
import es.udc.tfg.aga.backend.model.entities.TransferDataParams;
import es.udc.tfg.aga.backend.model.exceptions.EmptyShoppingCartException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InsufficientStockException;
import es.udc.tfg.aga.backend.model.exceptions.PaymentIntentException;
import es.udc.tfg.aga.backend.model.model.CatalogService;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;
import es.udc.tfg.aga.backend.rest.common.amazon.Email;
import es.udc.tfg.aga.backend.rest.dtos.ErrorsDto;

@RestController
@RequestMapping("/stripe")
@Validated
public class StripeWebhookController {

    @Autowired
    ShoppingService shoppingService;

    @Autowired
    UserService userService;

    @Autowired
    CatalogService catalogService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Email email;

    @Value("${my.stripe.webhook.secret}")
    private String endpointSecret;

    private String DESERIALIZATION = "exception.fields.Deserialization";
    private String EMPTY_SHOPPINGCART_EXCEPTION = "exceptions.EmptyShoppingCartException";
    private String INSUFICIENT_STOCK = "exceptions.InsufficientStock";
    private String ILLEGAL_STATE_EXCEPTION = "exceptions.IllegalStateException";

    @ExceptionHandler(EmptyShoppingCartException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsDto handleEmptyShoppingCartException(EmptyShoppingCartException e, Locale locale) {
        String message = messageSource.getMessage(EMPTY_SHOPPINGCART_EXCEPTION, new Object[] {},
                EMPTY_SHOPPINGCART_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(InsufficientStockException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ErrorsDto handleInsufficientStock(InsufficientStockException e, Locale locale) {
        String message = messageSource.getMessage(INSUFICIENT_STOCK, new Object[] {}, INSUFICIENT_STOCK, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorsDto handleIllegalStateException(IllegalStateException e, Locale locale) {
        String message = messageSource.getMessage(ILLEGAL_STATE_EXCEPTION, new Object[] { e.getMessage() },
                ILLEGAL_STATE_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @PostMapping("/webhook")
    public String stripeWebhookEndpoint(@RequestHeader("Stripe-Signature") String sigHeader,
            @RequestBody String stringEvent)
            throws NumberFormatException, InstanceNotFoundException, EmptyShoppingCartException, StripeException,
            InsufficientStockException, PaymentIntentException {

        Event event = Event.GSON.fromJson(stringEvent, Event.class);
        if (endpointSecret != null && sigHeader != null) {
            try {
                event = Webhook.constructEvent(stringEvent, sigHeader, endpointSecret);

            } catch (SignatureVerificationException e) {
                throw new PaymentIntentException();
            }
        }
        EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
        StripeObject stripeObject = null;

        if (dataObjectDeserializer.getObject().isPresent()) {
            stripeObject = dataObjectDeserializer.getObject().get();
        } else {
            throw new IllegalStateException(DESERIALIZATION);
        }

        if (event.getType().equals("checkout.session.completed")) {
            Session sessionEvent = (Session) stripeObject;

            // Retrieve the complete Session object with line items
            Session session = Session.retrieve(sessionEvent.getId(),
                    SessionRetrieveParams.builder().addExpand("line_items").build(), null);

            // Retrieve the complete PaymentIntent object
            PaymentIntent paymentIntent = PaymentIntent.retrieve(session.getPaymentIntent());

            // Check products stock
            if (session.getLineItems().getData().size() == 0) {
                throw new EmptyShoppingCartException();
            }

            // Change products stock
            List<CreateOrdersParams> createOrdersParams = new ArrayList<>();
            for (LineItem item : session.getLineItems().getData()) {
                Product product = Product.retrieve(item.getPrice().getProduct());
                createOrdersParams.add(new CreateOrdersParams(
                        catalogService.getProduct(Long.parseLong(product.getMetadata().get("productId"))),
                        item.getPrice().getUnitAmountDecimal().divide(new BigDecimal(100)),
                        item.getQuantity().intValue()));
            }

            try {
                shoppingService.checkAndChangeStock(createOrdersParams);
            } catch (InsufficientStockException e) {
                paymentIntent.cancel();
                try {
                    String subject = "Ha habido un error con tu pedido / There was an error with your order";
                    String body = String.join(System.getProperty("line.separator"),
                            "<h1>Ha habido un error con tu pedido / There was an error with your order</h1>",
                            "<p>Alguno de los productos que intentabas comprar no está disponible en la cantidad deseada. Por favor, revisa tu carrito. Sentimos las molestias.</p>",
                            "<p>Some of the products that you tried to buy are no longer available in the desirated quantity. Please, check your cart. We are sorry for the inconvenience.</p>");
                    email.sendEmail(session.getCustomerEmail(), subject, body);
                } catch (Exception ex) {
                }
                throw new InsufficientStockException();
            }

            // Capture payment
            PaymentIntentCaptureParams params = PaymentIntentCaptureParams.builder()
                    .setAmountToCapture(paymentIntent.getAmount()).build();
            paymentIntent.capture(params);

            // Create orders
            Address address = new Address(session.getShipping().getAddress().getCountry(),
                    session.getShipping().getAddress().getCity(),
                    session.getShipping().getAddress().getLine1() + " " + session.getShipping().getAddress().getLine2(),
                    session.getShipping().getAddress().getPostalCode());

            List<TransferDataParams> transferDataParams = shoppingService
                    .buy(Long.valueOf(session.getClientReferenceId()), createOrdersParams, address);

            // Send email to customer
            try {
                String subject = "Tu pedido se ha procesado / Your order has been proccessed";
                String body = String.join(System.getProperty("line.separator"),
                        "<h1>¡Tu pedido se ha procesado! / Your order has been proccessed!</h1>",
                        "<p>Comprueba tus <a href='http://localhost:3000/shopping/orders'>pedidos</a> para ver los detalles.</p>",
                        "<p>Check your <a href='http://localhost:3000/shopping/orders'>orders</a> to see the details.</p>");
                email.sendEmail(session.getCustomerEmail(), subject, body);
            } catch (Exception e) {
            }

            // Create transfers for each vendor and send emails
            for (TransferDataParams transferToDo : transferDataParams) {
                TransferCreateParams transferParams = TransferCreateParams.builder()
                        .setAmount(transferToDo.getAmount().scaleByPowerOfTen(2).longValue()).setCurrency("eur")
                        .setSourceTransaction(paymentIntent.getCharges().getData().get(0).getId())
                        .setDestination(transferToDo.getStripeAccId()).build();
                Transfer.create(transferParams);
                try {
                    String subject = "Has recibido un pedido / You have received an order";
                    String body = String.join(System.getProperty("line.separator"),
                            "<h1>¡Acabas de recibir un pedido! / You have received an order!</h1>",
                            "<p>Comprueba tus <a href='http://localhost:3000/shopping/sells'>ventas</a> para ver los detalles. Recuerda que ahora debes de realizar el envío correspondiente al comprador.</p>",
                            "<p>Check your <a href='http://localhost:3000/shopping/sells'>sells</a> to see the details. Remember that now you have to send the items to the buyer.</p>");
                    email.sendEmail(transferToDo.getSellerEmail(), subject, body);
                } catch (Exception e) {
                }
            }

        } else if (event.getType().equals("account.updated")) {
            Account account = (Account) stripeObject;
            if (account.getMetadata().get("userId") != null
                    && !userService.checkStripeAcc(Long.parseLong(account.getMetadata().get("userId")))) {
                userService.storeStripeAccId(Long.parseLong(account.getMetadata().get("userId")), account.getId());
            }
            if (account.getMetadata().get("userId") != null && userService
                    .getStripeAccId(Long.parseLong(account.getMetadata().get("userId"))).equals(account.getId())
                    && account.getDetailsSubmitted()) {
                userService.activateStripeAcc(Long.parseLong(account.getMetadata().get("userId")));
            }
        }

        return "";
    }

}
