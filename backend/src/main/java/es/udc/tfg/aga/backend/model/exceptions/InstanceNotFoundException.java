package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class InstanceNotFoundException extends InstanceException{
    
    public InstanceNotFoundException(String name, Object object){
        super(name,object);
    }
}
