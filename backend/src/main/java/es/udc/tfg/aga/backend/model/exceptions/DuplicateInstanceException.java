package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class DuplicateInstanceException extends InstanceException{
    
    public DuplicateInstanceException(String name, Object object){
        super(name,object);
    }
}
