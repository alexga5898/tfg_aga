package es.udc.tfg.aga.backend.model.entities;

public class RatingBlock {
    private Block<Rating> ratings;
    private double avg;

    public RatingBlock() {

    }

    public RatingBlock(Block<Rating> ratings, double avg) {
        this.ratings = ratings;
        this.avg = avg;
    }

    public Block<Rating> getRatings() {
        return this.ratings;
    }

    public void setRatings(Block<Rating> ratings) {
        this.ratings = ratings;
    }

    public double getAvg() {
        return this.avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

}
