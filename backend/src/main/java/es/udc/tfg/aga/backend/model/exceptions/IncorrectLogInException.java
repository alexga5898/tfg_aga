package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class IncorrectLogInException extends Exception{
    private String email;
    private String password;

    public IncorrectLogInException(String email, String password){
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

}
