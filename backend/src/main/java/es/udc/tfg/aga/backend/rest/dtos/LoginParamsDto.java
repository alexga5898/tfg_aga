package es.udc.tfg.aga.backend.rest.dtos;

public class LoginParamsDto {
    String email;
    String password;

    public LoginParamsDto() {

    }

    public LoginParamsDto(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
