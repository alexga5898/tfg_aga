package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class InvalidFileException extends Exception {
    private String contentType;

    public InvalidFileException(String contentType){
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }
}