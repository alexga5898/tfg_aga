package es.udc.tfg.aga.backend.model.entities;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ImageDao extends PagingAndSortingRepository<Image, Long> {
    List<Image> findByProductId(Long productId);
}
