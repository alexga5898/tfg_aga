package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// import java.util.List;
// import java.util.Set;
// import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Address;

public class AddressConversor {

    private AddressConversor() {

    }

    // public static final Set<Address> toAddresses(List<AddressDto> addressesDto){
    // Set<Address> address = addressesDto.stream().map(i -> AddressConversor
    // .toAddress(i)).collect(Collectors.toSet());
    // return address;
    // }

    public static final List<AddressDto> toAddressesDto(Set<Address> addresses) {
        List<AddressDto> address = addresses.stream().map(i -> AddressConversor.toAddressDto(i))
                .collect(Collectors.toList());
        return address;
    }

    public static final AddressDto toAddressDto(Address address) {
        return new AddressDto(address.getOrder().getId(), address.getCountry(), address.getCity(), address.getStreet(),
                address.getPostalCode());
    }

    public static final AddressDto toAddressDto(com.stripe.model.Address address) {
        return new AddressDto(null, address.getCountry(), address.getCity(),
                address.getLine1() + " " + address.getLine2(), address.getPostalCode());
    }

    // public static final Address toAddress(AddressDto addressDto) {
    // return new Address(addressDto.getAddressName(), addressDto.getCountry(),
    // addressDto.getCity(),
    // addressDto.getStreet(), addressDto.getPostalCode());
    // }
}
