package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Message;

public class MessageConversor {
    private MessageConversor() {

    }

    public static final List<MessageDto> toMessagesDto(List<Message> messages) {
        List<MessageDto> messagesdDtos = messages.stream().map(i -> MessageConversor.toMessageDto(i))
                .collect(Collectors.toList());
        return messagesdDtos;
    }

    public static final MessageDto toMessageDto(Message message) {
        return new MessageDto(message.getId(), message.getChat().getId(), message.getSender().getId(),
                message.getProduct().getId(), message.getContent(), message.getDate());
    }

    public static final NotificationDto toNotificationDto(Message message) {
        return new NotificationDto(message.getId(), message.getChat().getId(), message.getSender().getId(),
                message.getRecipient().getId(), message.getProduct().getId());
    }
}
