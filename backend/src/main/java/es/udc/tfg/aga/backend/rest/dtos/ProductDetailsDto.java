package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ProductDetailsDto {
    private Long id;
    private String name;
    private BigDecimal price;
    private int quantity;
    private String description;
    private LocalDateTime date;

    public ProductDetailsDto() {

    }

    public ProductDetailsDto(Long id, String name, BigDecimal price, int quantity, String description,
            LocalDateTime date) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
        this.date = date;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
