package es.udc.tfg.aga.backend.rest.dtos;

public class RatingDto {
    private Long id;
    private String buyerUsername;
    private String sellerUsername;
    private int rate;
    private String comment;

    public RatingDto() {

    }

    public RatingDto(Long id, String buyerUsername, String sellerUsername, int rate, String comment) {
        this.id = id;
        this.buyerUsername = buyerUsername;
        this.sellerUsername = sellerUsername;
        this.rate = rate;
        this.comment = comment;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBuyerUsername() {
        return this.buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getSellerUsername() {
        return this.sellerUsername;
    }

    public void setSellerName(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public int getRate() {
        return this.rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
