package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, Long>, CustomProductDao {
    Page<Product> findByUserIdOrderByDateDesc(Long userId, Pageable pageable);
    Optional<Product> findByName(String name);
    Page<Product> findByUserUsernameOrderByDateDesc(String userUsername, Pageable pageable);
}
