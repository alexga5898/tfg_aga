package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.udc.tfg.aga.backend.model.exceptions.MaxProductQuantityException;

@Entity
@Table(name="ShoppingCartItemTable")
public class ShoppingCartItem {

    private Long id;
    private ShoppingCart shoppingCart;
    private Product product;
    private int quantity;

    public ShoppingCartItem(){

    }

    public ShoppingCartItem(ShoppingCart shoppingCart, Product product, int quantity){
        this.shoppingCart = shoppingCart;
        this.product = product;
        this.quantity = quantity;
    }

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "shoppingCartId")
    public ShoppingCart getShoppingCart() {
        return this.shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "productId")
    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incrementQuantity(int quantity, int productQuantity) throws MaxProductQuantityException {
        if (this.quantity + quantity > productQuantity) {
            throw new MaxProductQuantityException();
        }
        this.quantity += quantity;
    }

    @Transient
	public BigDecimal getTotalPrice() {
		return product.getPrice().multiply(new BigDecimal(quantity));
	}

    public void updateQuantity(int quantity, int productQuantity) throws MaxProductQuantityException {
        if (quantity > productQuantity) {
            throw new MaxProductQuantityException();
        }
        this.quantity = quantity;
    }

}

