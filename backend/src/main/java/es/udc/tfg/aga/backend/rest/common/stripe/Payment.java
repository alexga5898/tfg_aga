package es.udc.tfg.aga.backend.rest.common.stripe;

import java.util.ArrayList;
import java.util.List;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.param.checkout.SessionCreateParams.LineItem;
import com.stripe.param.checkout.SessionCreateParams.PaymentIntentData;

import org.springframework.beans.factory.annotation.Value;

import es.udc.tfg.aga.backend.model.exceptions.EmptyShoppingCartException;
import es.udc.tfg.aga.backend.rest.dtos.ShoppingCartDto;
import es.udc.tfg.aga.backend.rest.dtos.ShoppingCartItemDto;
import es.udc.tfg.aga.backend.rest.dtos.StripeSessionDto;

public class Payment {

    @Value("${my.stripe.key}")
    private String stripeKey;

    private static final String YOUR_DOMAIN = "http://localhost:3000/shopping/";

    public StripeSessionDto paymentIntent(ShoppingCartDto shoppingCartDto)
            throws StripeException, EmptyShoppingCartException {
        Stripe.apiKey = stripeKey;
        List<LineItem> lineItems = new ArrayList<>();
        if (shoppingCartDto.getItems().size() == 0) {
            throw new EmptyShoppingCartException();
        }
        for (ShoppingCartItemDto item : shoppingCartDto.getItems()) {
            lineItems.add(SessionCreateParams.LineItem.builder().setQuantity(Long.valueOf(item.getQuantity()))
                    .setPriceData(SessionCreateParams.LineItem.PriceData.builder().setCurrency("eur")
                            .setUnitAmount(item.getPrice().scaleByPowerOfTen(2).longValue())
                            .setProductData(SessionCreateParams.LineItem.PriceData.ProductData.builder()
                                    .setName(item.getProductName())
                                    .putMetadata("productId", item.getProductId().toString()).build())
                            .build())
                    .build());
        }

        SessionCreateParams params = SessionCreateParams.builder()
                .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                .setCustomerEmail(shoppingCartDto.getUserEmail()).addExpand("payment_intent.charges")
                .setPaymentIntentData(PaymentIntentData.builder()
                        .setCaptureMethod(SessionCreateParams.PaymentIntentData.CaptureMethod.MANUAL).build())
                .setClientReferenceId(shoppingCartDto.getUserId().toString())
                .setBillingAddressCollection(SessionCreateParams.BillingAddressCollection.REQUIRED)
                .setShippingAddressCollection(SessionCreateParams.ShippingAddressCollection.builder()
                        .addAllowedCountry(SessionCreateParams.ShippingAddressCollection.AllowedCountry.ES)
                        .addAllowedCountry(SessionCreateParams.ShippingAddressCollection.AllowedCountry.FR).build())
                .setMode(SessionCreateParams.Mode.PAYMENT).addAllLineItem(lineItems)
                .setSuccessUrl(YOUR_DOMAIN + "order?session_id={CHECKOUT_SESSION_ID}")
                .setCancelUrl(YOUR_DOMAIN + "shoppingCart").build();

        Session session = Session.create(params);
        return new StripeSessionDto(session.getId());
    }
}