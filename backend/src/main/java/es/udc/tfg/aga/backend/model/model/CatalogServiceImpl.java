package es.udc.tfg.aga.backend.model.model;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.tfg.aga.backend.model.amazon.FileStore;
import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Category;
import es.udc.tfg.aga.backend.model.entities.CategoryDao;
import es.udc.tfg.aga.backend.model.entities.Condition;
import es.udc.tfg.aga.backend.model.entities.ConditionDao;
import es.udc.tfg.aga.backend.model.entities.Image;
import es.udc.tfg.aga.backend.model.entities.ImageDao;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.ProductDao;
import es.udc.tfg.aga.backend.model.exceptions.FilesDownloadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;

@Service
@Transactional
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ConditionDao conditionDao;

    @Autowired
    private FileStore fileStore;

    @Value("${my.aws.s3.bucket}")
    private String bucketName;

    private static final String IMAGEID_FIELD = "image.fields.imageId";
    private static final String PRODUCTID_FIELD = "product.fields.productId";
    private static final String CATEGORY_FIELD = "product.fields.category";
    private static final String CONDITION_FIELD = "product.fields.condition";

    @Override
    @Transactional(readOnly = true)
    public Block<Product> getUserProducts(Long userId, String keywords, int page, int size) {
        // Page<Product> productsPage = productDao.findByUserIdOrderByDateDesc(userId,
        // PageRequest.of(page, size));
        Page<Product> productsPage = productDao.findUserProducts(userId, keywords, page, size);
        return new Block<Product>(productsPage.getContent(), productsPage.getTotalElements(),
                productsPage.getTotalPages(), productsPage.hasNext());
    }

    @Override
    @Transactional(readOnly = true)
    public byte[] getImage(Long imageId) throws InstanceNotFoundException, FilesDownloadException {
        Optional<Image> imageOpt = imageDao.findById(imageId);
        if (!imageOpt.isPresent()) {
            throw new InstanceNotFoundException(IMAGEID_FIELD, imageId);
        }
        Image image = imageOpt.get();
        return fileStore.getImage(bucketName, image.getLink());
    }

    @Override
    @Transactional(readOnly = true)
    public byte[] getThumbnail(Long productId) throws InstanceNotFoundException, FilesDownloadException {
        Image image = imageDao.findByProductId(productId).get(0);
        if (image == null) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        return fileStore.getImage(bucketName, image.getLink());
    }

    @Override
    @Transactional(readOnly = true)
    public Product getProduct(Long productId) throws InstanceNotFoundException {
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        return productOpt.get();
    }

    @Override
    @Transactional(readOnly = true)
    public Block<Product> getProducts(String keywords, String category, String condition, BigDecimal lPrice,
            BigDecimal hPrice, int order, int page, int size) throws InstanceNotFoundException {
        Long categoryId = null;
        Long conditionId = null;
        if (category.length() > 0) {
            Optional<Category> categoryOpt = categoryDao.findByName(category);
            if (!categoryOpt.isPresent()) {
                throw new InstanceNotFoundException(CATEGORY_FIELD, category);
            }
            categoryId = categoryOpt.get().getId();
        }
        if (condition.length() > 0) {
            Optional<Condition> conditionOpt = conditionDao.findByName(condition);
            if (!conditionDao.findByName(condition).isPresent()) {
                throw new InstanceNotFoundException(CONDITION_FIELD, condition);
            }
            conditionId = conditionOpt.get().getId();
        }
        Page<Product> productsPage = productDao.findProducts(keywords, categoryId, conditionId, lPrice, hPrice, order,
                page, size);
        return new Block<Product>(productsPage.getContent(), productsPage.getTotalElements(),
                productsPage.getTotalPages(), productsPage.hasNext());
    }

    @Override
    @Transactional(readOnly = true)
    public Block<Product> getProductsByUsername(String username, int page, int size) {
        Page<Product> products = productDao.findByUserUsernameOrderByDateDesc(username, PageRequest.of(page, size));
        return new Block<Product>(products.getContent(), products.getTotalElements(), products.getTotalPages(),
                products.hasNext());
    }

}
