package es.udc.tfg.aga.backend.model.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import es.udc.tfg.aga.backend.model.entities.AddressDao;
import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Chat;
import es.udc.tfg.aga.backend.model.entities.ChatDao;
import es.udc.tfg.aga.backend.model.entities.Message;
import es.udc.tfg.aga.backend.model.entities.MessageDao;
import es.udc.tfg.aga.backend.model.entities.MessageStatus;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.ProductDao;
import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.entities.ShoppingCartDao;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.entities.UserDao;
import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.IncorrectLogInException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    AddressDao addressDao;

    @Autowired
    ProductDao productDao;

    @Autowired
    ShoppingCartDao shoppingCartDao;

    @Autowired
    ChatDao chatDao;

    @Autowired
    MessageDao messageDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private String USERNAME_FIELD = "user.fields.username";
    private String EMAIL_FIELD = "user.fields.email";
    private String USERID_FIELD = "user.fields.userId";
    private String PRODUCTID_FIELD = "product.fields.productId";
    private String CHATID_FIELD = "chat.fields.chatId";

    @Override
    public void registerUser(User user) throws DuplicateInstanceException {
        if (userDao.existsByUsername(user.getUsername())) {
            throw new DuplicateInstanceException(USERNAME_FIELD, user.getUsername());
        } else if (userDao.existsByEmail(user.getEmail())) {
            throw new DuplicateInstanceException(EMAIL_FIELD, user.getEmail());
        } else {
            ShoppingCart shoppingCart = new ShoppingCart(user);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole(User.RoleType.ROLE_USER);
            user.setShoppingCart(shoppingCart);
            userDao.save(user);
            shoppingCartDao.save(shoppingCart);
        }
    }

    @Override
    public User logIn(String email, String password) throws IncorrectLogInException {
        if (!userDao.existsByEmail(email)) {
            throw new IncorrectLogInException(email, password);
        }
        User user = userDao.findByEmail(email).get();
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new IncorrectLogInException(email, password);
        }
        return user;
    }

    @Override
    public User logInFromUserId(Long userId) throws InstanceNotFoundException {
        if (!userDao.existsById(userId)) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        return userDao.findById(userId).get();
    }

    @Override
    public User updateProfile(Long userId, String username, String email, String firstName, String lastName)
            throws InstanceNotFoundException, DuplicateInstanceException {
        if (!userDao.existsById(userId)) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        User user = userDao.findById(userId).get();
        Optional<User> userByUsername = userDao.findByUsername(username);
        if (userByUsername.isPresent() && !userByUsername.get().equals(user)) {
            throw new DuplicateInstanceException(USERNAME_FIELD, user.getUsername());
        }
        Optional<User> userByEmail = userDao.findByEmail(email);
        if (userByEmail.isPresent() && !userByEmail.get().equals(user)) {
            throw new DuplicateInstanceException(EMAIL_FIELD, user.getEmail());
        }
        user.setUsername(username);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public User getUser(Long userId) throws InstanceNotFoundException {
        Optional<User> user = userDao.findById(userId);
        if (!user.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        return user.get();
    }

    @Override
    public Message sendMessage(Long userId, Long recipientId, Long productId, String content)
            throws InstanceNotFoundException {
        Optional<User> senderOpt = userDao.findById(userId);
        if (!senderOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        User sender = senderOpt.get();
        Optional<User> recipientOpt = userDao.findById(recipientId);
        if (!recipientOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, recipientId);
        }
        User recipient = recipientOpt.get();
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Product product = productOpt.get();

        Optional<Chat> chatOpt = chatDao.findChat(userId, recipient.getId(), productId);
        Chat chat;
        if (!chatOpt.isPresent()) {
            chat = new Chat(sender, product.getUser(), product, LocalDateTime.now());
            chatDao.save(chat);
        } else {
            chat = chatOpt.get();
            chat.setLastModified(LocalDateTime.now());
        }
        Message message = new Message(chat, sender, recipient, product, content, LocalDateTime.now(),
                MessageStatus.DELIVERED);
        messageDao.save(message);
        return message;
    }

    @Override
    public void changeMessagesStatus(Long recipientId, Long chatId)
            throws InstanceNotFoundException, PermissionException {
        Optional<Chat> chatOpt = chatDao.findById(chatId);
        if (!chatOpt.isPresent()) {
            throw new InstanceNotFoundException(CHATID_FIELD, chatId);
        }
        if (chatOpt.get().getBuyer().getId() != recipientId && chatOpt.get().getSeller().getId() != recipientId) {
            throw new PermissionException();
        }
        List<Message> messages = messageDao.findByRecipientSenderProductStatus(recipientId, chatId);
        for (Message message : messages) {
            message.setStatus(MessageStatus.RECEIVED);
        }
    }

    @Override
    public Boolean checkNewMessages(Long recipientId, Long chatId)
            throws InstanceNotFoundException, PermissionException {
        Optional<Chat> chatOpt = chatDao.findById(chatId);
        if (!chatOpt.isPresent()) {
            throw new InstanceNotFoundException(CHATID_FIELD, chatId);
        }
        if (chatOpt.get().getBuyer().getId() != recipientId && chatOpt.get().getSeller().getId() != recipientId) {
            throw new PermissionException();
        }
        return messageDao.checkByRecipientChatStatus(recipientId, chatId);
    }

    @Override 
    public Boolean checkNewMessagesGlobal(Long recipientId) throws InstanceNotFoundException {
        Optional<User> recipientOpt = userDao.findById(recipientId);
        if (!recipientOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, recipientId);
        }
        List<Chat> chats = chatDao.findChats(recipientId, PageRequest.of(0, 9999)).getContent();
        for (Chat chat : chats) {
            if (messageDao.checkByRecipientChatStatus(recipientId, chat.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public Block<Chat> getChats(Long userId, int page, int size) {
        Page<Chat> chatsPage = chatDao.findChats(userId, PageRequest.of(page, size));
        return new Block<Chat>(chatsPage.getContent(), chatsPage.getTotalElements(), chatsPage.getTotalPages(),
                chatsPage.hasNext());
    }

    @Override
    @Transactional(readOnly = true)
    public Chat getChatIfExistent(Long buyerId, Long sellerId, Long productId) throws InstanceNotFoundException {
        Optional<User> buyerOpt = userDao.findById(buyerId);
        if (!buyerOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, buyerId);
        }
        Optional<User> sellerOpt = userDao.findById(sellerId);
        if (!sellerOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, sellerId);
        }
        Optional<Product> productOpt = productDao.findById(productId);
        if (!productOpt.isPresent()) {
            throw new InstanceNotFoundException(PRODUCTID_FIELD, productId);
        }
        Optional<Chat> chat = chatDao.findChat(buyerId, sellerId, productId);
        if (chat.isPresent()) {
            return chat.get();
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Chat getChat(Long userId, Long chatId) throws InstanceNotFoundException, PermissionException {
        Optional<Chat> chatOpt = chatDao.findById(chatId);
        if (!chatOpt.isPresent()) {
            throw new InstanceNotFoundException(CHATID_FIELD, chatId);
        }
        Chat chat = chatOpt.get();
        if (chat.getBuyer().getId() != userId && chat.getSeller().getId() != userId) {
            throw new PermissionException();
        }
        return chat;
    }

    @Override
    @Transactional(readOnly = true)
    public Block<Message> getMessages(Long userId, Long chatId, int page, int size)
            throws InstanceNotFoundException, PermissionException {
        Optional<Chat> chatOpt = chatDao.findById(chatId);
        if (!chatOpt.isPresent()) {
            throw new InstanceNotFoundException(CHATID_FIELD, chatId);
        }
        Chat chat = chatOpt.get();
        if (chat.getBuyer().getId() != userId && chat.getSeller().getId() != userId) {
            throw new PermissionException();
        }
        Slice<Message> messages = messageDao.findByChatIdOrderByDateDesc(chatId, PageRequest.of(page, size));
        return new Block<Message>(messages.getContent(), 0, 0, messages.hasNext());
    }

    @Override
    public void storeStripeAccId(Long userId, String stripeAccId) throws InstanceNotFoundException {
        Optional<User> userOpt = userDao.findById(userId);
        if (!userOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        userOpt.get().setStripeAccId(stripeAccId);
    }

    @Override
    public Boolean checkStripeAcc(Long userId) throws InstanceNotFoundException {
        Optional<User> userOpt = userDao.findById(userId);
        if (!userOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        if (userOpt.get().getStripeAccId() == null) {
            return false;
        }
        return true;
    }

    @Override
    public void activateStripeAcc(Long userId) throws InstanceNotFoundException {
        Optional<User> userOpt = userDao.findById(userId);
        if (!userOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        userOpt.get().setActiveTransfers(true);
    }

    @Override
    public String getStripeAccId(Long userId) throws InstanceNotFoundException {
        Optional<User> userOpt = userDao.findById(userId);
        if (!userOpt.isPresent()) {
            throw new InstanceNotFoundException(USERID_FIELD, userId);
        }
        return userOpt.get().getStripeAccId();
    }
}
