package es.udc.tfg.aga.backend.rest.dtos;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.entities.ShoppingCartItem;

public class ShoppingCartConversor {
    private ShoppingCartConversor() {

    }

    public static final List<ShoppingCartItemDto> toShoppingCartItemsDto(Set<ShoppingCartItem> shoppingCartItems) {
        List<ShoppingCartItemDto> shoppingCartItemsDto = shoppingCartItems.stream()
                .map(i -> ShoppingCartConversor.toShoppingCartItemDto(i)).collect(Collectors.toList());
        shoppingCartItemsDto.sort(Comparator.comparing(ShoppingCartItemDto::getSellerName));
        return shoppingCartItemsDto;
    }

    public static final ShoppingCartItemDto toShoppingCartItemDto(ShoppingCartItem shoppingCartItem) {
        return new ShoppingCartItemDto(shoppingCartItem.getProduct().getId(), shoppingCartItem.getProduct().getName(),
                shoppingCartItem.getProduct().getPrice(), shoppingCartItem.getQuantity(),
                shoppingCartItem.getProduct().getQuantity(), shoppingCartItem.getProduct().getUser().getUsername());
    }

    public static final ShoppingCartDto toShoppingCartDto(ShoppingCart shoppingCart) {
        return new ShoppingCartDto(shoppingCart.getId(), shoppingCart.getUser().getId(),
                shoppingCart.getUser().getEmail(), toShoppingCartItemsDto(shoppingCart.getItems()),
                shoppingCart.getTotalQuantity(), shoppingCart.getTotalPrice());
    }
}
