package es.udc.tfg.aga.backend.model.entities;
import java.util.List;

import org.springframework.data.domain.Page;

public interface CustomOrderDao {

    List<Order> findOrderCompleted(Long userId, String sessionId);
    Page<Order> findOrders(Long userId, Integer month, Integer year, Boolean asc, Boolean buyer, int page, int size);
    // Page<Order> findSells(Long userId, Integer month, Integer year, Boolean asc, int page, int size);
    
}
