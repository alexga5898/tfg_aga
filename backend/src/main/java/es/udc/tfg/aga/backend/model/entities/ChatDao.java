package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ChatDao extends PagingAndSortingRepository<Chat, Long>, CustomChatDao{
    Page<Chat> findByBuyerIdOrderByLastModifiedDesc(Long buyerId, Pageable pageable);
    Page<Chat> findBySellerIdOrderByLastModifiedDesc(Long sellerId, Pageable pageable);
    @Query("SELECT c FROM Chat c WHERE c.buyer.id = ?1 OR c.seller.id = ?1 ORDER BY c.lastModified DESC")
    Page<Chat> findChats(Long userId, Pageable pageable);
}
