package es.udc.tfg.aga.backend.model.entities;

import java.util.List;

public class Block<T> {
    private List<T> items;
    private long totalItems;
    private long totalPages;
    private boolean existMoreItems;

    public Block() {

    }

    public Block(List<T> items, long totalItems, int totalPages, boolean existMoreItems) {
        this.items = items;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.existMoreItems = existMoreItems;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public boolean getExistMoreItems() {
        return existMoreItems;
    }

    public void setExistMoreItems(boolean existMoreItems) {
        this.existMoreItems = existMoreItems;
    }

}
