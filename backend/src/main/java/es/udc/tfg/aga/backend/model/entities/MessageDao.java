package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageDao extends PagingAndSortingRepository<Message, Long>, CustomMessageDao{
    Slice<Message> findByChatIdOrderByDateDesc(Long chatId, Pageable pageable);
}
