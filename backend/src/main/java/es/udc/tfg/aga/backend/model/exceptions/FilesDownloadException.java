package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class FilesDownloadException extends Exception {
    String fileName;

    public FilesDownloadException(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
    
}
