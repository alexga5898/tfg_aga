package es.udc.tfg.aga.backend.rest.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import es.udc.tfg.aga.backend.rest.common.jwt.JwtGenerator;
import es.udc.tfg.aga.backend.rest.common.jwt.JwtFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtGenerator jwtGenerator;

    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilter(new JwtFilter(authenticationManager(), jwtGenerator)).authorizeRequests()
                .antMatchers(HttpMethod.POST, "/users/sign").permitAll()
                .antMatchers(HttpMethod.POST, "/users/logIn").permitAll()
                .antMatchers(HttpMethod.POST, "/users/logInFromServiceToken").permitAll()
                .antMatchers(HttpMethod.POST, "/users/profile").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/shopping/products/newProduct").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/products/myProducts").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/products/images/*").permitAll()
                .antMatchers(HttpMethod.GET, "/products/*/thumbnail").permitAll()
                .antMatchers(HttpMethod.POST, "/shopping/products/myProducts/*").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/products/*").permitAll()
                .antMatchers(HttpMethod.DELETE, "/shopping/products/myProducts/*").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/products").permitAll()
                .antMatchers(HttpMethod.POST, "/shopping/shoppingCart/*/add").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/shopping/shoppingCart/*/update").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/shopping/shoppingCart/*/delete").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/shopping/shoppingCart/checkout").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/stripe/webhook").permitAll()
                .antMatchers(HttpMethod.POST, "/users/stripe/connect").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/shopping/orders").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/shopping/orders/*").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/shopping/ratings/addRating").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/shopping/ratings").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/shopping/ratings/*").permitAll()
                .antMatchers(HttpMethod.GET, "/products/user/*").permitAll()
                .antMatchers(HttpMethod.GET, "/users/chats").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/users/chats/find/*/*").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/users/chats/newMessages").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/users/chats/*").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/users/chats/*/newMessages").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/users/chats/*/changeStatus").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/users/chats/*/messages").hasRole("USER")
                .antMatchers("/websocket").permitAll()
                .antMatchers("/websocket/**/**/**").permitAll()
                .anyRequest().denyAll();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration config = new CorsConfiguration();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        source.registerCorsConfiguration("/**", config);

        return source;

    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
