package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class FileException extends Exception{
    private String fileName;

    public FileException(String fileName){
        this.fileName = fileName;
    }

    public Object getFileName(){
        return fileName;
    }
}
