package es.udc.tfg.aga.backend.model.entities;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class CustomOrderDaoImpl implements CustomOrderDao {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Order> findOrderCompleted(Long userId, String sessionId) {
        String queryString = "SELECT o FROM Order o";

        if (userId != null || sessionId != null) {
            queryString += " WHERE ";
            if (userId != null) {
                queryString += "o.buyer.id = :userId";
            }
            if (sessionId != null) {
                if (userId != null) {
                    queryString += " AND ";
                }
                queryString += "o.sessionId = :sessionId";
            }
        }
        queryString += " ORDER BY o.date DESC";

        Query query = entityManager.createQuery(queryString);

        if (userId != null) {
            query.setParameter("userId", userId);
        }

        if (sessionId != null) {
            query.setParameter("sessionId", sessionId);
        }

        List<Order> orders = query.getResultList();

        return orders;
    }

    // @SuppressWarnings("unchecked")
    // @Override
    // public Page<Order> findOrders(Long userId, Integer month, Integer year, Boolean asc, int page,
    //         int size) {
    //     String queryString = "SELECT o FROM Order o";
    //     String countQueryString = "SELECT COUNT(o) FROM Order o";

    //     if (userId != null || month != null || year != null) {
    //         queryString += " WHERE ";
    //         countQueryString += " WHERE ";
    //         if (userId != null) {
    //             queryString += "o.buyer.id = :userId";
    //             countQueryString += "o.buyer.id = :userId";
    //         }
    //         if (month != null) {
    //             if (userId != null) {
    //                 queryString += " AND ";
    //                 countQueryString += " AND ";
    //             }
    //             queryString += "MONTH(o.date) = :month";
    //             countQueryString += "MONTH(o.date) = :month";
    //         }
    //         if (year != null) {
    //             if (userId != null || month != null) {
    //                 queryString += " AND ";
    //                 countQueryString += " AND ";
    //             }
    //             queryString += "YEAR(o.date) = :year";
    //             countQueryString += "YEAR(o.date) = :year";
    //         }
    //     }
    //     queryString += " ORDER BY o.date";
    //     if (asc) {
    //         queryString += " ASC";
    //     } else {
    //         queryString += " DESC";
    //     }

    //     Query query = entityManager.createQuery(queryString).setFirstResult(page * size).setMaxResults(size + 1);
    //     Query countQuery = entityManager.createQuery(countQueryString);

    //     if (userId != null) {
    //         query.setParameter("userId", userId);
    //         countQuery.setParameter("userId", userId);
    //     }
    //     if (month != null) {
    //         query.setParameter("month", month);
    //         countQuery.setParameter("month", month);
    //     }
    //     if (year != null) {
    //         query.setParameter("year", year);
    //         countQuery.setParameter("year", year);
    //     }
        
    //     List<Order> orders = query.getResultList();

    //     long totalOrders = (long) countQuery.getResultList().get(0);

    //     boolean hasNext = orders.size() == (size + 1);

    //     if (hasNext) {
    //         orders.remove(orders.size() - 1);
    //     }

    //     return new PageImpl<>(orders, PageRequest.of(page, size), totalOrders);
    // }

    @SuppressWarnings("unchecked")
    @Override
    public Page<Order> findOrders(Long userId, Integer month, Integer year, Boolean asc, Boolean buyer, int page,
            int size) {
        String queryString = "SELECT o FROM Order o";
        String countQueryString = "SELECT COUNT(o) FROM Order o";

        if (userId != null || month != null || year != null) {
            queryString += " WHERE ";
            countQueryString += " WHERE ";
            if (userId != null) {
                if (buyer) {
                    queryString += "o.buyer.id = :userId";
                    countQueryString += "o.buyer.id = :userId";
                } else {
                    queryString += "o.seller.id = :userId";
                    countQueryString += "o.seller.id = :userId";
                }
                
            }
            if (month != null) {
                if (userId != null) {
                    queryString += " AND ";
                    countQueryString += " AND ";
                }
                queryString += "MONTH(o.date) = :month";
                countQueryString += "MONTH(o.date) = :month";
            }
            if (year != null) {
                if (userId != null || month != null) {
                    queryString += " AND ";
                    countQueryString += " AND ";
                }
                queryString += "YEAR(o.date) = :year";
                countQueryString += "YEAR(o.date) = :year";
            }
        }
        queryString += " ORDER BY o.date";
        if (asc) {
            queryString += " ASC";
        } else {
            queryString += " DESC";
        }

        Query query = entityManager.createQuery(queryString).setFirstResult(page * size).setMaxResults(size + 1);
        Query countQuery = entityManager.createQuery(countQueryString);

        if (userId != null) {
            query.setParameter("userId", userId);
            countQuery.setParameter("userId", userId);
        }
        if (month != null) {
            query.setParameter("month", month);
            countQuery.setParameter("month", month);
        }
        if (year != null) {
            query.setParameter("year", year);
            countQuery.setParameter("year", year);
        }
        
        List<Order> orders = query.getResultList();

        long totalOrders = (long) countQuery.getResultList().get(0);

        boolean hasNext = orders.size() == (size + 1);

        if (hasNext) {
            orders.remove(orders.size() - 1);
        }

        return new PageImpl<>(orders, PageRequest.of(page, size), totalOrders);
    }
}
