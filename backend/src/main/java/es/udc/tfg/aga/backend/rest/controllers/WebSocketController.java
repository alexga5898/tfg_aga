package es.udc.tfg.aga.backend.rest.controllers;

import es.udc.tfg.aga.backend.model.entities.Message;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.model.UserService;
import es.udc.tfg.aga.backend.rest.dtos.MessageConversor;
import es.udc.tfg.aga.backend.rest.dtos.MessageParamsDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

@Controller
public class WebSocketController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/sendMessage")
    public void sendMessage(@Payload @Validated MessageParamsDto params, MessageHeaderAccessor headerAccessor)
            throws InstanceNotFoundException {
        UsernamePasswordAuthenticationToken header = (UsernamePasswordAuthenticationToken) headerAccessor
                .getMessageHeaders().get("simpUser");
        Long userId = (Long) header.getPrincipal();
        Message message = userService.sendMessage(userId, params.getRecipientId(), params.getProductId(), params.getContent());
        simpMessagingTemplate.convertAndSendToUser(params.getRecipientId().toString(), "/reply", MessageConversor.toNotificationDto(message));
        simpMessagingTemplate.convertAndSendToUser(userId.toString(), "/reply", MessageConversor.toNotificationDto(message));
    }

}