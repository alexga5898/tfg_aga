package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ShoppingCartItemDao extends PagingAndSortingRepository<ShoppingCartItem, Long> {
    Slice<ShoppingCartItem> findByShoppingCartId(Long shoppingCartId, Pageable pageable);
    void deleteByShoppingCartId(Long shoppingCartId);
}
