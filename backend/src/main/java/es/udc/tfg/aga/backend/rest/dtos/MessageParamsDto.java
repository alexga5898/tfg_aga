package es.udc.tfg.aga.backend.rest.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MessageParamsDto {
    private Long productId;
    private Long recipientId;
    private String content;

    public MessageParamsDto() {

    }

    public MessageParamsDto(Long productId, Long recipientId, String content) {
        this.productId = productId;
        this.recipientId = recipientId;
        this.content = content;
    }

    @NotNull
    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @NotNull
    @Size(min = 1, max = 300)
    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @NotNull
    public Long getRecipientId() {
        return this.recipientId;
    }

    public void setRecipientId(Long recipientId) {
        this.recipientId = recipientId;
    }
   
}
