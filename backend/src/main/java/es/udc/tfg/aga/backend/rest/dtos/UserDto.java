package es.udc.tfg.aga.backend.rest.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

public class UserDto {

    public interface Validations {
    }

    public interface UpdateValidations {
    }

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private ShoppingCartDto shoppingCartDto;
    private Boolean activeTransfers;
    private String role;
  
    public UserDto() {

    }

    public UserDto(Long id, String username, String firstName, String lastName, String email, String role,
            ShoppingCartDto shoppingCartDto, Boolean activeTransfers) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.shoppingCartDto = shoppingCartDto;
        this.activeTransfers = activeTransfers;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(min = 3, max = 20, groups = { Validations.class, UpdateValidations.class })
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(min = 1, max = 40, groups = { Validations.class, UpdateValidations.class })
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(min = 1, max = 40, groups = { Validations.class, UpdateValidations.class })
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(max = 40, groups = { Validations.class, UpdateValidations.class })
    @Email(groups = { Validations.class, UpdateValidations.class })
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @NotNull(groups = { Validations.class })
    @Size(min = 6, max = 30, groups = { Validations.class })
    @JsonProperty(access = Access.WRITE_ONLY)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActiveTransfers() {
        return this.activeTransfers;
    }

    public void setActiveTransfers(Boolean activeTransfers) {
        this.activeTransfers = activeTransfers;
    }
    

    @JsonProperty(access = Access.WRITE_ONLY)
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ShoppingCartDto getShoppingCartDto() {
        return this.shoppingCartDto;
    }

    public void setShoppingCartDto(ShoppingCartDto shoppingCartDto) {
        this.shoppingCartDto = shoppingCartDto;
    }

}
