package es.udc.tfg.aga.backend.rest.dtos;

public class RatingPageDto<T> {
    private PageDto<T> ratings;
    private double avg;

    public RatingPageDto() {
    }

    public RatingPageDto(PageDto<T> ratings, double avg) {
        this.ratings = ratings;
        this.avg = avg;
    }

    public PageDto<T> getRatings() {
        return this.ratings;
    }

    public void setRatings(PageDto<T> ratings) {
        this.ratings = ratings;
    }

    public double getAvg() {
        return this.avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

}
