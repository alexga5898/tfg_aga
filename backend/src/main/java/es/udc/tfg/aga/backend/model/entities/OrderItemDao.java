package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderItemDao extends PagingAndSortingRepository<OrderItem, Long> {
    Optional<OrderItem> findByOrderId(Long orderId);
    boolean existsByProductId(Long productId);
}
