package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.stripe.exception.StripeException;
import com.stripe.model.LineItem;
import com.stripe.model.Product;

import es.udc.tfg.aga.backend.model.entities.Order;
import es.udc.tfg.aga.backend.model.entities.OrderItem;

public class OrderConversor {

    public static final List<OrderDto> toOrderDtos(List<Order> orders) {
        List<OrderDto> orderDtos = orders.stream().map(i -> OrderConversor.toOrderDto(i)).collect(Collectors.toList());
        return orderDtos;
    }

    public static final List<OrderDetailsDto> toOrderDetailsDtos(List<Order> orders) {
        List<OrderDetailsDto> orderDtos = orders.stream().map(i -> OrderConversor.toOrderDetailsDto(i))
                .collect(Collectors.toList());
        return orderDtos;
    }

    public static final List<OrderItemDto> toOrderItemDtos(Set<OrderItem> orderItems) {
        List<OrderItemDto> orderItemDtos = orderItems.stream().map(i -> OrderConversor.toOrderItemDto(i))
                .collect(Collectors.toList());
        return orderItemDtos;
    }

    public static final List<BuySuccessItemDto> toBuySuccessItemsDtos(List<LineItem> items) throws StripeException {
        List<BuySuccessItemDto> buySuccessItemsDtos = new ArrayList<>();
        for (LineItem item : items) {
            buySuccessItemsDtos.add(OrderConversor.toBuySuccessItemDto(item));
        }
        // List<BuySuccessItemDto> buySuccessItemsDtos = items.stream().map(i ->
        // OrderConversor.toBuySuccessItemDto(i))
        // .collect(Collectors.toList());
        return buySuccessItemsDtos;
    }

    public static final OrderDto toOrderDto(Order order) {
        return new OrderDto(order.getId(), order.getSeller().getUsername(), order.getBuyer().getUsername(),
                toOrderItemDtos(order.getItems()), order.getTotalPrice(), order.getDate(), order.getRating() != null ? true : false);
    }

    public static final OrderDetailsDto toOrderDetailsDto(Order order) {
        return new OrderDetailsDto(order.getId(), toOrderItemDtos(order.getItems()), order.getSeller().getUsername(),
                order.getBuyer().getUsername(), order.getTotalPrice(),
                AddressConversor.toAddressDto(order.getAddress()), order.getDate());
    }

    public static final OrderItemDto toOrderItemDto(OrderItem orderItem) {
        return new OrderItemDto(orderItem.getId(), orderItem.getProduct().getId(), orderItem.getProduct().getName(),
                orderItem.getQuantity(), orderItem.getPrice());
    }

    public static final BuySuccessItemDto toBuySuccessItemDto(LineItem item) throws StripeException {
        Product product = Product.retrieve(item.getPrice().getProduct());
        return new BuySuccessItemDto(Long.parseLong(product.getMetadata().get("productId")), product.getName(),
                item.getQuantity().intValue(), item.getPrice().getUnitAmountDecimal().divide(new BigDecimal(100)));
    }

}
