package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class InvalidQuantityException extends Exception{
    private String field;
    private int min;
    private int max;

    public InvalidQuantityException(String field, int min, int max){
        this.min = min;
        this.field = field;
        this.max = max;
    }

    public String getField() {
        return field;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
