package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.util.List;

public class BuySuccessDto {
    private List<BuySuccessItemDto> items;
    private BigDecimal total;
    private AddressDto address;

    public BuySuccessDto() {
    }

    public BuySuccessDto(List<BuySuccessItemDto> items, BigDecimal total, AddressDto address) {
        this.items = items;
        this.total = total;
        this.address = address;
    }

    public List<BuySuccessItemDto> getItems() {
        return this.items;
    }

    public void setItems(List<BuySuccessItemDto> items) {
        this.items = items;
    }

    public BigDecimal getTotal() {
        return this.total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public AddressDto getAddress() {
        return this.address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

}