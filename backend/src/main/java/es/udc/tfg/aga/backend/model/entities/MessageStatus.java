package es.udc.tfg.aga.backend.model.entities;

public enum MessageStatus {
    DELIVERED, RECEIVED
}
