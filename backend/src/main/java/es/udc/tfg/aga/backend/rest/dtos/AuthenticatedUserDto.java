package es.udc.tfg.aga.backend.rest.dtos;

public class AuthenticatedUserDto {
    private String serviceToken;
    private UserDto userDto;  
    // private List<AddressDto> addressDto;

    public AuthenticatedUserDto(){

    }

    public AuthenticatedUserDto(String serviceToken, UserDto userDto){
        this.serviceToken = serviceToken;
        this.userDto = userDto;
        // this.addressDto = addressDto;
    }

    public UserDto getUserDto() {
        return this.userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public String getServiceToken() {
        return this.serviceToken;
    }

    public void setServiceToken(String serviceToken) {
        this.serviceToken = serviceToken;
    }

    // public List<AddressDto> getAddressDto() {
    //     return addressDto;
    // }
    
    // public void setAddressDto(List<AddressDto> addressDto) {
    //     this.addressDto = addressDto;
    // }
}

