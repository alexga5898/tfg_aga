package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.util.List;

public class ShoppingCartDto {
    private Long id;
    private Long userId;
    private String userEmail;
    private List<ShoppingCartItemDto> items;
    private int totalQuantity;
    private BigDecimal totalPrice;

    public ShoppingCartDto() {
    }

    public ShoppingCartDto(Long id, Long userId, String userEmail, List<ShoppingCartItemDto> items, int totalQuantity,
            BigDecimal totalPrice) {
        this.id = id;
        this.userId = userId;
        this.userEmail = userEmail;
        this.items = items;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return this.userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<ShoppingCartItemDto> getItems() {
        return this.items;
    }

    public void setItems(List<ShoppingCartItemDto> items) {
        this.items = items;
    }

    public int getTotalQuantity() {
        return this.totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

}