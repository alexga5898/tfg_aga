package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;

public class PageDto<T> {
    private List<T> items;
    private long totalItems;
    private long totalPages;
    private boolean existMoreItems;

    public PageDto() {

    }

    public PageDto(List<T> items, long totalItems, long totalPages, boolean existMoreItems) {
        this.items = items;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.existMoreItems = existMoreItems;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    // public List<byte[]> getImages() {
    //     return images;
    // }

    // public void setImages(List<byte[]> images) {
    //     this.images = images;
    // }

    public boolean getExistMoreItems() {
        return existMoreItems;
    }

    public void setExistMoreItems(boolean existMoreItems) {
        this.existMoreItems = existMoreItems;
    }
}
