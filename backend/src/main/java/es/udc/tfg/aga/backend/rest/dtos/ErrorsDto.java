package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;

public class ErrorsDto {
    private String error;
    private List<FieldErrorDto> fieldErrors;

    public ErrorsDto(){

    }

    public ErrorsDto(String error){
        this.error = error;
    }

    public ErrorsDto(List<FieldErrorDto> fieldErrors){
        this.fieldErrors = fieldErrors;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<FieldErrorDto> getFieldErrors() {
        return this.fieldErrors;
    }

    public void setFieldErrors(List<FieldErrorDto> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

}
