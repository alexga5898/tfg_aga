package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class FileUploadException extends FileException {
    
    public FileUploadException(String fileName) {
        super(fileName);
    }
    
}