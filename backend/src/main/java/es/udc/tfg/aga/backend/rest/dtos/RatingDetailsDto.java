package es.udc.tfg.aga.backend.rest.dtos;

import java.time.LocalDateTime;

public class RatingDetailsDto {
    private Long id;
    private String buyerUsername;
    private String sellerUsername;
    private Long orderId;
    private LocalDateTime date;
    private int rate;
    private String comment;

    public RatingDetailsDto() {

    }

    public RatingDetailsDto(Long id, String buyerUsername, String sellerUsername, Long orderId, LocalDateTime date, int rate,
            String comment) {
        this.id = id;
        this.buyerUsername = buyerUsername;
        this.sellerUsername = sellerUsername;
        this.orderId = orderId;
        this.date = date;
        this.rate = rate;
        this.comment = comment;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBuyerUsername() {
        return this.buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getSellerUsername() {
        return this.sellerUsername;
    }

    public void setSellerName(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getRate() {
        return this.rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
