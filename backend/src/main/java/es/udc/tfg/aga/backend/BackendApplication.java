package es.udc.tfg.aga.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import es.udc.tfg.aga.backend.rest.common.amazon.Email;
import es.udc.tfg.aga.backend.rest.common.jwt.JwtGenerator;
import es.udc.tfg.aga.backend.rest.common.stripe.ConnectAccount;
import es.udc.tfg.aga.backend.rest.common.stripe.Payment;

@SpringBootApplication
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JwtGenerator jwtGenerator() {
		return new JwtGenerator();
	}

    @Bean 
    public Email email() {
        return new Email();
    }

    @Bean 
    public Payment payment() {
        return new Payment();
    }

    @Bean 
    public ConnectAccount connect() {
        return new ConnectAccount();
    }

	@Bean
	public MessageSource messageSource() {
		var bean = new ReloadableResourceBundleMessageSource();

		bean.setBasename("classpath:messages");
		bean.setDefaultEncoding("UTF-8");

		return bean;
	}

	@Bean
	public LocalValidatorFactoryBean getValidator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}
	
	
}
