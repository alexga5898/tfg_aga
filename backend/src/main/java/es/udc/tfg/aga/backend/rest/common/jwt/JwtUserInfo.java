package es.udc.tfg.aga.backend.rest.common.jwt;

public class JwtUserInfo {
    private Long userId;
    private String username;
    private String role;

    public JwtUserInfo(){

    }

    public JwtUserInfo(Long userId, String username, String role){
        this.userId = userId;
        this.username = username;
        this.role = role;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
}
