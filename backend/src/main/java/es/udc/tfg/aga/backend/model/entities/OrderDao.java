package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderDao extends PagingAndSortingRepository<Order, Long>, CustomOrderDao{
    Slice<Order> findBySellerIdOrderByDateDesc(Long sellerId, Pageable pageable);
    Slice<Order> findByBuyerIdOrderByDateDesc(Long buyerId, Pageable pageable);
}
