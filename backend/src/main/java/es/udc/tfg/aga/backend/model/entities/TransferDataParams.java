package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;

public class TransferDataParams {
    private String stripeAccId;
    private BigDecimal amount;
    private Long orderId;
    private String sellerEmail;

    public TransferDataParams() {

    }

    public TransferDataParams(String stripeAccId, BigDecimal amount, Long orderId, String sellerEmail) {
        this.stripeAccId = stripeAccId;
        this.amount = amount;
        this.orderId = orderId;
        this.sellerEmail = sellerEmail;
    }

    public String getStripeAccId() {
        return this.stripeAccId;
    }

    public void setStripeAccId(String stripeAccId) {
        this.stripeAccId = stripeAccId;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getSellerEmail() {
        return this.sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

}
