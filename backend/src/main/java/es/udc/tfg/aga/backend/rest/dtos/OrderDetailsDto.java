package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class OrderDetailsDto {
    private Long id;
    private List<OrderItemDto> items;
    private String sellerUsername;
    private String buyerUsername;
    private BigDecimal totalPrice;
    private AddressDto buyerAddressDto;
    private LocalDateTime date;

    public OrderDetailsDto() {
    }

    public OrderDetailsDto(Long id, List<OrderItemDto> items, String sellerUsername, String buyerUsername,
            BigDecimal totalPrice, AddressDto buyerAddressDto, LocalDateTime date) {
        this.id = id;
        this.items = items;
        this.sellerUsername = sellerUsername;
        this.buyerUsername = buyerUsername;
        this.totalPrice = totalPrice;
        this.buyerAddressDto = buyerAddressDto;
        this.date = date;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<OrderItemDto> getItems() {
        return this.items;
    }

    public void setItems(List<OrderItemDto> items) {
        this.items = items;
    }

    public String getSellerUsername() {
        return this.sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getBuyerUsername() {
        return this.buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public AddressDto getBuyerAddressDto() {
        return this.buyerAddressDto;
    }

    public void setBuyerAddressDto(AddressDto buyerAddressDto) {
        this.buyerAddressDto = buyerAddressDto;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}