package es.udc.tfg.aga.backend.rest.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AddToOrUpdateItemCartParamsDto {
    private Long productId;
    private int quantity;

    public AddToOrUpdateItemCartParamsDto() {
    }

    public AddToOrUpdateItemCartParamsDto(Long productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    @NotNull
    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Min(value=1)
    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
