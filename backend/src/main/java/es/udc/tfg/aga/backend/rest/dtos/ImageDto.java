package es.udc.tfg.aga.backend.rest.dtos;

public class ImageDto {
    private Long id;
    private Long productId;
    private String name;
    private String link;

    public interface Validations {

    }

    public interface UpdateValidations {

    }

    public ImageDto() {

    }

    public ImageDto(Long id, Long productId, String name, String link) {
        this.id = id;
        this.productId = productId;
        this.name = name;
        this.link = link;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
