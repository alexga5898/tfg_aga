package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ShoppingCartDao extends PagingAndSortingRepository<ShoppingCart, Long> {  
    ShoppingCart findByUserId(Long userId);
}
