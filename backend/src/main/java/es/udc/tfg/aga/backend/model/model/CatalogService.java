package es.udc.tfg.aga.backend.model.model;

import java.math.BigDecimal;

import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.exceptions.FilesDownloadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;

public interface CatalogService {

    Block<Product> getUserProducts(Long userId, String keywords, int page, int size);

    byte[] getImage(Long imageId) throws InstanceNotFoundException, FilesDownloadException;

    byte[] getThumbnail(Long productId) throws InstanceNotFoundException, FilesDownloadException;

    Product getProduct(Long productId) throws InstanceNotFoundException;

    Block<Product> getProducts(String keywords, String category, String condition, BigDecimal lPrice, BigDecimal hPrice,
            int order, int page, int size) throws InstanceNotFoundException;

    Block<Product> getProductsByUsername(String username, int page, int size);
}
