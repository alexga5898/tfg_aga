package es.udc.tfg.aga.backend.rest.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import com.stripe.exception.StripeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PaymentIntentException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.rest.dtos.ErrorsDto;
import es.udc.tfg.aga.backend.rest.dtos.FieldErrorDto;

@ControllerAdvice
public class CommonControllerAdvice {

    @Autowired
    private MessageSource messageSource;

    private String DUPLICATE_INSTANCE_EXCEPTION = "exceptions.DuplicateInstanceException";
    private String INSTANCE_NOT_FOUND_EXCEPTION = "exceptions.InstanceNotFoundException";
    private String INVALID_QUANTITY_EXCEPTION = "exceptions.InvalidQuantityException";
    private String CONSTRAINT_VIOLATION_EXCEPTION = "exceptions.ConstraintViolationException";
    private String PERMISSION_EXCEPTION = "exceptions.PermissionException";
    private String STRIPE_EXCEPTION = "exceptions.StripeException";
    private String PAYMENT_INTENT_EXCEPTION = "exceptions.PaymentIntentException";

    @ExceptionHandler(DuplicateInstanceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleDuplicateInstanceException(DuplicateInstanceException e, Locale locale) {
        String nameMessage = messageSource.getMessage(e.getName(), null, e.getName(), locale);
        String message = messageSource.getMessage(DUPLICATE_INSTANCE_EXCEPTION,
                new Object[] { nameMessage, e.getObject().toString() }, DUPLICATE_INSTANCE_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(InstanceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorsDto handleInstanceNotFoundException(InstanceNotFoundException e, Locale locale) {
        String nameMessage = messageSource.getMessage(e.getName(), null, e.getName(), locale);
        String message = messageSource.getMessage(INSTANCE_NOT_FOUND_EXCEPTION,
                new Object[] { nameMessage, e.getObject().toString() }, INSTANCE_NOT_FOUND_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleInvalidArgumentException(MethodArgumentNotValidException e) {
        List<FieldErrorDto> errors = e.getBindingResult().getFieldErrors().stream()
                .map(error -> new FieldErrorDto(error.getField(), error.getDefaultMessage()))
                .collect(Collectors.toList());

        return new ErrorsDto(errors);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleConstraintViolationException(ConstraintViolationException e, Locale locale) {
        String errors = e.getMessage();
        String parts[] = errors.split(", ");
        List<FieldErrorDto> fieldErrorsDto = new ArrayList<FieldErrorDto>();
        for (String part : parts) {
            String[] error = part.split(":");
            fieldErrorsDto.add(new FieldErrorDto(error[0].split("\\.")[1], error[1]));
        }
        return new ErrorsDto(fieldErrorsDto);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, Locale locale) {
        String field = messageSource.getMessage("product.fields." + e.getName(), null, "product.fields." + e.getName(),
                locale);
        String message = messageSource.getMessage(CONSTRAINT_VIOLATION_EXCEPTION, new Object[] { field },
                CONSTRAINT_VIOLATION_EXCEPTION, locale);
        return new ErrorsDto(message);
    }

    @ExceptionHandler(InvalidQuantityException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleInvalidQuantityException(InvalidQuantityException e, Locale locale) {
        String nameMessage = messageSource.getMessage(e.getField(), null, e.getField(), locale);
        String message = messageSource.getMessage(INVALID_QUANTITY_EXCEPTION,
                new Object[] { nameMessage, e.getMin(), e.getMax() }, INVALID_QUANTITY_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(PermissionException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsDto handlerPermissionException(PermissionException e, Locale locale) {
        String message = messageSource.getMessage(PERMISSION_EXCEPTION, new Object[] {},
                PERMISSION_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(StripeException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @ResponseBody
    public ErrorsDto handleIllegalStateException(StripeException e, Locale locale) {
        String message = messageSource.getMessage(STRIPE_EXCEPTION,
                new Object[] {e.getMessage()}, STRIPE_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(PaymentIntentException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @ResponseBody
    public ErrorsDto handlePaymentIntentException(PaymentIntentException e, Locale locale) {
        String message = messageSource.getMessage(PAYMENT_INTENT_EXCEPTION, new Object[] {}, PAYMENT_INTENT_EXCEPTION,
                locale);

        return new ErrorsDto(message);
    }

}
