package es.udc.tfg.aga.backend.rest.dtos;

import javax.validation.constraints.NotNull;

public class DeleteItemCartParamsDto {
    private Long productId;

    public DeleteItemCartParamsDto() {
    }

    public DeleteItemCartParamsDto(Long productId) {
        this.productId = productId;
    }

    @NotNull
    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

}
