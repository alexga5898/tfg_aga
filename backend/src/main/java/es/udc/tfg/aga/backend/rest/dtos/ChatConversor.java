package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Chat;

public class ChatConversor {
    private ChatConversor() {

    }

    public static final List<ChatDto> toChatsDto(List<Chat> chats) {
        List<ChatDto> chatsDto = chats.stream().map(i -> ChatConversor.toChatDto(i)).collect(Collectors.toList());
        return chatsDto;
    }

    public static final ChatDto toChatDto(Chat chat) {
        return new ChatDto(chat.getId(), chat.getBuyer().getId(), chat.getBuyer().getUsername(),
                chat.getSeller().getId(), chat.getSeller().getUsername(), chat.getProduct().getId(),
                chat.getProduct().getName(), chat.getProduct().getPrice(),chat.getLastModified());
    }
}
