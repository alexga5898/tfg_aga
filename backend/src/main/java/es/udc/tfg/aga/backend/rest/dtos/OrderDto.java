package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class OrderDto {
    private Long id;
    private String sellerUsername;
    private String buyerUsername;
    private List<OrderItemDto> items;
    private BigDecimal totalPrice;
    private LocalDateTime date;
    private Boolean rated;

    public OrderDto() {
    }

    public OrderDto(Long id, String sellerUsername, String buyerUsername, List<OrderItemDto> items,
            BigDecimal totalPrice, LocalDateTime date, Boolean rated) {
        this.id = id;
        this.sellerUsername = sellerUsername;
        this.buyerUsername = buyerUsername;
        this.items = items;
        this.totalPrice = totalPrice;
        this.date = date;
        this.rated = rated;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSellerUsername() {
        return this.sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getBuyerUsername() {
        return this.buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public List<OrderItemDto> getItems() {
        return this.items;
    }

    public void setItems(List<OrderItemDto> items) {
        this.items = items;
    }

    public BigDecimal getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Boolean getRated() {
        return this.rated;
    }

    public void setRated(Boolean rated) {
        this.rated = rated;
    }
    
}