package es.udc.tfg.aga.backend.rest.dtos;

import java.time.LocalDateTime;

public class MessageDto {
    private Long id;
    private Long chatId;
    private Long senderId;
    private Long productId;
    private String content;
    private LocalDateTime date;

    public MessageDto() {

    }

    public MessageDto(Long id, Long chatId, Long senderId, Long productId, String content, LocalDateTime date) {
        this.id = id;
        this.chatId = chatId;
        this.senderId = senderId;
        this.productId = productId;
        this.content = content;
        this.date = date;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChatId() {
        return this.chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Long getSenderId() {
        return this.senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getContent() {
        return this.content;
    }

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    
}
