package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

public interface CustomChatDao {
    Optional<Chat> findChat(Long senderId, Long recipientId, Long productId);
}
