package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProductDto {

    public interface Validations {

    }

    public interface UpdateValidations {

    }

    private Long id;
    private Long sellerId;
    private String sellerUsername;
    private String name;
    private BigDecimal price;
    private int quantity;
    private List<ImageDto> images;
    private String condition;
    private String category;
    private String description;
    private LocalDateTime date;

    public ProductDto() {

    }

    public ProductDto(Long id, Long sellerId, String sellerUsername, String name, BigDecimal price, int quantity, List<ImageDto> images, String condition,
            String category, String description, LocalDateTime date) {
        this.id = id;
        this.sellerId = sellerId;
        this.sellerUsername = sellerUsername;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.images = images;
        this.condition = condition;
        this.category = category;
        this.description = description;
        this.date = date;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerId() {
        return this.sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }    

    public String getSellerUsername() {
        return this.sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(min = 1, max = 20, groups = { Validations.class, UpdateValidations.class })
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @DecimalMin(value = "0.01", groups = { Validations.class, UpdateValidations.class })
    @Max(value = 999999, groups = { Validations.class, UpdateValidations.class })
    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Min(value = 1, groups = { Validations.class, UpdateValidations.class })
    @Max(value = 999999, groups = { Validations.class, UpdateValidations.class })
    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<ImageDto> getImages() {
        return this.images;
    }

    public void setImages(List<ImageDto> images) {
        this.images = images;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    public String getCondition() {
        return this.condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @NotNull(groups = { Validations.class, UpdateValidations.class })
    @Size(min = 1, max = 800, groups = { Validations.class, UpdateValidations.class })
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}
