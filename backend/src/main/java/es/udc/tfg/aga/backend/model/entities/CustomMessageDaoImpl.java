package es.udc.tfg.aga.backend.model.entities;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CustomMessageDaoImpl implements CustomMessageDao {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Message> findByRecipientSenderProductStatus(Long recipientId, Long chatId) {
        String queryString = "SELECT m FROM Message m WHERE";

        if (recipientId != null) {
            queryString += " m.recipient.id = :recipientId AND";
        }

        if (chatId != null) {
            queryString += " m.chat.id = :chatId AND";

        }

        queryString += " m.status = 'DELIVERED'";

        Query query = entityManager.createQuery(queryString);

        if (recipientId != null) {
            query.setParameter("recipientId", recipientId);
        }

        if (chatId != null) {
            query.setParameter("chatId", chatId);
        }

        List<Message> messages = query.getResultList();

        return messages;

    }

    @Override
    public Boolean checkByRecipientChatStatus(Long recipientId, Long chatId) {
        String queryString = "SELECT m FROM Message m WHERE";

        if (recipientId != null) {
            queryString += " m.recipient.id = :recipientId AND";
        }

        if (chatId != null) {
            queryString += " m.chat.id = :chatId AND";

        }

        // if (senderId != null) {
        // queryString += " m.sender.id = :senderId AND";

        // }

        // if (productId != null) {
        // queryString += " m.product.id = :productId AND";
        // }

        queryString += " m.status = 'DELIVERED' ORDER BY NULL";

        Query query = entityManager.createQuery(queryString).setMaxResults(1);

        if (recipientId != null) {
            query.setParameter("recipientId", recipientId);
        }

        if (chatId != null) {
            query.setParameter("chatId", chatId);
        }

        // if (senderId != null) {
        // query.setParameter("senderId", senderId);
        // }

        // if (productId != null) {
        // query.setParameter("productId", productId);
        // }

        Boolean exists = !query.getResultList().isEmpty();

        return exists;

    }

}
