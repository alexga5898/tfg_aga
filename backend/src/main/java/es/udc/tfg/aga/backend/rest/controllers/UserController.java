package es.udc.tfg.aga.backend.rest.controllers;

import java.net.URI;
import java.util.Locale;

import com.stripe.exception.StripeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.IncorrectLogInException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;
import es.udc.tfg.aga.backend.model.model.UserService;
import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Chat;
import es.udc.tfg.aga.backend.model.entities.Message;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.rest.common.jwt.JwtGenerator;
import es.udc.tfg.aga.backend.rest.common.jwt.JwtUserInfo;
import es.udc.tfg.aga.backend.rest.common.stripe.ConnectAccount;
import es.udc.tfg.aga.backend.rest.dtos.AuthenticatedUserDto;
import es.udc.tfg.aga.backend.rest.dtos.ChatConversor;
import es.udc.tfg.aga.backend.rest.dtos.ChatDto;
import es.udc.tfg.aga.backend.rest.dtos.ErrorsDto;
import es.udc.tfg.aga.backend.rest.dtos.LoginParamsDto;
import es.udc.tfg.aga.backend.rest.dtos.MessageConversor;
import es.udc.tfg.aga.backend.rest.dtos.MessageDto;
import es.udc.tfg.aga.backend.rest.dtos.PageDto;
import es.udc.tfg.aga.backend.rest.dtos.SliceDto;
import es.udc.tfg.aga.backend.rest.dtos.StripeConnectDto;
import es.udc.tfg.aga.backend.rest.dtos.UserConversor;
import es.udc.tfg.aga.backend.rest.dtos.UserDto;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtGenerator jwtGenerator;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ConnectAccount connect;

    private String INCORRECT_LOGIN_EXCEPTION = "exceptions.IncorrectLogInException";
    private String STRIPE_ACCOUNT_MISSING_EXCEPTION = "exception.StripeAccountMissingException";

    @ExceptionHandler(IncorrectLogInException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleIncorrectLogInException(IncorrectLogInException e, Locale locale) {
        String message = messageSource.getMessage(INCORRECT_LOGIN_EXCEPTION, null, INCORRECT_LOGIN_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(StripeAccMissingException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsDto handleStripeAccMissingException(StripeAccMissingException e, Locale locale) {
        String message = messageSource.getMessage(STRIPE_ACCOUNT_MISSING_EXCEPTION, new Object[] {},
                STRIPE_ACCOUNT_MISSING_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @PostMapping("/sign")
    public ResponseEntity<AuthenticatedUserDto> registerUser(
            @Validated({ UserDto.Validations.class }) @RequestBody UserDto userDto) throws DuplicateInstanceException {

        User user = UserConversor.toUser(userDto);
        userService.registerUser(user);
        JwtUserInfo jwtUser = new JwtUserInfo(user.getId(), user.getUsername(), user.getRole().toString());

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId())
                .toUri();

        return ResponseEntity.created(location)
                .body(new AuthenticatedUserDto(jwtGenerator.generateToken(jwtUser), UserConversor.toUserDto(user)));
    }

    @PostMapping("/logIn")
    public AuthenticatedUserDto logInUser(
            @Validated({ UserDto.Validations.class }) @RequestBody LoginParamsDto loginParams)
            throws IncorrectLogInException {

        User user = userService.logIn(loginParams.getEmail(), loginParams.getPassword());
        JwtUserInfo jwtUser = new JwtUserInfo(user.getId(), user.getUsername(), user.getRole().toString());

        return new AuthenticatedUserDto(jwtGenerator.generateToken(jwtUser), UserConversor.toUserDto(user));
    }

    @PostMapping("/logInFromServiceToken")
    public AuthenticatedUserDto logInFromServiceToken(@RequestAttribute Long userId,
            @RequestAttribute String serviceToken) throws InstanceNotFoundException {
        User user = userService.logInFromUserId(userId);

        return new AuthenticatedUserDto(serviceToken, UserConversor.toUserDto(user));

    }

    @PostMapping("/profile")
    public UserDto updateUserProfile(@RequestAttribute Long userId,
            @Validated({ UserDto.UpdateValidations.class }) @RequestBody UserDto userDto)
            throws InstanceNotFoundException, DuplicateInstanceException {
        User user = UserConversor.toUser(userDto);
        User updatedUser = userService.updateProfile(userId, user.getUsername(), user.getEmail(), user.getFirstName(),
                user.getLastName());

        return UserConversor.toUserDto(updatedUser);
    }

    @GetMapping("/chats/{chatId}/newMessages")
    public Boolean checkNewMessages(@RequestAttribute Long userId, @PathVariable Long chatId)
            throws InstanceNotFoundException, PermissionException {

        return userService.checkNewMessages(userId, chatId);
    }
    @GetMapping("/chats/newMessages")
    public Boolean checkNewMessagesGlobal(@RequestAttribute Long userId)
            throws InstanceNotFoundException {

        return userService.checkNewMessagesGlobal(userId);
    }

    @PostMapping("/chats/{chatId}/changeStatus")
    public void changeMessageStatus(@RequestAttribute Long userId, @PathVariable Long chatId)
            throws InstanceNotFoundException, PermissionException {

        userService.changeMessagesStatus(userId, chatId);
    }

    @GetMapping("/chats")
    public PageDto<ChatDto> getChats(@RequestAttribute Long userId,
            @RequestParam(name = "page", defaultValue = "0") int page) {
        Block<Chat> chats = userService.getChats(userId, page, 15);

        return new PageDto<>(ChatConversor.toChatsDto(chats.getItems()), chats.getTotalItems(), chats.getTotalPages(),
                chats.getExistMoreItems());
    }

    @GetMapping("/chats/find/{sellerId}/{productId}")
    public ChatDto getChatIfExistent(@RequestAttribute Long userId, @PathVariable Long sellerId,
            @PathVariable Long productId) throws InstanceNotFoundException {

        Chat chat = userService.getChatIfExistent(userId, sellerId, productId);

        if (chat == null) {
            return null;
        }

        return ChatConversor.toChatDto(chat);
    }

    @GetMapping("/chats/{chatId}")
    public ChatDto getChat(@RequestAttribute Long userId, @PathVariable Long chatId)
            throws InstanceNotFoundException, PermissionException {
        Chat chat = userService.getChat(userId, chatId);

        return ChatConversor.toChatDto(chat);
    }

    @GetMapping("/chats/{chatId}/messages")
    public SliceDto<MessageDto> getMessages(@RequestAttribute Long userId, @PathVariable Long chatId,
            @RequestParam(name = "page", defaultValue = "0") int page)
            throws InstanceNotFoundException, PermissionException {
        Block<Message> messages = userService.getMessages(userId, chatId, page, 20);

        return new SliceDto<>(MessageConversor.toMessagesDto(messages.getItems()), messages.getExistMoreItems());
    }

    @PostMapping("/stripe/connect")
    public StripeConnectDto createAndConnectStripeAccount(@RequestAttribute Long userId)
            throws StripeException, InstanceNotFoundException {
        String accountId = userService.getStripeAccId(userId);
        userService.getUser(userId);
        if (accountId != null) {
            return connect.connectExpressAccount(accountId);
        }
        return connect.createExpressAccount(userId);
    }

}
