package es.udc.tfg.aga.backend.model.amazon;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.udc.tfg.aga.backend.model.exceptions.FileDeleteException;
import es.udc.tfg.aga.backend.model.exceptions.FilesDownloadException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;

@Service
public class FileStore {

    @Autowired
    private AmazonS3 s3;

    public void saveImage(String link, String fileName, Optional<Map<String, String>> inputMetadata,
            InputStream inputStream) throws FileUploadException {
        ObjectMetadata metadata = new ObjectMetadata();
        inputMetadata.ifPresent(map -> {
            if (!map.isEmpty()) {
                map.forEach(metadata::addUserMetadata);
            }
        });
        try {
            s3.putObject(link, fileName, inputStream, metadata);
        } catch (AmazonServiceException e) {
            throw new FileUploadException(fileName);
        }
    }

    public byte[] getImage(String bucketName, String link) throws FilesDownloadException {
        try {
            S3Object object = s3.getObject(bucketName, link);
            return IOUtils.toByteArray(object.getObjectContent());
        } catch (AmazonServiceException | IOException e) {
            throw new FilesDownloadException(e.getMessage());
        }
    }

    public void deleteImage(String path, String filename, String bucketName) throws FileDeleteException {
        try {
            ObjectListing objects = s3.listObjects(bucketName, path);
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                s3.deleteObject(bucketName, objectSummary.getKey());
            }
            // s3.deleteObject(bucketName, path);
        } catch (AmazonServiceException e) {
            throw new FileDeleteException(filename);
        }

    }
}
