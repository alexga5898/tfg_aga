package es.udc.tfg.aga.backend.model.entities;

import java.util.List;

public interface CustomMessageDao {
    List<Message> findByRecipientSenderProductStatus(Long recipientId, Long chatId);
    Boolean checkByRecipientChatStatus(Long recipientId, Long chatId); 
}
