package es.udc.tfg.aga.backend.model.model;

import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Chat;
import es.udc.tfg.aga.backend.model.entities.Message;
import es.udc.tfg.aga.backend.model.entities.User;
import es.udc.tfg.aga.backend.model.exceptions.DuplicateInstanceException;
import es.udc.tfg.aga.backend.model.exceptions.IncorrectLogInException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;

public interface UserService {
    void registerUser(User user) throws DuplicateInstanceException;

    User logIn(String email, String password) throws IncorrectLogInException;

    User logInFromUserId(Long userId) throws InstanceNotFoundException;

    User updateProfile(Long userId, String username, String email, String firstName, String lastName)
            throws InstanceNotFoundException, DuplicateInstanceException;

    User getUser(Long userId) throws InstanceNotFoundException;

    Message sendMessage(Long senderId, Long recipientId, Long productId, String content)
            throws InstanceNotFoundException;

    void changeMessagesStatus(Long recipientId, Long chatId) throws InstanceNotFoundException, PermissionException;

    Boolean checkNewMessages(Long recipientId, Long chatId) throws InstanceNotFoundException, PermissionException;

    Boolean checkNewMessagesGlobal(Long recipientId) throws InstanceNotFoundException;

    Block<Chat> getChats(Long userId, int page, int size);

    Chat getChatIfExistent(Long buyerId, Long sellerId, Long productId)
            throws InstanceNotFoundException;

    Chat getChat(Long userId, Long chatId) throws InstanceNotFoundException, PermissionException;

    Block<Message> getMessages(Long userId, Long chatId, int page, int size)
            throws InstanceNotFoundException, PermissionException;

    void storeStripeAccId(Long userId, String stripeAccId) throws InstanceNotFoundException;

    Boolean checkStripeAcc(Long userId) throws InstanceNotFoundException;

    void activateStripeAcc(Long userId) throws InstanceNotFoundException;

    String getStripeAccId(Long userId) throws InstanceNotFoundException;

}
