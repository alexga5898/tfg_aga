package es.udc.tfg.aga.backend.model.model;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import es.udc.tfg.aga.backend.model.entities.Address;
import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.CreateOrdersParams;
import es.udc.tfg.aga.backend.model.entities.Order;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.Rating;
import es.udc.tfg.aga.backend.model.entities.RatingBlock;
import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.entities.TransferDataParams;
import es.udc.tfg.aga.backend.model.exceptions.AlreadyRatedException;
import es.udc.tfg.aga.backend.model.exceptions.ExistingOrderWithProductException;
import es.udc.tfg.aga.backend.model.exceptions.FileDeleteException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InsufficientStockException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxItemQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxProductQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;

public interface ShoppingService {

    Product sellProduct(Long userId, String name, BigDecimal price, int quantity, String condition, String category,
            String description, MultipartFile[] images) throws InstanceNotFoundException, InvalidQuantityException,
            InvalidFileException, FileUploadException, StripeAccMissingException;

    Product updateProduct(Long userId, Long productId, String name, BigDecimal price, int quantity, String condition,
            String category, String description, MultipartFile[] images)
            throws InstanceNotFoundException, PermissionException, FileUploadException, FileDeleteException,
            InvalidQuantityException, InvalidFileException;

    void removeProduct(Long userId, Long productId) throws InstanceNotFoundException, FileDeleteException,
            PermissionException, ExistingOrderWithProductException;

    ShoppingCart addItemCart(Long userId, Long shoppingCartId, Long productId, int quantity)
            throws InstanceNotFoundException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException;

    ShoppingCart updateItemCart(Long userId, Long shoppingCartId, Long productId, int quantity)
            throws InstanceNotFoundException, MaxProductQuantityException, PermissionException;

    ShoppingCart deleteItemCart(Long userId, Long shoppingCartId, Long productId)
            throws InstanceNotFoundException, PermissionException;

    void checkShoppingCartExists(Long shoppingCartId) throws InstanceNotFoundException;

    void checkAndChangeStock(List<CreateOrdersParams> createOrdersParams)
            throws InstanceNotFoundException, InsufficientStockException;

    List<TransferDataParams> buy(Long userId, List<CreateOrdersParams> createOrdersParams, Address address);

    Block<Order> getOrders(Long userId, Integer month, Integer year, Boolean asc, Boolean buyer, int page, int size);

    Order getOrder(Long userId, Long orderId) throws InstanceNotFoundException, PermissionException;

    Rating addRating(Long userId, Long orderId, int rate, String comment)
            throws InstanceNotFoundException, PermissionException, AlreadyRatedException;

    RatingBlock getUserRatings(Long userId, int page, int size);

    RatingBlock getRatings(String username, int page, int size);
}
