package es.udc.tfg.aga.backend.rest.dtos;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Rating;

public class RatingConversor {

    private RatingConversor() {

    }

    public static final List<RatingDetailsDto> toRatingDtos(List<Rating> ratings) {
        List<RatingDetailsDto> ratingDtos = ratings.stream().map(i -> RatingConversor.toRatingDto(i))
                .collect(Collectors.toList());
        return ratingDtos;
    }

    public static final List<RatingDto> toRatingShowDtos(List<Rating> ratings) {
        List<RatingDto> ratingDtos = ratings.stream().map(i -> RatingConversor.toRatingShowDto(i))
                .collect(Collectors.toList());
        return ratingDtos;
    }

    public static final RatingDetailsDto toRatingDto(Rating rating) {
        return new RatingDetailsDto(rating.getId(), rating.getBuyer().getUsername(), rating.getSeller().getUsername(),
                rating.getOrder().getId(), rating.getDate(), rating.getRate(), rating.getComment());
    }

    public static final RatingDto toRatingShowDto(Rating rating) {
        return new RatingDto(rating.getId(), rating.getBuyer().getUsername(), rating.getSeller().getUsername(),
                rating.getRate(), rating.getComment());
    }

}
