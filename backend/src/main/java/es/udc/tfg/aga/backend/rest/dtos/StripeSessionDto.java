package es.udc.tfg.aga.backend.rest.dtos;

public class StripeSessionDto {
    private String id;

    public StripeSessionDto() {
    }

    public StripeSessionDto(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}