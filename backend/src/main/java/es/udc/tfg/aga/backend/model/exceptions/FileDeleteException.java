package es.udc.tfg.aga.backend.model.exceptions;

@SuppressWarnings("serial")
public class FileDeleteException extends FileException {

    public FileDeleteException(String fileName) {
        super(fileName);
    }

}
