package es.udc.tfg.aga.backend.model.entities;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;

public interface CustomProductDao {
    Page<Product> findUserProducts(Long userId, String keywords, int page, int size);

    Page<Product> findProducts(String keywords, Long categoryId, Long conditionId, 
            BigDecimal lPrice, BigDecimal hPrice, int order, int page, int size);

}
