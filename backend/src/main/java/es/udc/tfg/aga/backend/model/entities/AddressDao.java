package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AddressDao extends PagingAndSortingRepository<Address, Long> {
    // Set<Address> findByUserId(Long userId);
}