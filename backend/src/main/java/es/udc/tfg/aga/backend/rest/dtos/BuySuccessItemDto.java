package es.udc.tfg.aga.backend.rest.dtos;

import java.math.BigDecimal;

public class BuySuccessItemDto {
    private Long productId;
    private String name;
    private int quantity;
    private BigDecimal price;
    
    public BuySuccessItemDto() {

    }

    public BuySuccessItemDto(Long productId, String name, int quantity, BigDecimal price) {
        this.productId = productId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
