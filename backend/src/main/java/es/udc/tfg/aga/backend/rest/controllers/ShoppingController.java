package es.udc.tfg.aga.backend.rest.controllers;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Locale;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.stripe.exception.StripeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import es.udc.tfg.aga.backend.model.entities.Block;
import es.udc.tfg.aga.backend.model.entities.Order;
import es.udc.tfg.aga.backend.model.entities.Product;
import es.udc.tfg.aga.backend.model.entities.Rating;
import es.udc.tfg.aga.backend.model.entities.RatingBlock;
import es.udc.tfg.aga.backend.model.entities.ShoppingCart;
import es.udc.tfg.aga.backend.model.exceptions.AlreadyRatedException;
import es.udc.tfg.aga.backend.model.exceptions.EmptyShoppingCartException;
import es.udc.tfg.aga.backend.model.exceptions.ExistingOrderWithProductException;
import es.udc.tfg.aga.backend.model.exceptions.FileDeleteException;
import es.udc.tfg.aga.backend.model.exceptions.FileUploadException;
import es.udc.tfg.aga.backend.model.exceptions.InstanceNotFoundException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidFileException;
import es.udc.tfg.aga.backend.model.exceptions.InvalidQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxItemQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.MaxProductQuantityException;
import es.udc.tfg.aga.backend.model.exceptions.PermissionException;
import es.udc.tfg.aga.backend.model.exceptions.StripeAccMissingException;
import es.udc.tfg.aga.backend.model.model.ShoppingService;
import es.udc.tfg.aga.backend.model.model.UserService;
import es.udc.tfg.aga.backend.rest.common.amazon.Email;
import es.udc.tfg.aga.backend.rest.common.stripe.Payment;
import es.udc.tfg.aga.backend.rest.dtos.AddRatingParamsDto;
import es.udc.tfg.aga.backend.rest.dtos.AddToOrUpdateItemCartParamsDto;
import es.udc.tfg.aga.backend.rest.dtos.DeleteItemCartParamsDto;
import es.udc.tfg.aga.backend.rest.dtos.ErrorsDto;
import es.udc.tfg.aga.backend.rest.dtos.OrderConversor;
import es.udc.tfg.aga.backend.rest.dtos.OrderDetailsDto;
import es.udc.tfg.aga.backend.rest.dtos.OrderDto;
import es.udc.tfg.aga.backend.rest.dtos.PageDto;
import es.udc.tfg.aga.backend.rest.dtos.ProductConversor;
import es.udc.tfg.aga.backend.rest.dtos.ProductDto;
import es.udc.tfg.aga.backend.rest.dtos.RatingConversor;
import es.udc.tfg.aga.backend.rest.dtos.RatingDetailsDto;
import es.udc.tfg.aga.backend.rest.dtos.RatingPageDto;
import es.udc.tfg.aga.backend.rest.dtos.RatingDto;
import es.udc.tfg.aga.backend.rest.dtos.ShoppingCartConversor;
import es.udc.tfg.aga.backend.rest.dtos.ShoppingCartDto;
import es.udc.tfg.aga.backend.rest.dtos.StripeSessionDto;

@RestController
@RequestMapping("/shopping")
@Validated
public class ShoppingController {

    @Autowired
    ShoppingService shoppingService;

    @Autowired
    UserService userService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Email email;

    @Autowired
    private Payment payment;

    private String INVALID_FILE_EXCEPTION = "exceptions.InvalidFileException";
    private String FILE_UPLOAD_EXCEPTION = "exceptions.FileUploadException";
    private String FILE_DELETE_EXCEPTION = "exceptions.FileDeleteException";
    private String MAX_PRODUCT_QUANTITY_EXCEPTION = "exceptions.MaxProductQuantityException";
    private String MAX_ITEM_QUANTITY_EXCEPTION = "exceptions.MaxItemsQuantityException";
    private String ALREADY_RATED_EXCEPTION = "exceptions.AlreadyRatedException";
    private String EXISTING_ORDER_WITH_PRODUCT_EXCEPTION = "exceptions.ExistingOrderWithProductException";

    @ExceptionHandler(InvalidFileException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    public ErrorsDto handleInvalidFileException(InvalidFileException e, Locale locale) {
        String message = messageSource.getMessage(INVALID_FILE_EXCEPTION, new Object[] { e.getContentType() },
                INVALID_FILE_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(FileUploadException.class)
    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ResponseBody
    public ErrorsDto handleFileUploadException(FileUploadException e, Locale locale) {
        String message = messageSource.getMessage(FILE_UPLOAD_EXCEPTION, new Object[] { e.getFileName() },
                FILE_UPLOAD_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(FileDeleteException.class)
    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ResponseBody
    public ErrorsDto handleFileDeleteException(FileDeleteException e, Locale locale) {
        String message = messageSource.getMessage(FILE_DELETE_EXCEPTION, new Object[] { e.getFileName() },
                FILE_DELETE_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(MaxProductQuantityException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleMaxProductQuantityException(MaxProductQuantityException e, Locale locale) {
        String message = messageSource.getMessage(MAX_PRODUCT_QUANTITY_EXCEPTION, new Object[] {},
                MAX_PRODUCT_QUANTITY_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(MaxItemQuantityException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleMaxItemQuantityException(MaxItemQuantityException e, Locale locale) {
        String message = messageSource.getMessage(MAX_ITEM_QUANTITY_EXCEPTION, new Object[] {},
                MAX_ITEM_QUANTITY_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(AlreadyRatedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsDto handleAlreadyRatedException(AlreadyRatedException e, Locale locale) {
        String message = messageSource.getMessage(ALREADY_RATED_EXCEPTION, new Object[] { e.getOrderId() },
                ALREADY_RATED_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @ExceptionHandler(ExistingOrderWithProductException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsDto handleExistingOrderWithProductException(ExistingOrderWithProductException e, Locale locale) {
        String message = messageSource.getMessage(EXISTING_ORDER_WITH_PRODUCT_EXCEPTION, new Object[] {},
                EXISTING_ORDER_WITH_PRODUCT_EXCEPTION, locale);

        return new ErrorsDto(message);
    }

    @PostMapping(path = "/products/newProduct", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> sellProduct(@RequestAttribute Long userId,
            @RequestParam("name") @NotNull @Size(min = 1, max = 30) String name,
            @RequestParam("price") @NotNull @DecimalMin(value = "0.01") @Max(value = 999999) BigDecimal price,
            @RequestParam("quantity") @NotNull @Min(value = 1) @Max(value = 999999) int quantity,
            @RequestParam("category") @NotNull String category, @RequestParam("condition") @NotNull String condition,
            @RequestParam("description") @NotNull @Size(min = 1, max = 800) String description,
            @RequestParam("images") MultipartFile[] images) throws InstanceNotFoundException, InvalidQuantityException,
            InvalidFileException, FileUploadException, StripeAccMissingException {

        Product addedProduct = shoppingService.sellProduct(userId, name, price, quantity, condition, category,
                description, images);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(addedProduct.getId()).toUri();

        return ResponseEntity.created(location).body(ProductConversor.toProductDto(addedProduct));
    }

    @PostMapping("/products/myProducts/{productId}")
    public ProductDto updateProduct(@RequestAttribute Long userId, @PathVariable Long productId,
            @RequestParam("name") @NotNull @Size(min = 1, max = 30) String name,
            @RequestParam("price") @NotNull @DecimalMin(value = "0.01") @Max(value = 999999) BigDecimal price,
            @RequestParam("quantity") @NotNull @Min(value = 1) @Max(value = 999999) int quantity,
            @RequestParam("category") @NotNull String category, @RequestParam("condition") @NotNull String condition,
            @RequestParam("description") @NotNull @Size(min = 1, max = 800) String description,
            @RequestParam("images") MultipartFile[] images) throws InstanceNotFoundException, PermissionException,
            FileUploadException, FileDeleteException, InvalidQuantityException, InvalidFileException {

        Product updatedProduct = shoppingService.updateProduct(userId, productId, name, price, quantity, condition,
                category, description, images);

        return ProductConversor.toProductDto(updatedProduct);
    }

    @DeleteMapping("/products/myProducts/{productId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeProduct(@RequestAttribute Long userId, @PathVariable Long productId)
            throws InstanceNotFoundException, PermissionException, FileDeleteException,
            ExistingOrderWithProductException {

        shoppingService.removeProduct(userId, productId);
    }

    @PostMapping("/shoppingCart/{shoppingCartId}/add")
    public ShoppingCartDto addItemCart(@RequestAttribute Long userId, @PathVariable Long shoppingCartId,
            @Validated @RequestBody AddToOrUpdateItemCartParamsDto shoppingCartItemDto)
            throws InstanceNotFoundException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException {

        ShoppingCart shoppingCart = shoppingService.addItemCart(userId, shoppingCartId,
                shoppingCartItemDto.getProductId(), shoppingCartItemDto.getQuantity());

        return ShoppingCartConversor.toShoppingCartDto(shoppingCart);

    }

    @PostMapping("/shoppingCart/{shoppingCartId}/update")
    public ShoppingCartDto updateItemCart(@RequestAttribute Long userId, @PathVariable Long shoppingCartId,
            @Validated @RequestBody AddToOrUpdateItemCartParamsDto shoppingCartItemDto)
            throws InstanceNotFoundException, MaxProductQuantityException, MaxItemQuantityException,
            PermissionException {
        ShoppingCart shoppingCart = shoppingService.updateItemCart(userId, shoppingCartId,
                shoppingCartItemDto.getProductId(), shoppingCartItemDto.getQuantity());

        return ShoppingCartConversor.toShoppingCartDto(shoppingCart);
    }

    @PostMapping("/shoppingCart/{shoppingCartId}/delete")
    public ShoppingCartDto deleteItemCart(@RequestAttribute Long userId, @PathVariable Long shoppingCartId,
            @Validated @RequestBody DeleteItemCartParamsDto shoppingCartItemDto) throws InstanceNotFoundException,
            MaxProductQuantityException, MaxItemQuantityException, PermissionException {
        ShoppingCart shoppingCart = shoppingService.deleteItemCart(userId, shoppingCartId,
                shoppingCartItemDto.getProductId());

        return ShoppingCartConversor.toShoppingCartDto(shoppingCart);
    }

    @PostMapping("/shoppingCart/checkout")
    public StripeSessionDto checkout(@RequestAttribute Long userId,
            @Validated @RequestBody ShoppingCartDto shoppingCartDto)
            throws StripeException, PermissionException, InstanceNotFoundException, EmptyShoppingCartException {
        if (userId != shoppingCartDto.getUserId()) {
            throw new PermissionException();
        }
        shoppingService.checkShoppingCartExists(shoppingCartDto.getId());
        return payment.paymentIntent(shoppingCartDto);
    }

    @GetMapping("/orders")
    public PageDto<OrderDto> getOrders(@RequestAttribute Long userId,
            @RequestParam(name = "month", required = false) Integer month,
            @RequestParam(name = "year", required = false) Integer year,
            @RequestParam(name = "asc", required = false, defaultValue = "false") Boolean asc,
            @RequestParam(name = "buyer", required = false, defaultValue = "true") Boolean buyer,
            @RequestParam(name = "page", defaultValue = "0") int page) {
        Block<Order> orders = shoppingService.getOrders(userId, month, year, asc, buyer, page, 10);
        return new PageDto<OrderDto>(OrderConversor.toOrderDtos(orders.getItems()), orders.getTotalItems(),
                orders.getTotalPages(), orders.getExistMoreItems());
    }

    @GetMapping("/orders/{orderId}")
    public OrderDetailsDto getOrder(@RequestAttribute Long userId, @PathVariable Long orderId)
            throws InstanceNotFoundException, PermissionException {
        return OrderConversor.toOrderDetailsDto(shoppingService.getOrder(userId, orderId));
    }

    @PostMapping("/ratings/addRating")
    public RatingDetailsDto addRating(@RequestAttribute Long userId,
            @Validated({ AddRatingParamsDto.Validations.class }) @RequestBody AddRatingParamsDto addRatingParamsDto)
            throws InstanceNotFoundException, AlreadyRatedException, PermissionException {
        Rating rating = shoppingService.addRating(userId, addRatingParamsDto.getOrderId(), addRatingParamsDto.getRate(),
                addRatingParamsDto.getComment());
        try {
            String subject = "Has recibido una valoración / You received a rating";
            String body = String.join(System.getProperty("line.separator"),
                    "<p>Has recibido una valoración de un cliente. Puedes ver tus valoraciones <a href='http://localhost:3000/shopping/ratings'>aquí.</a></p>",
                    "<p>You just received a rating from a buyer. You can check your ratings <a href='http://localhost:3000/shopping/ratings'>here.</a></p>");
            email.sendEmail(rating.getSeller().getEmail(), subject, body);
        } catch (Exception ex) {
        }
        return RatingConversor.toRatingDto(rating);
    }

    @GetMapping("/ratings")
    public RatingPageDto<RatingDetailsDto> getUserRatings(@RequestAttribute Long userId,
            @RequestParam(name = "page", defaultValue = "0") int page) {
        RatingBlock ratings = shoppingService.getUserRatings(userId, page, 10);
        return new RatingPageDto<RatingDetailsDto>(new PageDto<RatingDetailsDto>(
                RatingConversor.toRatingDtos(ratings.getRatings().getItems()), ratings.getRatings().getTotalItems(),
                ratings.getRatings().getTotalPages(), ratings.getRatings().getExistMoreItems()), ratings.getAvg());
    }

    @GetMapping("/ratings/{username}")
    public RatingPageDto<RatingDto> getRatings(@PathVariable String username,
            @RequestParam(name = "page", defaultValue = "0") int page) {
        RatingBlock ratings = shoppingService.getRatings(username, page, 10);
        return new RatingPageDto<RatingDto>(new PageDto<RatingDto>(
                RatingConversor.toRatingShowDtos(ratings.getRatings().getItems()), ratings.getRatings().getTotalItems(),
                ratings.getRatings().getTotalPages(), ratings.getRatings().getExistMoreItems()), ratings.getAvg());
    }

}
