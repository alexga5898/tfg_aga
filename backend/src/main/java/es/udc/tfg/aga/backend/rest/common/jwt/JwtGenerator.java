package es.udc.tfg.aga.backend.rest.common.jwt;

import java.security.Key;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JwtGenerator {
    
    @Value("${my.jwt.key}")
    private String key;
    
    @Value("${my.jwt.expiration}")
    private long expirationMin;

    public String generateToken(JwtUserInfo jwtUser){
        Claims claims = Jwts.claims();
        claims.put("userId", jwtUser.getUserId()); 
        claims.setSubject(jwtUser.getUsername());
        claims.put("role", jwtUser.getRole()); 
        Date expirationDate = new Date(System.currentTimeMillis() + expirationMin*60*1000);
        claims.setExpiration(expirationDate);      
        Key secretKey = Keys.hmacShaKeyFor(key.getBytes());

        return Jwts.builder().
            setClaims(claims).signWith(secretKey).setExpiration(expirationDate).compact();
    }

    public JwtUserInfo getJwtUser(String token) {
        Claims claims;
        claims = Jwts.parserBuilder().setSigningKey(key.getBytes()).build()
            .parseClaimsJws(token).getBody();
        return new JwtUserInfo(((Integer) claims.get("userId")).longValue(), claims.getSubject(),
             (String) claims.get("role"));
    }
}
