package es.udc.tfg.aga.backend.rest.dtos;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AddRatingParamsDto {

    public interface Validations {

    }

    private int rate;
    private String comment;
    private Long orderId;

    public AddRatingParamsDto() {

    }

    public AddRatingParamsDto(int rate, String comment, Long orderId) {
        this.rate = rate;
        this.comment = comment;
        this.orderId = orderId;
    }

    @NotNull(groups = Validations.class)
    @Digits(fraction = 0, integer = 1)
    @Min(value = 1, groups = Validations.class)
    @Max(value = 5, groups = Validations.class)
    public int getRate() {
        return this.rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    @NotNull(groups = Validations.class)
    @Size(min = 1, max = 800, groups = Validations.class)
    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @NotNull(groups = Validations.class)
    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

}
