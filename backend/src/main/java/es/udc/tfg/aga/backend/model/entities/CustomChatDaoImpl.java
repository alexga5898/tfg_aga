package es.udc.tfg.aga.backend.model.entities;

import java.util.Optional;

import javax.persistence.Query;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

public class CustomChatDaoImpl implements CustomChatDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Chat> findChat(Long senderId, Long recipientId, Long productId) {
        String queryString = "SELECT c FROM Chat c";

        if (senderId != null || productId != null) {
            queryString += " WHERE";
        }

        if (senderId != null && recipientId != null) {
            queryString += " ((c.buyer.id = :senderId AND c.seller.id = :recipientId) OR (c.seller.id = :senderId AND c.buyer.id = :recipientId))";

            if (productId != null) {
                queryString += " AND";
            }
        }

        if (productId != null) {
            queryString += " c.product.id = :productId";
        }

        Query query = entityManager.createQuery(queryString).setMaxResults(1);

        if (senderId != null) {
            query.setParameter("senderId", senderId);
        }

        if (senderId != null) {
            query.setParameter("recipientId", recipientId);
        }

        if (productId != null) {
            query.setParameter("productId", productId);
        }

        Optional<Chat> chat = Optional.empty();

        try {
            chat = Optional.of((Chat) query.getSingleResult());
        } catch (NoResultException e) {
        }

        return chat;
    }

}
