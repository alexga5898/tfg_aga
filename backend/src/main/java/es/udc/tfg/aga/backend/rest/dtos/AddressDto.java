package es.udc.tfg.aga.backend.rest.dtos;

public class AddressDto {
    private Long orderId;
    private String country;
    private String city;
    private String street;
    private String postalCode;

    public AddressDto() {

    }

    public AddressDto(Long orderId, String country, String city, String street, String postalCode) {
        this.orderId = orderId;
        this.country = country;
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
    }

    public Long getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}
