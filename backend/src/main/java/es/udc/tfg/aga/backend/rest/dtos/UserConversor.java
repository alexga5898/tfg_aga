package es.udc.tfg.aga.backend.rest.dtos;

import es.udc.tfg.aga.backend.model.entities.User;

public class UserConversor {

    private UserConversor() {

    }

    public final static UserDto toUserDto(User user) {
        return new UserDto(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(), user.getEmail(),
                user.getRole().toString(), ShoppingCartConversor.toShoppingCartDto(user.getShoppingCart()),
                user.getActiveTransfers());
    }

    public final static User toUser(UserDto userDto) {
        return new User(userDto.getUsername(), userDto.getFirstName(), userDto.getLastName(), userDto.getEmail(),
                userDto.getPassword());
    }

    public final static UserSummaryDto toUserSummaryDto(User user) {
        return new UserSummaryDto(user.getId(), user.getUsername());
    }

}
