package es.udc.tfg.aga.backend.rest.dtos;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.udc.tfg.aga.backend.model.entities.Image;

public class ImageConversor {

    private ImageConversor() {

    }

    // public static final Set<Image> toImages(List<ImageDto> imagesDto) {
    //     Set<Image> images = imagesDto.stream().map(i -> ImageConversor.toImage(i)).collect(Collectors.toSet());
    //     return images;
    // }

    public static final List<ImageDto> toImagesDto(Set<Image> images) {
        List<ImageDto> imagesDto = images.stream().map(i -> ImageConversor.toImageDto(i)).collect(Collectors.toList());
        imagesDto.sort(Comparator.comparing(ImageDto::getId));
        return imagesDto;
    }

    // public static final Image toImage(ImageDto imageDto) {
    //     return new Image(imageDto.getLink());
    // }

    public static final ImageDto toImageDto(Image image) {
        return new ImageDto(image.getId(), image.getProduct().getId(), image.getName(), image.getLink());
    }
}
