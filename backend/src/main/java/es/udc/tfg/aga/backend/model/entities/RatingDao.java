package es.udc.tfg.aga.backend.model.entities;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RatingDao extends PagingAndSortingRepository<Rating, Long>, CustomRatingDao{
    Page<Rating> findBySellerIdOrderByDateDesc(Long sellerId, Pageable pageable);
    Page<Rating> findBySellerUsernameOrderByDateDesc(String sellerUsername, Pageable pageable);
    @Query("SELECT AVG(r.rate) FROM Rating r WHERE r.seller.id = ?1")
    double getAvgBySellerId(Long sellerId);
    @Query("SELECT AVG(r.rate) FROM Rating r WHERE r.seller.username = ?1")
    double getAvgBySellerUsername(String sellerUsername);
}
